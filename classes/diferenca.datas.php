<?php
/*
Retorna diferenša entre as datas em Dias, Horas ou Minutos

Function Diferenca(data maior, [data menos],[dias horas ou minutos])

 "m" Minutos
 "H" Horas
 "h": Horas arredondada
 "D": Dias 
 "d": Dias arredontados
*/

function Diferenca($data1, $data2="",$tipo="")
	{
	if($data2=="")
		{
		$data2 = date("d/m/Y H:i:s");
		}
	if($tipo=="")
		{
		$tipo = "h";
		}

	for($i=1;$i<=2;$i++)
		{
		${"dia".$i} = substr(${"data".$i},0,2);
		${"mes".$i} = substr(${"data".$i},3,2);
		${"ano".$i} = substr(${"data".$i},6,4);
		${"horas".$i} = substr(${"data".$i},11,2);
		${"minutos".$i} = substr(${"data".$i},14,2);
		${"segundos".$i} = substr(${"data".$i},17,2);
		}

	$segundos = mktime($horas2,$minutos2,$segundos2,$mes2,$dia2,$ano2) - mktime($horas1,$minutos1,$segundos1,$mes1,$dia1,$ano1);

	switch($tipo)
		{
		 case "s": $difere = $segundos;    
		 	break;
		 case "m": $difere = $segundos/60;    
		 	break;
		 case "H": $difere = $segundos/3600;
		 	break;
		 case "h": $difere = round($segundos/3600);    
		 	break;
		 case "D": $difere = $segundos/86400;    
		 	break;
		 case "d": $difere = round($segundos/86400);    
		 	break;
		}

	return $difere;
	}

/*
$data1 = "01/02/2006 08:00";
$data2 = "01/02/2006 08:04";

echo Diferenca($data1,$data2,"D"); 
echo " dias exatos.<br>";
echo Diferenca($data1,$data2,"d"); 
echo " dias arredondados.<br>";
echo Diferenca($data1,$data2,"H"); 
echo " horas exatas.<br>";
echo Diferenca($data1,$data2,"h"); 
echo " horas arredondadas.<br>";
echo Diferenca($data1,$data2,"m"); 
echo " minutos <br>";
echo Diferenca($data1,$data2,"s"); 
echo " segundos <br>";
*/
?>
