<?php
function CorImagem($img) 
	{
	$imagem = imagecreatefromjpeg($img);
    $w = imagesx($imagem);
    $h = imagesy($imagem);
    $r = $g = $b = 0;
	
    for($y = 0; $y < $h; $y++) 
		{
        for($x = 0; $x < $w; $x++) 
			{
            $rgb = imagecolorat($imagem, $x, $y);
			
            $r += $rgb >> 16;
            $g += $rgb >> 8 & 255;
            $b += $rgb & 255;
			}
    	}
	
    $pxls = $w * $h;
    $r = dechex(round($r / $pxls));
    $g = dechex(round($g / $pxls));
    $b = dechex(round($b / $pxls));
    
	// acresenta 0 para valores com 1 digito
	if(strlen($r) < 2) 
		{
        $r = 0 . $r;
    	}
    if(strlen($g) < 2) 
		{
        $g = 0 . $g;
    	}
    if(strlen($b) < 2) 
		{
        $b = 0 . $b;
    	}
	
	// retorna valor
    return "#" . $r . $g . $b;
	}

//echo CorImagem("teste.jpg");
?>