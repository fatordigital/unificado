<?php
// funcao para pegar IP de mais informacoes da requisicao
$ip1 = $_SERVER['REMOTE_ADDR'];
$ip2 = $_SERVER['HTTP_X_FORWARDED_FOR'];
$ip3 = $_SERVER['HTTP_X_FORWARDED'];
$ip4 = $_SERVER['HTTP_FORWARDED_FOR'];
$ip5 = $_SERVER['HTTP_FORWARDED'];
$ip6 = $_SERVER['HTTP_X_COMING_FROM'];
$ip7 = $_SERVER['HTTP_COMING_FROM'];
$ip8 = $_SERVER['HTTP_CLIENT_IP'];

$ip_requisicao = $ip1;
if ($ip2 != "") $ip_requisicao = $ip2;
if ($ip3 != "") $ip_requisicao = $ip3;
if ($ip4 != "") $ip_requisicao = $ip4;
if ($ip5 != "") $ip_requisicao = $ip5;
if ($ip6 != "") $ip_requisicao = $ip6;
if ($ip7 != "") $ip_requisicao = $ip7;
if ($ip8 != "") $ip_requisicao = $ip8;
?>