<?php
function AcentosCaracteresEspeciais( $dado ) 
    {
    // $dado = trim( str_replace( "\'", "", $dado) );
    // $dado = str_replace( "'", "", $dado );
    $dado = str_replace( "–", "-", $dado );
    $dado = str_replace( "ç", "&ccedil;", $dado );
    $dado = str_replace( "á", "&aacute;", $dado );
    $dado = str_replace( "à", "&agrave;", $dado );
    $dado = str_replace( "â", "&acirc;", $dado );
    $dado = str_replace( "ã", "&atilde;", $dado );
    $dado = str_replace( "é", "&eacute;", $dado );
    $dado = str_replace( "è", "&egrave;", $dado );
    $dado = str_replace( "ê", "&ecirc;", $dado );
    $dado = str_replace( "í", "&iacute;", $dado );
    $dado = str_replace( "ì", "&igrave;", $dado );
    $dado = str_replace( "î", "&icirc;", $dado );
    $dado = str_replace( "ó", "&oacute;", $dado );
    $dado = str_replace( "ò", "&ograve;", $dado );
    $dado = str_replace( "ô", "&ocirc;", $dado );
    $dado = str_replace( "õ", "&otilde;", $dado );
    $dado = str_replace( "ú", "&uacute;", $dado );
    $dado = str_replace( "ù", "&ugrave;", $dado );
    $dado = str_replace( "û", "&ucirc;", $dado );
    $dado = str_replace( "ü", "&uuml;;", $dado );
    
    $dado = str_replace( "Ç", "&Ccedil;", $dado );
    $dado = str_replace( "Á", "&Aacute;", $dado );
    $dado = str_replace( "À", "&Agrave;", $dado );
    $dado = str_replace( "Â", "&Acirc;", $dado );
    $dado = str_replace( "Ã", "&Atilde;", $dado );
    $dado = str_replace( "É", "&Eacute;", $dado );
    $dado = str_replace( "È", "&Egrave;", $dado );
    $dado = str_replace( "Ê", "&Ecirc;", $dado );
    $dado = str_replace( "Í", "&Iacute;", $dado );
    $dado = str_replace( "Ì", "&Igrave;", $dado );
    $dado = str_replace( "Î", "&Icirc;", $dado );
    $dado = str_replace( "Ó", "&Oacute;", $dado );
    $dado = str_replace( "Ò", "&Ograve;", $dado );
    $dado = str_replace( "Ô", "&Ocirc;", $dado );
    $dado = str_replace( "Õ", "&Otilde;", $dado );
    $dado = str_replace( "Ú", "&Uacute;", $dado );
    $dado = str_replace( "Ù", "&Ugrave;", $dado );
    $dado = str_replace( "Û", "&Ucirc;", $dado );
    $dado = str_replace( "Ü", "&Uuml;;", $dado );

    return $dado;
    }

function AcentosCaracteresEspeciais2( $dado ) 
    {
    // $dado = trim( str_replace( "\'", "", $dado) );
    // $dado = str_replace( "'", "", $dado );
    $dado = str_replace( "–", "-", $dado );
    $dado = str_replace( "ç", "c", $dado );
    $dado = str_replace( "á", "a", $dado );
    $dado = str_replace( "à", "a", $dado );
    $dado = str_replace( "â", "a", $dado );
    $dado = str_replace( "ã", "a", $dado );
    $dado = str_replace( "é", "e", $dado );
    $dado = str_replace( "è", "e", $dado );
    $dado = str_replace( "ê", "e", $dado );
    $dado = str_replace( "í", "i", $dado );
    $dado = str_replace( "ì", "i", $dado );
    $dado = str_replace( "î", "i", $dado );
    $dado = str_replace( "ó", "o", $dado );
    $dado = str_replace( "ò", "o", $dado );
    $dado = str_replace( "ô", "o", $dado );
    $dado = str_replace( "õ", "o", $dado );
    $dado = str_replace( "ú", "u", $dado );
    $dado = str_replace( "ù", "u", $dado );
    $dado = str_replace( "û", "u", $dado );
    $dado = str_replace( "ü", "u", $dado );
    
    $dado = str_replace( "Ç", "C", $dado );
    $dado = str_replace( "Á", "A", $dado );
    $dado = str_replace( "À", "A", $dado );
    $dado = str_replace( "Â", "A", $dado );
    $dado = str_replace( "Ã", "A", $dado );
    $dado = str_replace( "É", "E", $dado );
    $dado = str_replace( "È", "E", $dado );
    $dado = str_replace( "Ê", "E", $dado );
    $dado = str_replace( "Í", "I", $dado );
    $dado = str_replace( "Ì", "I", $dado );
    $dado = str_replace( "Î", "I", $dado );
    $dado = str_replace( "Ó", "O", $dado );
    $dado = str_replace( "Ò", "O", $dado );
    $dado = str_replace( "Ô", "O", $dado );
    $dado = str_replace( "Õ", "O", $dado );
    $dado = str_replace( "Ú", "U", $dado );
    $dado = str_replace( "Ù", "U", $dado );
    $dado = str_replace( "Û", "U", $dado );
    $dado = str_replace( "Ü", "U", $dado );

    return $dado;
    }

function AcentosCaracteresEspeciais3( $dado ) 
    {
    // $dado = trim( str_replace( "\'", "", $dado) );
    // $dado = str_replace( "'", "", $dado );
    $dado = str_replace( "–", "-", $dado );
    $dado = str_replace( "ç", "c", $dado );
    $dado = str_replace( "á", "a", $dado );
    $dado = str_replace( "à", "a", $dado );
    $dado = str_replace( "â", "a", $dado );
    $dado = str_replace( "ã", "a", $dado );
    $dado = str_replace( "é", "e", $dado );
    $dado = str_replace( "è", "e", $dado );
    $dado = str_replace( "ê", "e", $dado );
    $dado = str_replace( "í", "i", $dado );
    $dado = str_replace( "ì", "i", $dado );
    $dado = str_replace( "î", "i", $dado );
    $dado = str_replace( "ó", "o", $dado );
    $dado = str_replace( "ò", "o", $dado );
    $dado = str_replace( "ô", "o", $dado );
    $dado = str_replace( "õ", "o", $dado );
    $dado = str_replace( "ú", "u", $dado );
    $dado = str_replace( "ù", "u", $dado );
    $dado = str_replace( "û", "u", $dado );
    $dado = str_replace( "ü", "u", $dado );
    
    $dado = str_replace( "Ç", "c", $dado );
    $dado = str_replace( "Á", "a", $dado );
    $dado = str_replace( "À", "a", $dado );
    $dado = str_replace( "Â", "a", $dado );
    $dado = str_replace( "Ã", "a", $dado );
    $dado = str_replace( "É", "e", $dado );
    $dado = str_replace( "È", "e", $dado );
    $dado = str_replace( "Ê", "e", $dado );
    $dado = str_replace( "Í", "i", $dado );
    $dado = str_replace( "Ì", "i", $dado );
    $dado = str_replace( "Î", "i", $dado );
    $dado = str_replace( "Ó", "o", $dado );
    $dado = str_replace( "Ò", "o", $dado );
    $dado = str_replace( "Ô", "o", $dado );
    $dado = str_replace( "Õ", "o", $dado );
    $dado = str_replace( "Ú", "u", $dado );
    $dado = str_replace( "Ù", "u", $dado );
    $dado = str_replace( "Û", "u", $dado );
    $dado = str_replace( "Ü", "u", $dado );
	
	$dado = str_replace(" ", "", $dado );

    return $dado;
    }

function AcentosCaracteresEspeciais4( $dado ) 
    {
	// IGUAL AcentosCaracteresEspeciais3 mas sem tirar espaco em branco
	$dado = str_replace( "–", "-", $dado );
    $dado = str_replace( "ç", "c", $dado );
    $dado = str_replace( "á", "a", $dado );
    $dado = str_replace( "à", "a", $dado );
    $dado = str_replace( "â", "a", $dado );
    $dado = str_replace( "ã", "a", $dado );
    $dado = str_replace( "é", "e", $dado );
    $dado = str_replace( "è", "e", $dado );
    $dado = str_replace( "ê", "e", $dado );
    $dado = str_replace( "í", "i", $dado );
    $dado = str_replace( "ì", "i", $dado );
    $dado = str_replace( "î", "i", $dado );
    $dado = str_replace( "ó", "o", $dado );
    $dado = str_replace( "ò", "o", $dado );
    $dado = str_replace( "ô", "o", $dado );
    $dado = str_replace( "õ", "o", $dado );
    $dado = str_replace( "ú", "u", $dado );
    $dado = str_replace( "ù", "u", $dado );
    $dado = str_replace( "û", "u", $dado );
    $dado = str_replace( "ü", "u", $dado );
    
    $dado = str_replace( "Ç", "c", $dado );
    $dado = str_replace( "Á", "a", $dado );
    $dado = str_replace( "À", "a", $dado );
    $dado = str_replace( "Â", "a", $dado );
    $dado = str_replace( "Ã", "a", $dado );
    $dado = str_replace( "É", "e", $dado );
    $dado = str_replace( "È", "e", $dado );
    $dado = str_replace( "Ê", "e", $dado );
    $dado = str_replace( "Í", "i", $dado );
    $dado = str_replace( "Ì", "i", $dado );
    $dado = str_replace( "Î", "i", $dado );
    $dado = str_replace( "Ó", "o", $dado );
    $dado = str_replace( "Ò", "o", $dado );
    $dado = str_replace( "Ô", "o", $dado );
    $dado = str_replace( "Õ", "o", $dado );
    $dado = str_replace( "Ú", "u", $dado );
    $dado = str_replace( "Ù", "u", $dado );
    $dado = str_replace( "Û", "u", $dado );
    $dado = str_replace( "Ü", "u", $dado );

    return $dado;
    }
?>