<?php
include "whoisphp.php";
// $allowed should be a list of authorised callers seperated by commas, If you don't care leave it blank
// Be careful if you call this in a new browser window as the referer may be blank.
$allowed="";
?>

<?php
// The following line gets the url variables and is used by the demo.
if (!empty($HTTP_GET_VARS)) while(list($name, $value) = each($HTTP_GET_VARS)) $$name = $value;
?>
<html>
<head>
<title>whoisphp demo - Whois lookup</title>
</head>
<body bgcolor="#FFFFCC">

<form name="DomainForm" method="get" action="lookup.php">
    <font face="Arial"><span style="font-size:10pt;">Demo domain lookup (whois)<br>Works
    with all supported domain extensions<br></span></font><br><input type="text" value="<?php print($domain);?>" name="domain" maxlength="63">

<input type="submit" name="button1" value="Check">
</form>
<form name="form1">
<?php

if ($domain!="")
{
    $i=whoisphp($domain,$domext,$Reg);
    if ($i==4)
    {
      print("<font face=\"Arial\">Sorry but you are not allowed access to this page</font>");
    }
    if ($i==5)
    {
      print("<font face=\"Arial\">Could not contact registry for $domext domains</font>");
    }
    if (($i==0) || ($i==1) || ($i==6))
    {
      print("<font face=\"Arial\">Registration details for $domain$domext<BR><BR></font>");
      print("-----------------------------------------------------------------<BR>");
      for ($k=0;$k<count($Reg);$k++)
      {
        print ("$Reg[$k]<BR>");
      }
      print("-----------------------------------------------------------------<BR>");
    }
    if ($i==2)
    {
      print("<font face=\"Arial\">Domain extension $domext not recognised</font>");
    }
    if ($i==3)
    {
      print("<font face=\"Arial\">$domain$domext is not a valid domain name</font>");
    }
}

?>

<p>&nbsp;</p>
</body>
</html>