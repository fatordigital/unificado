<?php
$diasemana[0] = "Domingo";
$diasemana[1] = "Segunda-feira";
$diasemana[2] = "Ter�a-feira";
$diasemana[3] = "Quarta-feira";
$diasemana[4] = "Quinta-feira";
$diasemana[5] = "Sexta-feira";
$diasemana[6] = "S�bado";

$mesnome[1] = "Janeiro";
$mesnome[2] = "Fevereiro";
$mesnome[3] = "Mar�o";
$mesnome[4] = "Abril";
$mesnome[5] = "Maio";
$mesnome[6] = "Junho";
$mesnome[7] = "Julho";
$mesnome[8] = "Agosto";
$mesnome[9] = "Setembro";
$mesnome[10] = "Outubro";
$mesnome[11] = "Novembro";
$mesnome[12] = "Dezembro";

$anoatual = date('Y');
$mesatual = date('n');
$diaatual = date('d');
$diasem = date('w');
$horacerta = date("H:i:s");

if ($diaatual=='1') { $diaatual="01"; }
if ($diaatual=='2') { $diaatual="02"; }
if ($diaatual=='3') { $diaatual="03"; }
if ($diaatual=='4') { $diaatual="04"; }
if ($diaatual=='5') { $diaatual="05"; }
if ($diaatual=='6') { $diaatual="06"; }
if ($diaatual=='7') { $diaatual="07"; }
if ($diaatual=='8') { $diaatual="08"; }
if ($diaatual=='9') { $diaatual="09"; }

if ($mesatual=='1') { $mesatual="01"; }
if ($mesatual=='2') { $mesatual="02"; }
if ($mesatual=='3') { $mesatual="03"; }
if ($mesatual=='4') { $mesatual="04"; }
if ($mesatual=='5') { $mesatual="05"; }
if ($mesatual=='6') { $mesatual="06"; }
if ($mesatual=='7') { $mesatual="07"; }
if ($mesatual=='8') { $mesatual="08"; }
if ($mesatual=='9') { $mesatual="09"; }

if ($mesatual=='01') 
	{
	$nome_periodo_ant="Dezembro";
	$nome_periodo="Janeiro";
	$nome_periodo_prox="Fevereiro";
	$mesanterior="12";
	$mesproximo="02"; 
	$anoanterior= $anoatual - 1;
	$anoproximo= $anoatual;
	$periodoanterior="12/" . $anoanterior; 
	if ($diaatual=='01') 
		{
		$diaanterior="31"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='02') 
	{
	$nome_periodo_ant="Janeiro";
	$nome_periodo="Fevereiro";
	$nome_periodo_prox="Mar�o";
	$mesanterior="01";
	$mesproximo="03"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$diaanterior="31"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='03') 
	{
	$nome_periodo_ant="Fevereiro";
	$nome_periodo="Mar�o";
	$nome_periodo_prox="Abril";
	$mesanterior="02";
	$mesproximo="04"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$TotalDiasPassado = cal_days_in_month(CAL_GREGORIAN, 02, $anoatual);
		$diaanterior=$TotalDiasPassado; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='04') 
	{
	$nome_periodo_ant="Mar�o";
	$nome_periodo="Abril";
	$nome_periodo_prox="Maio";
	$mesanterior="03";
	$mesproximo="05"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$diaanterior="31"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='05') 
	{
	$nome_periodo_ant="Abril";
	$nome_periodo="Maio";
	$nome_periodo_prox="Junho";
	$mesanterior="04";
	$mesproximo="06"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$diaanterior="30"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='06') 
	{
	$nome_periodo_ant="Maio";
	$nome_periodo="Junho";
	$nome_periodo_prox="Julho";
	$mesanterior="05";
	$mesproximo="07"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$diaanterior="31"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='07') 
	{
	$nome_periodo_ant="Junho";
	$nome_periodo="Julho";
	$nome_periodo_prox="Agosto";
	$mesanterior="06";
	$mesproximo="08"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$diaanterior="30"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='08') 
	{
	$nome_periodo_ant="Julho";
	$nome_periodo="Agosto";
	$nome_periodo_prox="Setembro";
	$mesanterior="07";
	$mesproximo="09"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$diaanterior="31"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='09') 
	{
	$nome_periodo_ant="Agosto";
	$nome_periodo="Setembro";
	$nome_periodo_prox="Outubro";
	$mesanterior="08";
	$mesproximo="10"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$diaanterior="31"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='10') 
	{
	$nome_periodo_ant="Setembro";
	$nome_periodo="Outubro";
	$nome_periodo_prox="Novembro";
	$mesanterior="09";
	$mesproximo="11"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$diaanterior="30"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='11') 
	{
	$nome_periodo_ant="Outubro";
	$nome_periodo="Novembro";
	$nome_periodo_prox="Dezembro";
	$mesanterior="10";
	$mesproximo="12"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$diaanterior="31"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($mesatual=='12') 
	{
	$nome_periodo_ant="Novembro";
	$nome_periodo="Dezembro";
	$nome_periodo_prox="Janeiro";
	$mesanterior="11";
	$mesproximo="01"; 
	$anoanterior= $anoatual;
	$anoproximo= $anoatual + 1;
	$periodoanterior = $mesanterior . $anoatual; 
	if ($diaatual=='01') 
		{
		$diaanterior="30"; 
		}
	else 
		{
		$diaanterior= $diaatual - 1; 
		}
	}

if ($diaanterior=='1') { $diaanterior="01"; }
if ($diaanterior=='2') { $diaanterior="02"; }
if ($diaanterior=='3') { $diaanterior="03"; }
if ($diaanterior=='4') { $diaanterior="04"; }
if ($diaanterior=='5') { $diaanterior="05"; }
if ($diaanterior=='6') { $diaanterior="06"; }
if ($diaanterior=='7') { $diaanterior="07"; }
if ($diaanterior=='8') { $diaanterior="08"; }
if ($diaanterior=='9') { $diaanterior="09"; }

$dataatual = $diasemana[$diasem].', '.$diaatual.' de '.$nome_periodo.' de '.$anoatual.' - Hora: '.$horacerta;
$dataatual_sem_hora = $diasemana[$diasem].', '.$diaatual.' de '.$nome_periodo.' de '.$anoatual;

$periodoatual = $mesatual . "/" . $anoatual;
$datainv_atual = $anoatual . $mesatual . $diaatual;

$data_aaaammdd = $anoatual."-".$mesatual."-".$diaatual;

// revisar daqui pra baixo

	if ($mesanterior=='1') { $mesanterior="01"; }
	if ($mesanterior=='2') { $mesanterior="02"; }
	if ($mesanterior=='3') { $mesanterior="03"; }
	if ($mesanterior=='4') { $mesanterior="04"; }
	if ($mesanterior=='5') { $mesanterior="05"; }
	if ($mesanterior=='6') { $mesanterior="06"; }
	if ($mesanterior=='7') { $mesanterior="07"; }
	if ($mesanterior=='8') { $mesanterior="08"; }
	if ($mesanterior=='9') { $mesanterior="09"; }
	
$invatual = $anoatual.$mesatual.$diaatual;
/*
echo"DIA ATUAL: $diaatual<BR>";
echo"DIA ANTERIOR: $diaanterior<BR><BR>";
echo"MES ATUAL: $mesatual<BR>";
echo"MES ANTERIOR: $mesanterior<BR><BR>";
echo"ANO ATUAL: $anoatual<BR>";
echo"ANO ANTERIOR: $anoanterior<BR><BR>";
echo"Data por Extenso: $dataatual<BR><BR>";
echo"Per�odo Atual: $periodoatual<BR>";
echo"Per�odo Atual: $periodoanterior<BR>";
echo"nome_periodo: $nome_periodo<BR>";
*/
?>