<?php 
// colorPalette
// detecta as 3 cores mais encontradas na imagem
function colorPalette($imageFile, $numColors, $granularity = 5) 
	{ 
	global $prefixo_db;
	global $origem;
	//echo"origem colorpalette - $origem<BR>";
	
	if ($origem == "site")
		{
		$diretorioclasses = "manager/classes";
		}
	else
		{
		$diretorioclasses = "classes";
		}
	include"".$diretorioclasses."/ordem.php";
	
    $granularity = max(1, abs((int)$granularity)); 
    $colors = array(); 
    $size = @getimagesize($imageFile); 
    if($size === false) 
    	{ 
        user_error("Unable to get image size data"); 
        return false; 
        } 
   	$img = @imagecreatefromjpeg($imageFile); 
    // Andres mentioned in the comments the above line only loads jpegs, 
    // and suggests that to load any file type you can use this: 
    // $img = @imagecreatefromstring(file_get_contents($imageFile)); 
    
    if (!$img) 
    	{ 
        user_error("Unable to open image file"); 
        return false; 
        } 
   	
    for ($x = 0; $x < $size[0]; $x += $granularity) 
    	{ 
        for ($y = 0; $y < $size[1]; $y += $granularity) 
        	{ 
            $thisColor = imagecolorat($img, $x, $y); 
            $rgb = imagecolorsforindex($img, $thisColor); 
            $red = round(round(($rgb['red'] / 0x33)) * 0x33); 
            $green = round(round(($rgb['green'] / 0x33)) * 0x33); 
            $blue = round(round(($rgb['blue'] / 0x33)) * 0x33); 
            $thisRGB = sprintf('%02X%02X%02X', $red, $green, $blue); 
            
			//$conta = $conta + 1;	
			
            if (array_key_exists($thisRGB, $colors)) 
            	{ 
				//echo"entrou1<BR>";
				//echo"thisRGB - $thisRGB<BR>";
                $colors[$thisRGB]++; 
				
				// verifica se COR ENCONTRADA esta no banco de dados
				
				//echo"COR - ".$thisRGB."<BR>";
				$acao=mysql_query("select idtmp1,conta from ".$prefixo_db."tmp1 where ordem='".$ordem."' && campo1='".$thisRGB."'") or die (mysql_error());
				$conta=mysql_num_rows($acao);
				//echo"contador da cor - ".$conta."<BR>";
				if ($conta < 1)
					{
					//echo"vai inserir registro<BR>";
					// se nao estiver, grava
					$ac=mysql_query("insert into ".$prefixo_db."tmp1(
					ordem, campo1, conta)
			
							  values
						  
					('".$ordem."','".$thisRGB."','1')") or die (mysql_error());
					}
				
				else
					{
					//echo"vai ATUALIZAR registro<BR>";
					while($r=mysql_fetch_array($acao)) 
						{
						$idtmp1=$r['idtmp1']; 
						$conta=$r['conta']; 
						}
					//echo"id registro - ".$idtmp1."<BR>";
					//echo"contador atual - ".$campo2."<BR>";
					
					$soma = $conta + 1;
					//echo"novo contador - ".$soma."<BR>";
					
					// se estiver, soma 1 no contador da cor
					$ac=mysql_query
					("update ".$prefixo_db."tmp1 set
					conta= '".$soma."'
					where idtmp1='".$idtmp1."'") or die (mysql_error());
					}
				//echo"<BR><BR>";
				
				// processo 1 - remove cores a partir da 4 posição
				$inicio = 1;
				$acao0=mysql_query("select * from ".$prefixo_db."tmp1 where ordem='".$ordem."' order by conta desc") or die (mysql_error());
				$conta0=mysql_num_rows($acao0);
				while($r0=mysql_fetch_array($acao0)) 
					{
					$idtmp1=$r0['idtmp1'];
					
					if ($inicio > 20)
						{
						$ac=mysql_query("delete from ".$prefixo_db."tmp1 where idtmp1='".$idtmp1."'") or die (mysql_error());
						}
					$inicio = $inicio + 1;
					}
				//*/
                } 
           	else 
            	{
				//echo"entrou2<BR>";
				//echo"thisRGB - $thisRGB<BR>"; 
                $colors[$thisRGB] = 1; 
                } 
           	}
       	} 
	
	$acao21=mysql_query("select * from ".$prefixo_db."tmp1 where ordem='".$ordem."' order by conta desc") or die (mysql_error());
	$conta21=mysql_num_rows($acao21);
	while($r21=mysql_fetch_array($acao21)) 
		{
		$m_conta2=$r21['conta']; 
		$total = $total + $m_conta2;
		}
		
	// lista as cores
	//echo "<table>"; 
	$acao2=mysql_query("select * from ".$prefixo_db."tmp1 where ordem='".$ordem."' order by conta desc") or die (mysql_error());
	$inicio2 = 1;
	$conta2=mysql_num_rows($acao2);
	while($r2=mysql_fetch_array($acao2)) 
		{
		$m_campo1=$r2['campo1']; 
		$m_conta=$r2['conta']; 
		$x = (100 * $m_conta) / $total;
		$x = round($x);
		if ($inicio2 == 1)
			{
			$totalx = $x;
			$retorno = $x."#".$m_campo1;
			}
		else
			{
			if ($inicio2 == 2)
				{
				$totalx = $totalx + $x;
				$xn = $x;
				}
			if ($inicio2 == 3)
				{
				$xn = 100 - $totalx;
				$xn = $xn;
				}
			$retorno = $retorno . "#".$xn."#".$m_campo1;
			}
		$inicio2 = $inicio2 + 1;
		//echo"<tr><td>".$m_conta."&nbsp;&nbsp;</td><td>".$x."&nbsp;&nbsp;</td><td>".$m_campo1."</td></tr>";
		}
	
	//foreach($palette as $color) 
	//	{ 
	//  echo "<tr><td style='background-color:#".$color.";width:2em;'>&nbsp;</td><td>#".$color."</td></tr>\n"; 
	//  }
	
	//echo "</table>\n";
	//echo"<BR>conta - $conta<BR>";
   	
	// apaga tabelas temporarias
	$ac=mysql_query("delete from ".$prefixo_db."tmp1 where ordem='".$ordem."'") or die (mysql_error());
	
	// retorno original
	//arsort($colors); 
    //return array_slice(array_keys($colors), 0, $numColors); 
    
	// novo retorno
	//echo"retorno - $retorno<BR>";
	return $retorno;
	} 

/*

// EXEMPLO
$palette = colorPalette('colete.jpg', 10, 4); 

// EXEMPLO DE TABELA DE CORES ENCONTRADAS
// soma todos contadores para obter porcentagem da cor na tabela de 3 cores mais encontradas
$acao21=mysql_query("select * from ".$prefixo_db."tmp1 where ordem='cor' order by conta desc") or die (mysql_error());
$conta21=mysql_num_rows($acao21);
while($r21=mysql_fetch_array($acao21)) 
	{
	$m_conta2=$r21['conta']; 
	$total = $total + $m_conta2;
	}
	
// lista as cores
echo "<table>"; 
$acao2=mysql_query("select * from ".$prefixo_db."tmp1 where ordem='cor' order by conta desc") or die (mysql_error());
$conta2=mysql_num_rows($acao2);
while($r2=mysql_fetch_array($acao2)) 
	{
	$m_campo1=$r2['campo1']; 
	$m_conta=$r2['conta']; 
	$x = (100 * $m_conta) / $total;
	$x = round($x);
	echo"<tr><td>".$m_conta."&nbsp;&nbsp;</td><td>".$x."&nbsp;&nbsp;</td><td>".$m_campo1."</td></tr>";
	}

//foreach($palette as $color) 
//	{ 
//  echo "<tr><td style='background-color:#".$color.";width:2em;'>&nbsp;</td><td>#".$color."</td></tr>\n"; 
//  }

echo "</table>\n";

*/
?>