<?php
session_start();

// abre sessao
ini_set('session.cache_expire', 30);
@ini_set("memory_limit", "256M");

//error_reporting(0);
//ini_set("display_errors", 0 );

foreach ($_REQUEST as $campo => $valor) {
    // pega os valores do formulario
    $$campo = $valor;
}

include "classes/config.php";

include "classes/data_class.php";
include "classes/RemoveAcentos.php";
include "classes/function.AcentosCaracteresEspeciais.php";
include "classes/CaixaAltaCaixaBaixa.php";
include "classes/anti.injection.php"; // nao altere
include "classes/verificar.email.php"; // nao altere
include "classes/func_caixa_alta.php"; // nao altere
include "classes/func_caixa_baixa.php"; // nao altere
include "classes/diferenca.datas.php"; // nao altere
include "classes/DataSomaTempo.php"; // nao altere
include "phpmailer/class.phpmailer.php"; // nao altere
include "classes/function.RemoveArquivo.php";
include "classes/funcao.TamanhoArquivo.php";
include "classes/convertefloat.php";
include "classes/convertemoeda.php";
include "classes/calcula.periodo.filtro.php";
include "classes/CalculoFrete.php";
include "classes/function.muralClientes_nomeAutor.php";

include "classes/function.proximoDia.php";
include "classes/function.anteriorDia.php";
include "classes/function.validaCPF.php";
include "classes/function.validaCNPJ.php";
include "classes/function.Agenda.php";
include "classes/function.ClienteTipoCadastro.php";
include "classes/function.DimensaoImagem.php";
include "classes/function.CorImagem.php";
include "classes/function.GetDPI.php";
include "classes/function.colorPalette.php";
include "classes/function.notNull.php";
include "classes/function.permissoes.php";
include "classes/function.MontaURLAmigavel.php";
include "classes/function.MesNominal.php";
include "classes/function.Formatar.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['contact-us-submit'])) {
    $enviar_contato = 'on';
    //echo"entrou1<BR>";
    //exit;
    include "includes/set-contato.php";
    exit;
}

if (isset($enviar_trabalhe) && $enviar_trabalhe == "on") {
    //echo"entrou1<BR>";
    //
    include "includes/set-trabalhe.php";
}

if (isset($enviar_matricula) && $enviar_matricula == "on") {
    //echo"entrou1<BR>";
    //
    include "includes/set-matriculas.php";
}
?>