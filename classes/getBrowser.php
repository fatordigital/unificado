<?php
function getBrowser($tipo)
	{
    // tipo eh a forma de retorno
    // 1 - nomenclatura completo // ex: Google Chrome 20.0
    // 2 - nomenclatura simples // chrome
    
	$useragent = $_SERVER['HTTP_USER_AGENT'];
 
 	if(strpos($useragent,"MSIE") !== false && strpos($useragent,"Opera") === false && strpos($useragent,"Netscape") === false)
 		{
 		$found = preg_match("/MSIE ([0-9]{1}\.[0-9]{1,2})/",$useragent, $mathes);
 		if($found)
 			{
            if ($tipo == 1)
            	{
 				return "Internet Explorer " . $mathes[1];
 				}
           	else
            	{
                return "ie";
                }
            }
 		}
 
 	elseif(strpos($useragent,"Gecko"))
 		{
 		$found = preg_match("/Firefox\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
        if($found)
            {
            if ($tipo == 1)
            	{
            	return "Mozilla Firefox " . $mathes[1];
                }
           	else
            	{
                return "firefox";
                }
            }
 
        $found = preg_match("/Netscape\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
        if($found)
            {
            if ($tipo == 1)
            	{
            	return "Netscape " . $mathes[1];
                }
           	else
            	{
                return "netscape";
                }
            }
     
        $found = preg_match("/Chrome\/([^\s]+)/",$useragent, $mathes);
        if($found)
            {
            if ($tipo == 1)
            	{
            	return "Google Chrome " . $mathes[1];
                }
           	else
            	{
                return "chrome";
                }
            }
     
        $found = preg_match("/Safari\/([0-9]{2,3}(\.[0-9])?)/",$useragent, $mathes);
        if($found)
            {
            if ($tipo == 1)
            	{
            	return "Safari " . $mathes[1];
                }
           	else
            	{
                return "safari";
                }
            }
     
        $found = preg_match("/Galeon\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
        if($found)
            {
            if ($tipo == 1)
            	{
            	return "Galeon " . $mathes[1];
                }
           	else
            	{
                return "galeon";
                }
            }
     
        $found = preg_match("/Konqueror\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
        if($found)
            {
            if ($tipo == 1)
            	{
            	return "Konqueror " . $mathes[1];
                }
           	else
            	{
                return "konqueror";
                }
            }
       	if ($tipo == 1)
          	{
        	return "Gecko based";
        	}
     	else
           	{
            return "gecko";
            }
      	}
 
    elseif(strpos($useragent,"Opera") !== false)
        {
        $found = preg_match("/Opera[\/ ]([0-9]{1}\.[0-9]{1}([0-9])?)/",$useragent,$mathes);
        if($found)
            {
            if ($tipo == 1)
                {
                return "Opera " . $mathes[1];
                }
            else
                {
                return "opera";
                }
            }
        }
 
    elseif (strpos($useragent,"Lynx") !== false)
        {
        $found = preg_match("/Lynx\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
        if($found)
            {
            if ($tipo == 1)
                {
                return "Lynx " . $mathes[1];
                }
            else
                {
                return "lynx";
                }
            }
        }
     
    elseif (strpos($useragent,"Netscape") !== false)
        {
        $found = preg_match("/Netscape\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",$useragent,$mathes);
        if($found)
            {
            if ($tipo == 1)
                {
                return "Netscape " . $mathes[1];
                }
            else
                {
                return "netscape";
                }
            }
        }
 
    else
        {
        return false;
        }
   	}
?>