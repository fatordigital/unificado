<html>
    <head>
        <title>Miniaturas e Marca D'�gua com PHP</title>
        <!--<link href="http://cachedcommons.org/cache/960/0.0.0/stylesheets/960.css" rel="stylesheet" />
        <style>
            @import url(http://fonts.googleapis.com/css?family=Electrolize|Open+Sans|Carrois+Gothic);
            body{background:#eee; font-family:'Open Sans'}
            img{border:3px solid #C32A25; padding:0px; margin:5px; margin-left:0px;}
            h1{color:#C32A25; text-align: center; font-family:'Electrolize'}
            p{padding:0px; margin:0px;  font-size: 15px; font-family:'Carrois Gothic'; text-align:left;}
            .b{color: #C32A25; font-size: 14px; font-weight: bold;}
            .c{display: inline-block; width:25px;}
            .code{font-size: 12px; font-family:'Open Sans'; }
            .clear{margin-bottom: 200px;}
        </style>-->
    </head>
    <body>
        <div class="container_12">           
            <div class="grid_12">
                <h1>PHP Watermark</h1>
                <img src="thumb.php?mv=centro&mh=meio&w=634&h=374&img=images/foto.jpg" />
                <img src="thumb.php?mv=centro&mh=topo&w=634&h=374&img=images/foto.jpg" />
                <img src="thumb.php?mv=centro&mh=baixo&w=634&h=374&img=images/foto.jpg" />
                <img src="thumb.php?mv=direita&mh=meio&w=634&h=374&img=images/foto.jpg" />
                <img src="thumb.php?mv=direita&mh=topo&w=634&h=374&img=images/foto.jpg" />
                <img src="thumb.php?mv=direita&mh=baixo&w=634&h=374&img=images/foto.jpg" />
                <img src="thumb.php?mv=esquerda&mh=meio&w=634&h=374&img=images/foto.jpg" />
                <img src="thumb.php?mv=esquerda&mh=topo&w=634&h=374&img=images/foto.jpg" />
            </div>
            <!--
            <div class="grid_12">
                <img src="thumb.php?mv=esquerda&mh=baixo&w=215&h=160&img=images/1.jpg" />
            </div>
            -->
            
            
            <div class="grid_12">
                <p>&nbsp;</p>                
                <p><strong>Posi��es Dispon�veis para Marca (na ordem das figuras):</strong></p>
                <p>Centro - Meio</p>
                <p>Centro - Topo</p>
                <p>Centro - Baixo</p>
                <p>Direita - Meio</p>
                <p>Direita - Topo</p>
                <p>Direita - Baixo</p>
                <p>Esquerda - Meio</p>
                <p>Esquerda - Topo</p>
                <p>Esquerda - Baixo</p>
            </div>
            <div class="grid_12">
                <p>&nbsp;</p>
                <p><strong>C�digo Utilizado para Gerar as Miniaturas:</strong></p>
                <code>
                &#60;img src="thumb.php?mv=centro&mh=meio&w=215&h=160&img=images/1.jpg" /&#62;   <Br />
                &#60;img src="thumb.php?mv=centro&mh=topo&w=215&h=160&img=images/1.jpg" /&#62;   <Br />
                &#60;img src="thumb.php?mv=centro&mh=baixo&w=215&h=160&img=images/1.jpg" /&#62;  <Br />
                &#60;img src="thumb.php?mv=direita&mh=meio&w=215&h=160&img=images/1.jpg" /&#62;  <Br />
                &#60;img src="thumb.php?mv=direita&mh=topo&w=215&h=160&img=images/1.jpg" /&#62;  <Br />
                &#60;img src="thumb.php?mv=direita&mh=baixo&w=215&h=160&img=images/1.jpg" /&#62; <Br />
                &#60;img src="thumb.php?mv=esquerda&mh=meio&w=215&h=160&img=images/1.jpg" /&#62; <Br />
                &#60;img src="thumb.php?mv=esquerda&mh=topo&w=215&h=160&img=images/1.jpg" /&#62; <Br />                  
                </code>
            </div>
            <div class="grid_12">
                <p>&nbsp;</p>
                <p><strong>Legendas:</strong></p>
                <code>
                &#60;img src="thumb.php?<span class="b">mv</span>=centro&<span class="b">mh</span>=meio&<span class="b">w</span>=215&<span class="b">h</span>=160&<span class="b">c</span>=1&<span class="b">img</span>=images/1.jpg" /&#62;   <Br />
                </code>
                <p>&nbsp;</p>
                <p><span class="b c">mv</span>  = Marca Vertical / Posi��o</p>
                <p><span class="b c">mh</span>  = Marca Horizontal / Posi��o</p>
                <p><span class="b c">w</span>   = Largura da Miniatura em Pixels</p>                
                <p><span class="b c">h</span>   = Altura da Miniatura em Pixels</p>                
                <p><span class="b c">img</span> = Caminho da Imagem</p>                
                <p><span class="b c">c</span>   = Crop ao Redimensionar (para n�o dar crop basta omitir <span class="b c">c</span>) </p>                
            </div>
            
            <div class="grid_12 clear">
                <p>&nbsp;</p>
            </div>
        </div>
    </body>
</html>