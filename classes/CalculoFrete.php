<?php
function calculaFrete($cod_servico, $cep_origem, $cep_destino, $peso, $altura='2', $largura='11', $comprimento='16', $valor_declarado='0.50')
	{
    #OFICINADANET###############################
    # C�digo dos Servi�os dos Correios
    # 41106 PAC sem contrato
    # 40010 SEDEX sem contrato
    # 40045 SEDEX a Cobrar, sem contrato
    # 40215 SEDEX 10, sem contrato
    ############################################

    $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=".$cep_origem."&sCepDestino=".$cep_destino."&nVlPeso=".$peso."&nCdFormato=1&nVlComprimento=".$comprimento."&nVlAltura=".$altura."&nVlLargura=".$largura."&sCdMaoPropria=n&nVlValorDeclarado=".$valor_declarado."&sCdAvisoRecebimento=n&nCdServico=".$cod_servico."&nVlDiametro=0&StrRetorno=xml";
    $xml = simplexml_load_file($correios);
    if($xml->cServico->Erro == '0')
        return $xml->cServico->Valor;
    else
        return false;
	}

/*
echo "<br><Br>C�lculo de FRETE PAC: ". 
calculaFrete('41106','91330150','91330150','0.3')."<br>";

echo "<br><Br>C�lculo de FRETE SEDEX: ". 
calculaFrete('40010','91330150','91330150','0.3')."<br>";

echo "<br><Br>C�lculo de FRETE SEDEX a cobrar: ". 
calculaFrete('40045','91330150','91330150','0.3')."<br>";

echo "<br><Br>C�lculo de FRETE SEDEX 10: ". 
calculaFrete('40215','91330150','91330150','0.3')."<br>";
*/
?>