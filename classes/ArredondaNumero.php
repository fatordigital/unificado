<?php
function ArredondaNumero($valor, $parametro)
	{
	if ($parametro == 1) // arredondar para cima
		{
		$numero = ceil($valor);
		}
	if ($parametro == 2) // arredondar para baixo
		{
		$numero = floor($valor);
		}
	if ($parametro == 3) // arredondar com round
	// 2.4 vai para 2
	// 2.6 vai para 3
		{
		$numero = round($valor);
		}
	return $numero;
	}
/*
$numero = 2.7;

$pracima = ArredondaNumero($numero, 1);
$prabaixo = ArredondaNumero($numero, 2);
$comround = ArredondaNumero($numero, 3);

echo"pracima - $pracima<BR>";
echo"prabaixo - $prabaixo<BR>";
echo"comround - $comround<BR>";
*/
?>