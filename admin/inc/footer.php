<!-- bottom footer -->
    <footer class="content-footer">

      <nav class="footer-right">
        <ul class="nav">
          <li>
            <a href="mailto:contato@wemake-md.com">Contato</a>
          </li>
          <li>
            <a href="javascript:;" class="scroll-up">
              <i class="fa fa-angle-up"></i>
            </a>
          </li>
        </ul>
      </nav>

      <nav class="footer-left">
        <ul class="nav">
          <li>
            <a href="javascript:;">Copyright <i class="fa fa-copyright"></i> <span>WEMAKE Marketing Digital</span> 2015. All rights reserved</a>
          </li>
          <li>
            <a href="javascript:;">
                Política de Privacidade
            </a>
          </li>
        </ul>
      </nav>

    </footer>
    <!-- /bottom footer -->