<!-- top header -->
      <header class="header navbar">

        <div class="brand visible-xs">
          <!-- toggle offscreen menu -->
          <div class="toggle-offscreen">
            <a href="#" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <!-- /toggle offscreen menu -->

          <!-- logo -->
          <div class="brand-logo">
            <img src="images/logo-dark.png" height="15" alt="">
          </div>
          <!-- /logo -->

          <!-- toggle chat sidebar small screen-->
          <div class="toggle-chat">
            <a href="javascript:;" class="hamburger-icon v2 visible-xs" data-toggle="layout-chat-open">
              <span></span>
              <span></span>
              <span></span>
            </a>
          </div>
          <!-- /toggle chat sidebar small screen-->
        </div>

        <ul class="nav navbar-nav hidden-xs">
          <li>
            <p class="navbar-text">
              Gerenciador de Conteúdo
              <?php
			  if ($_SESSION['unificado']['session_site'] == 1)
			  	{
				?>
                 | 
              Site: Unificado
                <?php
				}
			  elseif ($_SESSION['unificado']['session_site'] == 2)
			  	{
				?>
                 | 
              Site: Unificado Z
                <?php
				}
			  elseif ($_SESSION['unificado']['session_site'] == 3)
			  	{
				?>
                 | 
              Site: Unificado Med
                <?php
				}
			  elseif ($_SESSION['unificado']['session_site'] == 4)
			  	{
				?>
                 | 
              Site: Unificado Colégio
                <?php
				}
			?>
            </p>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-right hidden-xs">
          <li>
            <a href="javascript:;" data-toggle="dropdown">
              <img src="images/icon-site.jpg" class="header-avatar img-circle ml10" alt="user" title="user">
              <span class="pull-left">Selecione o Site</span>
            </a>
            <ul class="dropdown-menu">
              <!--
              <li>
                <a href="index.php?site=all">--todos--</a>
              </li>
              -->
              <li>
                <a href="index.php?site=1" title="Unificado">Unificado</a>
              </li>
              <li>
                <a href="index.php?site=2" title="Unificado Z">Unificado Z</a>
              </li>
              <li>
                <a href="index.php?site=3" title="Unificado Med">Unificado Med</a>
              </li>
              <li>
                <a href="index.php?site=4" title="Unificado Colégio">Unificado Colégio</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="javascript:;" data-toggle="dropdown">
              <img src="images/avatar.jpg" class="header-avatar img-circle ml10" alt="user" title="user">
              <span class="pull-left"><?php echo $cli_nom; ?></span>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="configuracoes.php">Configurações</a>
              </li>
              <li>
                <a href="usuario.php?ver_id=<?php echo $idcli; ?>">Alterar dados</a>
              </li>
              <li>
                <a href="index.php?sair=on">Logout</a>
              </li>
            </ul>
          </li>
        </ul>

      </header>
      <!-- /top header -->