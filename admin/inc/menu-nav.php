<!-- main navigation -->
      <nav role="navigation">

        <ul class="nav">

          <!-- Home -->
          <li>
            <a href="./">
              <i class="fa fa-home"></i>
              <span>Home</span>
            </a>
          </li>
          <!-- /Home -->
          
          <!-- Sliders -->
          <li>
            <a href="javascript:;">
              <i class="fa fa-slideshare"></i>
              <span>Slider</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="slider.php">
                  <span>Cadastrar</span>
                </a>
              </li>
              <li>
                <a href="sliders.php">
                  <span>Gerenciar</span>
                </a>
              </li>
              <li>
                <a href="slider-texto.php">
                  <span>Chamada</span>
                </a>
              </li>
            </ul>
          </li>
          <!-- /Sliders -->

          <!-- Empresa -->
          <li>
            <a href="javascript:;">
              <i class="fa fa-university"></i>
              <span>Empresa</span>
            </a>
            <ul class="sub-menu">
            <?php
			if (($_SESSION['unificado']['session_site'] == 2) or ($_SESSION['unificado']['session_site'] == 3) or ($_SESSION['unificado']['session_site'] == 4))
				{
				?>
              <li>
                <a href="quem-somos.php">
                  <span>Quem Somos</span>
                </a>
              </li>
              	<?php
				}
			
			if ($_SESSION['unificado']['session_site'] == 1)
				{
				?>
              <li>
                <a href="sobre.php">
                  <span>Sobre o Grupo</span>
                </a>
              </li>
              	<?php
				}
			if ($_SESSION['unificado']['session_site'] == 1)
				{
				?>
              <li>
                <a href="historia.php">
                  <span>História</span>
                </a>
              </li>
              	<?php
				}
			
			if ($_SESSION['unificado']['session_site'] == 1)
				{
				?>
              <li>
                <a href="javascript:;">
                  <span>Responsabilidade S.</span>
                </a>
                <ul class="sub-menu">
              		<li>
                	<a href="responsabilidade-social-projeto.php">
                  	<span>Cadastrar</span>
                	</a>
              		</li>
                    <li>
                	<a href="responsabilidade-social-projetos.php">
                  	<span>Gerenciar</span>
                	</a>
              		</li>
                    <li>
                	<a href="responsabilidade-social-intro.php">
                  	<span>Introdução</span>
                	</a>
              		</li>
                </ul>
              </li>
              	<?php
				}
			
			/*if ($_SESSION['unificado']['session_site'] == 3)
				{
				?>
              <li>
                <a href="historico.php">
                  <span>Histórico</span>
                </a>
              </li>
              <?php
				}*/
				?>
              <li>
                <a href="javascript:;">
                  <span>Sedes</span>
                </a>
                <ul class="sub-menu">
                  <li>
                    <a href="sede.php">
                      <span>Cadastrar</span>
                    </a>
                  </li>
                  <li>
                    <a href="sedes.php">
                      <span>Gerenciar</span>
                    </a>
                  </li>
                  <!--<li>
                    <a href="sedes-categorias.php">
                      <span>Categorias</span>
                    </a>
                  </li>
                  <li>
                    <a href="sede-texto.php">
                      <span>Chamada</span>
                    </a>
                  </li>-->
                </ul>
              </li>
              <?php
			  if (($_SESSION['unificado']['session_site'] == 2) or ($_SESSION['unificado']['session_site'] == 3))
				{
				?>
              <li>
                <a href="diferenciais.php">
                  <span>Diferenciais</span>
                </a>
              </li>
              <?php
				}
			if ($_SESSION['unificado']['session_site'] == 4)
				{
				?>
              <li>
                <a href="historico.php">
                  <span>Histórico</span>
                </a>
              </li>
              	<?php
				}
			
				?>
               
              <?php
			  if (($_SESSION['unificado']['session_site'] == 2) or ($_SESSION['unificado']['session_site'] == 3) or ($_SESSION['unificado']['session_site'] == 4))
			  	{
				?>
              <li>
                <a href="conheca-o-grupo.php">
                  <span>Chamadas</span>
                </a>
              </li>
                <?php
				}
			if ($_SESSION['unificado']['session_site'] == 1)
			  	{
				?>
              <li>
                <a href="grupo-unificado.php">
                  <span>Empresas</span>
                </a>
                <ul class="sub-menu">
                  <li>
                    <a href="grupo-unificado-z.php">
                      <span>Unificado Z</span>
                    </a>
                  </li>
                  <li>
                    <a href="grupo-unificado-med.php">
                      <span>Unificado Med</span>
                    </a>
                  </li>
                  <li>
                    <a href="grupo-unificado-colegio.php">
                      <span>Unificado Colégio</span>
                    </a>
                  </li>
                  <li>
                    <a href="grupo-unificado-colegio-eja.php">
                      <span>Unificado Colégio EJA</span>
                    </a>
                  </li>
                  <li>
                    <a href="grupo-gabarito.php">
                      <span>Gabarito</span>
                    </a>
                  </li>
                </ul>
              </li>
                <?php
				}
			?>
            </ul>
          </li>
          <!-- /Empresa -->
		
        	<?php
			if (($_SESSION['unificado']['session_site'] == 2) or ($_SESSION['unificado']['session_site'] == 3))
				{
				?>
          <!-- Cursos -->
          <li>
            <a href="javascript:;">
              <i class="fa fa-book"></i>
              <span>Cursos</span>
            </a>
            <ul class="sub-menu">
            <?php
			if ($_SESSION['unificado']['session_site'] == 2)
				{
				?>
              <li>
                <a href="curso-revisao.php">
                  <span>Curso Revisão</span>
                </a>
              </li>
              <li>
                <a href="curso-marco.php">
                  <span>Curso Março</span>
                </a>
              </li>
              <li>
                <a href="curso-agosto.php">
                  <span>Curso Agosto</span>
                </a>
              </li>
              <?php
				}
			if ($_SESSION['unificado']['session_site'] == 3)
				{
				?>
              <li>
                <a href="curso-extensivo.php">
                  <span>Curso Extensivo</span>
                </a>
              </li>
              <?php
				}
				?>
            </ul>
          </li>
          <!-- /Cursos -->
			<?php
				}
			
			if ($_SESSION['unificado']['session_site'] == 4)
				{
				?>
          <!-- Proposta -->
          <li>
            <a href="javascript:;">
              <i class="fa fa-bookmark"></i>
              <span>Proposta</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="proposta-pedagogica.php">
                  <span>Projeto Pedagógico</span>
                </a>
              </li>
              <li>
                <a href="processo-avaliativo.php">
                  <span>Processo Avaliativo</span>
                </a>
              </li>
              <li>
                <a href="proposta-introducao.php">
                  <span>Introdução</span>
                </a>
              </li>
              <li>
                <a href="proposta-chamadas.php">
                  <span>Chamadas</span>
                </a>
              </li>
            </ul>
          </li>
          <!-- /Proposta -->
          	

          <!-- Ensino Médio -->
          <li>
            <a href="ensino-medio.php">
              <i class="fa fa-bookmark-o"></i>
              <span>Ensino Médio</span>
            </a>
          </li>
          <!-- /Ensino Médio -->
          <?php
				}
			
			if ($_SESSION['unificado']['session_site'] == 2)
				{
			?>

          <!-- Videos -->
          <li>
			<a href="javascript:;">
              <i class="fa fa-youtube-play"></i>
              <span>Videos</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="video.php">
                  <span>Cadastrar</span>
                </a>
              </li>
              <li>
                <a href="videos.php">
                  <span>Gerenciar</span>
                </a>
              </li>
            </ul>
          </li>
          <!-- /Videos -->
          	<?php
				}
				?>

          <!-- Imagens -->
          <li>
			<a href="javascript:;">
              <i class="fa fa-youtube-play"></i>
              <span>Imagens</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="imagem.php">
                  <span>Cadastrar</span>
                </a>
              </li>
              <li>
                <a href="imagens.php">
                  <span>Gerenciar</span>
                </a>
              </li>
            </ul>
          </li>
          <!-- /Imagens -->

		<?php
        if ($_SESSION['unificado']['session_site'] == 2)
            {
            ?>
          <!-- Donwloads -->
          <li>
            <a href="javascript:;">
              <i class="fa fa-download"></i>
              <span>Downloads</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="download.php">
                  <span>Cadastrar</span>
                </a>
              </li>
              <li>
                <a href="downloads.php">
                  <span>Gerenciar</span>
                </a>
              </li>
              <li>
                <a href="downloads-categorias.php">
                  <span>Categorias</span>
                </a>
              </li>
            </ul>
          </li>
          <!-- /Donwloads -->
          	<?php
				}
			?>

          <!-- Notícias -->
          <li>
            <a href="javascript:;">
              <i class="fa fa-newspaper-o"></i>
              <span>Notícias</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="noticia.php">
                  <span>Cadastrar</span>
                </a>
              </li>
              <li>
                <a href="noticias.php">
                  <span>Gerenciar</span>
                </a>
              </li>
            </ul>
          </li>
          <!-- /Notícias -->
		<?php
        if (($_SESSION['unificado']['session_site'] == 2) or ($_SESSION['unificado']['session_site'] == 3) or ($_SESSION['unificado']['session_site'] == 4))
			{
			?>
          <!-- Formulários -->
          <li>
            <a href="javascript:;">
              <i class="fa fa-envelope"></i>
              <span>Formulários</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="form-matriculas.php">
                  <span>Matrículas</span>
                </a>
                <ul class="sub-menu">
                  <li>
                    <a href="form-mensagens.php?form=2">
                      <span>Mensagens</span>
                    </a>
                  </li>
                  <li>
                    <a href="form-configurar.php?form=2">
                      <span>Configurar</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="form-contatos.php">
                  <span>Contato</span>
                </a>
                <ul class="sub-menu">
                  <li>
                    <a href="form-mensagens.php?form=1">
                      <span>Mensagens</span>
                    </a>
                  </li>
                  <li>
                    <a href="form-configurar.php?form=1">
                      <span>Configurar</span>
                    </a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="form-contatos.php">
                  <span>Trabalhe conosco</span>
                </a>
                <ul class="sub-menu">
                  <li>
                    <a href="form-mensagens.php?form=3">
                      <span>Mensagens</span>
                    </a>
                  </li>
                  <li>
                    <a href="form-configurar.php?form=3">
                      <span>Configurar</span>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <!-- /Formulários -->
          	<?php
			}
		?>

		
          <!-- Informações -->
          <!--<li>
            <a href="javascript:;">
              <i class="fa fa-info-circle"></i>
              <span>Informações</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="informacao.php">
                  <span>Cadastrar</span>
                </a>
              </li>
              <li>
                <a href="informacoes.php">
                  <span>Gerenciar</span>
                </a>
              </li>
            </ul>
          </li>-->
          <li>
            <a href="informacoes.php">
              <i class="fa fa-bookmark-o"></i>
              <span>Informações</span>
            </a>
          </li>
          <!-- /Informações -->
          
          <?php
		  if ($cli_nivel == 1)
		  	{
			?>
          <!-- Usuários -->
          <li>
            <a href="javascript:;">
              <i class="fa fa-users"></i>
              <span>Usuários</span>
            </a>
            <ul class="sub-menu">
              <li>
                <a href="usuario.php">
                  <span>Cadastrar</span>
                </a>
              </li>
              <li>
                <a href="usuarios.php">
                  <span>Gerenciar</span>
                </a>
              </li>
            </ul>
          </li>
          <!-- /Usuários -->
          <?php
			}
		?>

        </ul>
      </nav>
      <!-- /main navigation -->