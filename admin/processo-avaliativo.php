<?php
include"inc/functions.php";
include"functions/verifica-logado.php";
include"functions/setform-processo-avaliativo.php";

$acao_busca_id=mysql_query("select * from processo_avaliativo where id='".$_SESSION['unificado']['session_site']."'") or die (mysql_error());
while($r_busca_id=mysql_fetch_array($acao_busca_id)) 
    {
    $rec_titulo=utf8_encode($r_busca_id['titulo']);
    $rec_subtitulo=utf8_encode($r_busca_id['subtitulo']);
    $rec_imagem=$r_busca_id['imagem'];
    $rec_texto=utf8_encode($r_busca_id['texto']);
    }
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Gerenciador de Conteúdo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="vendor/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="vendor/summernote/dist/summernote.css">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="styles/roboto.css">
  <link rel="stylesheet" href="styles/font-awesome.css">
  <link rel="stylesheet" href="styles/panel.css">
  <link rel="stylesheet" href="styles/feather.css">
  <link rel="stylesheet" href="styles/animate.css">
  <link rel="stylesheet" href="styles/urban.css">
  <link rel="stylesheet" href="styles/urban.skins.css">
  <!-- endbuild -->
  
  <link rel="stylesheet" href="styles/styles-adicional.css">
  
    <script type="text/javascript" src="classes/ckeditor_simples/ckeditor.js"></script>
    <script src="classes/ckeditor_simples/sample/sample.js" type="text/javascript"></script>
    <link href="classes/ckeditor_simples/sample/sample.css" rel="stylesheet" type="text/css" />

</head>

<body>

  <div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

      <div class="brand">

        <!-- logo -->
        <div class="brand-logo">
          <img src="images/logo.png" height="19" alt="">
        </div>
        <!-- /logo -->

        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
          <span></span>
  	      <span></span>
  	      <span></span>
  	      <span></span>
        </a>
        <!-- /toggle small sidebar menu -->

      </div>

      <?php
	  include"inc/menu-nav.php";
	  ?>

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

      <?php
	  include"inc/header.php";
	  ?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
        <?php
        if ($sucesso_alterar == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Texto alterado com sucesso!</strong></p></div>
            <?php
            }
        if ($erro_form == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-erro.png" style="float: left; margin-right: 10px;"><strong><span style="color:red">Erro - revise os campos obrigatórios!</span></strong></p></div>
            <?php
            }
        
        //echo"site - ".$_SESSION['unificado']['session_site']."<BR>";
        ?>
          <div class="panel-heading border">
                <ol class="breadcrumb" style="margin-bottom:5px">
                  <li>
                    <a href="./">Home</a>
                  </li>
                  <li>
                    <a href="javascript:;">Proposta</a>
                  </li>
                  <li class="active ng-binding">Processo Avaliativo</li>
                </ol>
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
                <form class="form-horizontal bordered-group" enctype="multipart/form-data" role="form" action="processo-avaliativo.php" method="post">

                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_titulo == 1)) { ?> style="color:red"<?php } ?>>Título</label>
                    <div class="col-sm-10">
                      <input <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> id="titulo" name="titulo" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $titulo; } else { echo $rec_titulo; } ?>">
                    </div>
                  </div>

                  <!--<div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_subtitulo == 1)) { ?> style="color:red"<?php } ?>>Sub-título</label>
                    <div class="col-sm-10">
                      <input <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> id="subtitulo" name="subtitulo" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $subtitulo; } else { echo $rec_subtitulo; } ?>">
                    </div>
                  </div>-->

                  <?php
                  /*
                      //echo"rec_imagem0 - $rec_imagem<BR>";
                      
                      //echo"entrou alterar<BR>";
                        //echo"erro_form - $erro_form<BR>";
                        if ($erro_form == 1)
                            {
                            //echo"entrou erro form<BR>";
                            //echo"gravou_arquivo - $gravou_arquivo<BR>";
                            //echo"file_temp - $file_temp<BR>";
                            //echo"rec_imagem - $rec_imagem<BR>";
                            if (($gravou_arquivo == 1) && ($file_temp != ""))
                                {
                                ?>
                                <div class="form-group">
                                <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && (($erro_file == 1) or ($erro_extensao == 1))) { ?> style="color:red"<?php } ?>>Imagem</label>
                                <div class="col-sm-10">
                                <img src="imagens/<?php echo $file_temp; ?>" style="width:300px">
                                </div>
                                </div>
                                
                                <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem" name="del_imagem" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                                <?php
                                }
                            elseif ($rec_imagem != "")
                                {
                                ?>
                                <div class="form-group">
                                <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && (($erro_file == 1) or ($erro_extensao == 1))) { ?> style="color:red"<?php } ?>>Imagem</label>
                                <div class="col-sm-10">
                                <img src="imagens/<?php echo $rec_imagem; ?>" style="width:300px">
                                </div>
                                </div>
                                
                                <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem" name="del_imagem" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                                <?php
                                }
                            }
                        elseif ($rec_imagem != "")
                            {
                            ?>
                            <div class="form-group">
                            <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && (($erro_file == 1) or ($erro_extensao == 1))) { ?> style="color:red"<?php } ?>>Imagem</label>
                            <div class="col-sm-10">
                            <img src="imagens/<?php echo $rec_imagem; ?>" style="width:300px">
                            </div>
                            </div>
                                
                                <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem" name="del_imagem" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                            <?php
                            }
                        
                        
                        ?>
                        <div class="form-group">
                        <label class="col-sm-2 control-label" style="<?php if (($erro_form == 1) && (($erro_file == 1) or ($erro_extensao == 1))) { ?> color:red<?php } ?>"><?php if (($file_temp != "") or (($alterar == "on") && ($rec_imagem != ""))) { ?>Alterar <?php } ?>Imagem</label>
                        <div class="col-sm-10">
                        <input id="file" name="file" type="file" style="margin-top:10px;">
                        </div>
                        </div>
                        */
                        ?>
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_texto == 1)) { ?> style="color:red"<?php } ?>>Texto</label>
                    <div class="col-sm-10">
                  <textarea <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> class="ckeditor" style="width:100%" id="texto" name="texto" rows="10"><?php if ($erro_form == 1) { echo $texto; } else { echo utf8_encode($rec_texto); } ?></textarea> 
			<script type="text/javascript">
			//<![CDATA[

				// This call can be placed at any point after the
				// <textarea>, or inside a <head><script> in a
				// window.onload event handler.

				// Replace the <textarea id="editor"> with an CKEditor
				// instance, using default configurations.
				CKEDITOR.replace( 'texto' );

			//]]>
			</script>
                  </div>
                  </div>
                  
                  <?php
                  //include"inc/form-sites.php";
                  ?>

                  <div class="form-group" style="text-align:right; padding-top:10px; padding-right:10px">
                    <button type="submit" name="alterar_processo_avaliativo" value="on" class="btn btn-warning">Salvar</button>
		  </div>
                  
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /main area -->

    </div>
    <!-- /content panel -->

    <?php
	include"inc/footer.php";
	?>

  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/extentions/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/onScreen/jquery.onscreen.js"></script>
  <script src="vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/ui/accordion.js"></script>
  <script src="scripts/ui/animate.js"></script>
  <script src="scripts/ui/link-transition.js"></script>
  <script src="scripts/ui/panel-controls.js"></script>
  <script src="scripts/ui/preloader.js"></script>
  <script src="scripts/ui/toggle.js"></script>
  <script src="scripts/urban-constants.js"></script>
  <script src="scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  <!-- page level scripts -->
  <script src="vendor/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js"></script>
  <script src="vendor/summernote/dist/summernote.min.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="scripts/pages/form-wysiwyg.js"></script>
  <!-- /initialize page scripts -->
  
  <script src="scripts/validate/jquery.maskedinput.min.js"></script>
    <script src="scripts/validate/jquery.validate.min.js"></script>
    <script src="scripts/validate/additional-methods.js"></script>
    
    <script src="scripts/function-certeza.js"></script>
    
    <script>
    jQuery(function($){
    //defina as máscaras de seus campos, o 9 indica um caracter numérico qualquer
    $("#fone").mask("(99)9999-9999?9");
    $("#date").mask("99/99/9999");
    $("#data").mask("99/99/9999");
    $("#cep").mask("99999-999");
    });
    </script>
  
</body>

</html>
