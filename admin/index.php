<?php
//echo"aqui444<BR>";
//echo"enviar_login - $enviar_login<BR>";
//echo"enviar_lembrar_senha - $enviar_lembrar_senha<BR>";
//exit;
include"functions/efetuar-login.php";
//exit;
include"inc/functions.php";
include"functions/verifica-logado.php";
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Gerenciador de Conteúdo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="styles/climacons-font.css">
  <link rel="stylesheet" href="vendor/rickshaw/rickshaw.min.css">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="styles/roboto.css">
  <link rel="stylesheet" href="styles/font-awesome.css">
  <link rel="stylesheet" href="styles/panel.css">
  <link rel="stylesheet" href="styles/feather.css">
  <link rel="stylesheet" href="styles/animate.css">
  <link rel="stylesheet" href="styles/urban.css">
  <link rel="stylesheet" href="styles/urban.skins.css">
  <!-- endbuild -->

</head>

<body>

  <div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

      <div class="brand">

        <!-- logo -->
        <div class="brand-logo">
          <img src="images/logo.png" height="26" alt="">
        </div>
        <!-- /logo -->

        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
          <span></span>
  	      <span></span>
  	      <span></span>
  	      <span></span>
        </a>
        <!-- /toggle small sidebar menu -->

      </div>

      <?php
	  include"inc/menu-nav.php";
	  ?>

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

      <?php
	  include"inc/header.php";
	  ?>

      <!-- main area -->
      <div class="main-content">

        <div class="row">
          <div class="col-md-12">
            <div class="widget bg-white">
              <div class="widget-icon bg-blue pull-left fa fa-tachometer" style="margin-top:15px">
              </div>
              <div class="overflow-hidden">
                <h2>Painel de Instrumentos</h2>
              </div>
            </div>
          </div>
        </div>
        
        <!--
        <div class="row">
          <div class="col-md-8">
            <div class="row">
              <div class="col-md-12">
              	<div style="width:100%; background:#F49D0F; padding:15px 20px 15px 20px">
                	<span style="font-size:16px; color:#fff">Últimas Mensagens</span>
                </div>
                <section class="widget bg-white post-comments">
                  <div class="media">
                    <a class="pull-left" href="javascript:;">
                      <img class="media-object avatar avatar-sm" src="images/avatar.jpg" alt="">
                    </a>
                    <div class="comment">
                      <div class="comment-author h6 no-margin">
                        <div class="comment-meta small">May 2015, 19:20</div>
                        <a href="javascript:;"><strong>Samuel Perkins</strong></a>

                        <ul class="text-white list-style-none mb0">
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                        </ul>
                      </div>

                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
                      </p>
                    </div>
                  </div>

                  <hr>


                  <div class="media">
                    <a class="pull-left" href="javascript:;">
                      <img class="media-object avatar avatar-sm" src="images/face1.jpg" alt="">
                    </a>
                    <div class="comment">
                      <div class="comment-author h6 no-margin">
                        <div class="comment-meta small">May 2015, 19:20</div>
                        <a href="javascript:;"><strong>Adam Robertson</strong></a>
                      </div>

                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
                      </p>
                    </div>
                  </div>

                  <hr>

                  <div class="media">
                    <a class="pull-left" href="javascript:;">
                      <img class="media-object avatar avatar-sm" src="images/face5.jpg" alt="">
                    </a>
                    <div class="comment">
                      <div class="comment-author h6 no-margin">
                        <div class="comment-meta small">May 2015, 19:20</div>
                        <a href="javascript:;"><strong>Christina Day</strong></a>

                        <ul class="text-white list-style-none mb0">
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star"></li>
                          <li class="fa fa-star"></li>
                        </ul>
                      </div>

                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
                      </p>
                    </div>
                  </div>

                </section>
              </div>
            </div>
          </div>
          <div class="col-md-4">

            <div class="widget bg-danger-light">
              <div class="text-uppercase">
                <strong>New update</strong>
              </div>
              <p class="text-white">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
              </p>

              <a href="javascript:;" class="btn btn-danger mt5">Download Now</a>
            </div>

            <div class="widget bg-lightblue">
              <div class="widget-bg-icon">
                <i class="fa fa-bookmark-o"></i>
              </div>
              <div class="widget-details">
                <span class="block h4 mt0 mb5">4,894K</span>
                <span>Parks and recreation</span>
                <progressbar value="40" type="info" class="widget-progress"></progressbar>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm-6 col-md-3">
            <div class="widget bg-white">
              <div class="widget-icon bg-blue pull-left fa fa-microphone">
              </div>
              <div class="overflow-hidden">
                <span class="widget-title">8,372K</span>
                <span class="widget-subtitle">Registered users</span>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget bg-white">
              <div class="widget-icon bg-danger pull-left fa fa-tint">
              </div>
              <div class="overflow-hidden">
                <span class="widget-title percent">86</span>
                <span class="widget-subtitle">Revenue increase</span>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget bg-white">
              <div class="widget-icon bg-success pull-left fa fa-paper-plane">
              </div>
              <div class="overflow-hidden">
                <span class="widget-title">7,355K</span>
                <span class="widget-subtitle">Pending orders</span>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-3">
            <div class="widget bg-white">
              <div class="widget-icon bg-purple pull-left fa fa-envelope">
              </div>
              <div class="overflow-hidden">
                <span class="widget-title">7,834K</span>
                <span class="widget-subtitle">Messages</span>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-8">
            <div class="row">
              <div class="col-md-12">
              	<div style="width:100%; background:#F49D0F; padding:15px 20px 15px 20px">
                	<span style="font-size:16px; color:#fff">Últimas Notícias</span>
                </div>
                <section class="widget bg-white post-comments">
                  <div class="media">
                    <a class="pull-left" href="javascript:;">
                      <img class="media-object avatar avatar-sm" src="images/avatar.jpg" alt="">
                    </a>
                    <div class="comment">
                      <div class="comment-author h6 no-margin">
                        <div class="comment-meta small">May 2015, 19:20</div>
                        <a href="javascript:;"><strong>Samuel Perkins</strong></a>

                        <ul class="text-white list-style-none mb0">
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                        </ul>
                      </div>

                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
                      </p>
                    </div>
                  </div>

                  <hr>


                  <div class="media">
                    <a class="pull-left" href="javascript:;">
                      <img class="media-object avatar avatar-sm" src="images/face1.jpg" alt="">
                    </a>
                    <div class="comment">
                      <div class="comment-author h6 no-margin">
                        <div class="comment-meta small">May 2015, 19:20</div>
                        <a href="javascript:;"><strong>Adam Robertson</strong></a>
                      </div>

                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
                      </p>
                    </div>
                  </div>

                  <hr>

                  <div class="media">
                    <a class="pull-left" href="javascript:;">
                      <img class="media-object avatar avatar-sm" src="images/face5.jpg" alt="">
                    </a>
                    <div class="comment">
                      <div class="comment-author h6 no-margin">
                        <div class="comment-meta small">May 2015, 19:20</div>
                        <a href="javascript:;"><strong>Christina Day</strong></a>

                        <ul class="text-white list-style-none mb0">
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star text-warning"></li>
                          <li class="fa fa-star"></li>
                          <li class="fa fa-star"></li>
                        </ul>
                      </div>

                      <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Frater et T. Sed ad bona praeterita redeamus.
                      </p>
                    </div>
                  </div>

                </section>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="row">
              <div class="col-md-12">
              	<div style="width:100%; background:#F49D0F; padding:15px 20px 15px 20px">
                	<span style="font-size:16px; color:#fff">Últimas Vídeo</span>
                </div>
              <iframe width="100%" height="238" src="https://www.youtube.com/embed/1a4XJIPDS5I?list=PLAOEeqD-HfWT2219FNu6siGPsQA11fa8r&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>

              </div>
            </div>
          </div>
		</div>-->

      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

    <?php
	include"inc/footer.php";
	?>

  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/extentions/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/onScreen/jquery.onscreen.js"></script>
  <script src="vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/ui/accordion.js"></script>
  <script src="scripts/ui/animate.js"></script>
  <script src="scripts/ui/link-transition.js"></script>
  <script src="scripts/ui/panel-controls.js"></script>
  <script src="scripts/ui/preloader.js"></script>
  <script src="scripts/ui/toggle.js"></script>
  <script src="scripts/urban-constants.js"></script>
  <script src="scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  <!-- page level scripts -->
  <script src="vendor/d3/d3.min.js"></script>
  <script src="vendor/rickshaw/rickshaw.min.js"></script>
  <script src="vendor/flot/jquery.flot.js"></script>
  <script src="vendor/flot/jquery.flot.resize.js"></script>
  <script src="vendor/flot/jquery.flot.categories.js"></script>
  <script src="vendor/flot/jquery.flot.pie.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="scripts/pages/dashboard.js"></script>
  <!-- /initialize page scripts -->
  
  <script src="scripts/validate/jquery.maskedinput.min.js"></script>
    <script src="scripts/validate/jquery.validate.min.js"></script>
    <script src="scripts/validate/additional-methods.js"></script>
    
    <script src="scripts/function-certeza.js"></script>
    
    <script>
    jQuery(function($){
    //defina as máscaras de seus campos, o 9 indica um caracter numérico qualquer
    $("#fone").mask("(99)9999-9999?9");
    $("#date").mask("99/99/9999");
    $("#data").mask("99/99/9999");
    $("#cep").mask("99999-999");
    });
    </script>

</body>

</html>
