<?php
include"inc/functions.php";
include"functions/verifica-logado.php";
include"functions/setform-imagens.php";
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Gerenciador de Conteúdo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="styles/climacons-font.css">
  <link rel="stylesheet" href="vendor/rickshaw/rickshaw.min.css">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="styles/roboto.css">
  <link rel="stylesheet" href="styles/font-awesome.css">
  <link rel="stylesheet" href="styles/panel.css">
  <link rel="stylesheet" href="styles/feather.css">
  <link rel="stylesheet" href="styles/animate.css">
  <link rel="stylesheet" href="styles/urban.css">
  <link rel="stylesheet" href="styles/urban.skins.css">
  <!-- endbuild -->
  
  <link rel="stylesheet" href="styles/styles-adicional.css">

</head>

<body>

  <div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

      <div class="brand">

        <!-- logo -->
        <div class="brand-logo">
          <img src="images/logo.png" height="19" alt="">
        </div>
        <!-- /logo -->

        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
          <span></span>
  	      <span></span>
  	      <span></span>
  	      <span></span>
        </a>
        <!-- /toggle small sidebar menu -->

      </div>

      <?php
	  include"inc/menu-nav.php";
	  ?>

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

      <?php
	  include"inc/header.php";
	  ?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
        <?php
        if ($sucesso_cadastro == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Galeria cadastrada com sucesso!</strong></p></div>
            <?php
            }
        if ($sucesso_alterar == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Galeria alterada com sucesso!</strong></p></div>
            <?php
            }
        if ($sucesso_alterar_imagem == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Imagem alterada com sucesso!</strong></p></div>
            <?php
            }
        if ($sucesso_remover == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Galeria removida com sucesso!</strong></p></div>
            <?php
            }
        if ($sucesso_remover_imagem == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Imagem removida com sucesso!</strong></p></div>
            <?php
            }
        if ($sucesso_enviar_imagens == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Imagens enviadas com sucesso!</strong></p></div>
            <?php
            }
        if ($erro_form == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-erro.png" style="float: left; margin-right: 10px;"><strong><span style="color:red">Erro - revise os campos obrigatórios!</span></strong></p></div>
            <?php
            }
        ?>
          <div class="panel-heading">
                <ol class="breadcrumb">
                  <li>
                    <a href="./">Home</a>
                  </li>
                  <li class="active ng-binding">Imagens</li>
                </ol>
          </div>
          <div class="panel-body">
          <?php
            if (($pagina=="") or ($pagina == 0)) $pagina = 1;  
            else $pagina=$pagina;
            
            //echo"pagina - $pagina<BR>"; 
            
            $num_por_pagina = 10;
            
            if ($_SESSION['unificado']['session_site'] != "")
                {
                //echo"tabela_site - $tabela_site<BR>";
                $acao_not_ver="select * from imagens 
                order by id desc";
                //echo"acao_not_ver 1 - $acao_not_ver<BR>";
                }
            else
                {
                $acao_not_ver="select * from imagens 
                order by id desc";
                //echo"acao_not_ver 2 - $acao_not_ver<BR>";
                }
            
            $consulta = $acao_not_ver;
            $res1 = mysql_query($consulta);
            $total=mysql_num_rows($res1);
            $total = $total/$num_por_pagina;
            
            $prev = $pagina - 1;
            $next = $pagina + 1;	
            if ($pagina > 1) 
                {
                $prev_link = "<li><a href=\"imagens.php?pagina=".$prev."\">←</a></li>";
                } 
            else 
                { 
                $prev_link = "<li><a href=\"javascript:;\">←</a></li>";
                }
            if ($total > $pagina) 
                {
                $next_link = "<li><a href=\"imagens.php?pagina=".$next."\">→</a></li>";
                } 
            else 
                { 
                $next_link = "<li><a href=\"javascript:;\">→</a></li>";
                }
            $total = ceil($total);
            $painel = "";
            for ($x=1; $x<=$total; $x++) 
                {
                if ($x==$pagina) 
                    {
                    $painel .= "<li><a style=\"background-color: #eee\" href=\"javascript:;\">".$x."</a></li>";
                    } 
                else 
                    {
                    $painel .= "<li><a href=\"imagens.php?pagina=".$x."\">".$x."</a></li>";
                    }
                }
                    
            $conta_acao_not_ver = $total;
            //echo" conta_acao_not_ver - $conta_acao_not_ver<BR>";
            
            $inicio = 1;
            $primeiro_registro = ($pagina*$num_por_pagina) - $num_por_pagina;
                    
            $inicio_txt = 0;
            $query_stringNot=mysql_query($acao_not_ver." LIMIT ".$primeiro_registro.", ".$num_por_pagina."");
            $total_stringNot=mysql_num_rows($query_stringNot);
            if ($total_stringNot > 0)
                {
                ?>
                <div class="table-responsive">
                <table class="table table-bordered table-striped mb0">
                <thead>
                  <tr>
                    <th>Notícia</th>
                    <th>Capa</th>
                    <th>Título</th>
                    <th>Ações</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Notícia</th>
                    <th>Capa</th>
                    <th>Título</th>
                    <th>Ações</th>
                  </tr>
                </tfoot>
                <tbody>
                <?php
                $inot = 1;
                $i = 1;
                while($array=mysql_fetch_array($query_stringNot)) 
                    {
                    $ver_id=$array['id'];
                    $titulo=utf8_encode($array['titulo']); 
                    //$imagem=$array['imagem'];
                    
                    $acao_busca_img=mysql_query("select imagem_p,imagem_g from imagens_img where img_id='".$ver_id."' && capa='1'") or die (mysql_error());
                    while($r_busca_img=mysql_fetch_array($acao_busca_img)) 
                        {
                        $imagem = "";
                        $del_imagem_p=$r_busca_img['imagem_p']; 
                        $imagem = $del_imagem_p;
                        }
                    
                    $acao_busca_rel=mysql_query("select * from imagens_rel where rel_idgal='".$ver_id."'") or die (mysql_error());
                    $conta_busca_rel=mysql_num_rows($acao_busca_rel);
                    //echo"conta_busca_rel - $conta_busca_rel<BR>";
                    while($r_busca_rel=mysql_fetch_array($acao_busca_rel)) 
                        {
                        $rel_idnot=$r_busca_rel['rel_idnot'];
                        }
                    $acao_busca_not=mysql_query("select * from noticias where id='".$rel_idnot."'") or die (mysql_error());
                    $conta_busca_not=mysql_num_rows($acao_busca_not);
                    //echo"conta_busca_not - $conta_busca_not<BR>";
                    while($r_busca_not=mysql_fetch_array($acao_busca_not)) 
                        {
                        $noticia_data_d=utf8_encode($r_busca_not['data_d']); 
                        $noticia_data_m=utf8_encode($r_busca_not['data_m']); 
                        $noticia_data_a=utf8_encode($r_busca_not['data_a']); 
                        $noticia_titulo=utf8_encode($r_busca_not['titulo']); 
                        }
                    ?>
                    <tr>
                    <td><?php echo $noticia_data_d; ?>/<?php echo $noticia_data_m; ?>/<?php echo $noticia_data_a; ?><br /><?php echo $noticia_titulo; ?></td>
                    <td><a href="imagem.php?ver=on&ver_id=<?php echo $ver_id; ?>#form_upload"><img src="imagens/<?php echo $imagem; ?>" alt="<?php echo $titulo; ?>" style="max-width:200px"></a></td>
                    <td class="linkpadrao"><a href="imagem.php?ver=on&id=<?php echo $ver_id; ?>&form_upload=on"><?php echo $titulo; ?></a></td>
                    <td><a href="imagens.php?remover_galeria=on&set_id=<?php echo $ver_id; ?>&pagina=<?php echo $pagina; ?>" onClick='return Certeza();'><i class="fa fa-close"></i></a> | <a href="imagem.php?alterar=on&ver_id=<?php echo $ver_id; ?>&pagina=<?php echo $pagina; ?>"><i class="fa fa-edit"></i></a></td>
                    </tr>
                    <?php
                    }
                ?>
                    
                    
                </tbody>
              </table>
            </div>
            
                  <div class="form-group" style="text-align:right; padding-top:30px; padding-right:10px">
                    <a href="imagem.php" type="button" class="btn btn-success">Incluir</a>
				  </div>
                                  
                <?php
                  }
                else
                    {
                    ?>
                    <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 15px;"><p><img src="images/icon-erro.png" style="float: left; margin-right: 10px;">Nenhuma galeria encontrada no banco de dados.</p></div>
                    
                    <div class="form-group" style="text-align:right; padding-top:30px; padding-right:10px">
                    <a href="imagem.php" type="button" class="btn btn-success">Incluir</a>
                    </div>
                    <?php
                    }
            ?>

          </div>
        </div>
        
                <?php
            if ($total_stringNot > 0)
                {
                ?>
                <ul class="pagination clearfix block">
                  <?php
                  echo $prev_link;
                  echo $painel;
                  echo $next_link;
                  ?>
                </ul>
                <?php
                }
            ?>
                    
      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

    <?php
	include"inc/footer.php";
	?>

  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/extentions/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/onScreen/jquery.onscreen.js"></script>
  <script src="vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/ui/accordion.js"></script>
  <script src="scripts/ui/animate.js"></script>
  <script src="scripts/ui/link-transition.js"></script>
  <script src="scripts/ui/panel-controls.js"></script>
  <script src="scripts/ui/preloader.js"></script>
  <script src="scripts/ui/toggle.js"></script>
  <script src="scripts/urban-constants.js"></script>
  <script src="scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  <!-- page level scripts -->
  <script src="vendor/d3/d3.min.js"></script>
  <script src="vendor/rickshaw/rickshaw.min.js"></script>
  <script src="vendor/flot/jquery.flot.js"></script>
  <script src="vendor/flot/jquery.flot.resize.js"></script>
  <script src="vendor/flot/jquery.flot.categories.js"></script>
  <script src="vendor/flot/jquery.flot.pie.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="scripts/pages/dashboard.js"></script>
  <!-- /initialize page scripts -->
  
  <script src="scripts/validate/jquery.maskedinput.min.js"></script>
    <script src="scripts/validate/jquery.validate.min.js"></script>
    <script src="scripts/validate/additional-methods.js"></script>
    
    <script src="scripts/function-certeza.js"></script>
    
    <script>
    jQuery(function($){
    //defina as máscaras de seus campos, o 9 indica um caracter numérico qualquer
    $("#fone").mask("(99)9999-9999?9");
    $("#date").mask("99/99/9999");
    $("#data").mask("99/99/9999");
    $("#cep").mask("99999-999");
    });
    </script>

</body>

</html>
