<?php
function ValidaCPF($str) 
    {
    if (!preg_match('|^(\d{3})\.?(\d{3})\.?(\d{3})\-?(\d{2})$|', $str, $matches))
    return false;

    array_shift($matches);
    $str = implode('', $matches);

    for ($i=0; $i < 10; $i++)
    if ($str == str_repeat($i, 11))
        return false;

    for ($t=9; $t < 11; $t++) 
        {
        for ($d=0, $c=0; $c < $t; $c++)
        $d += $str[$c] * ($t + 1 - $c);

        $d = ((10 * $d) % 11) % 10;

        if ($str[$c] != $d)
        return false;
        }

    return $str;
    }
   
// if (!is_cpf($cpf)) //cpf inválido 
?>