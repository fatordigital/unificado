<?php
function truncate($str, $len, $etc='') 
	{
	$end = array(' ', '.', ',', ';', ':', '!', '?');

	if (strlen($str) <= $len)
		return $str;

	if (!in_array($str{$len - 1}, $end) && !in_array($str{$len}, $end))
		while (--$len && !in_array($str{$len - 1}, $end));

	return rtrim(substr($str, 0, $len)).$etc;
	}
?>