<?php
#$imagem - source of image
#$dpi - resolution to convert E.g.: 72dpi or 300dpi

function px2cm($image, $dpi) {
    #Create a new image from file or URL
    $img = ImageCreateFromJpeg($image);

    #Get image width / height
    $x = ImageSX($img);
    $y = ImageSY($img);
    
    #Convert to centimeter
    $h = $x * 2.54 / $dpi;
    $l = $y * 2.54 / $dpi;
    
    #Format a number with grouped thousands
    $h = number_format($h, 2, ',', ' ');
    $l = number_format($l, 2, ',', ' ');
    
    #add size unit
    $px2cm[] = $h."cm";
    $px2cm[] = $l."cm";
    
    #return array w values
    #$px2cm[0] = X
    #$px2cm[1] = Y    
    return $px2cm;
}

$image = "C:\\inetpub\\wwwroot\\lab\\trata_img\\l0gik.jpg";
$dpi = 300;

$result = px2cm($image, $dpi);

print ($result[0]." x ".$result[1]);
?>