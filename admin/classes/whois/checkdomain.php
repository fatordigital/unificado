<?php

include "whoisphp.php";
// $allowed should be a list of authorised callers seperated by commas, If you don't care leave it blank
// Be careful if you call this in a new browser window as the referer may be blank.

$allowed="";

// The following line gets the url variables.
if (!empty($HTTP_GET_VARS)) while(list($name, $value) = each($HTTP_GET_VARS)) $$name = $value;
?>

<html>
<head>
<title>Whois demo - Domain Availability</title>
</head>
<body bgcolor="#FFFFCC">
<form name="DomainForm" method="get" action="checkdomain.php">
    <font face="Arial"><span style="font-size:10pt;">This demo shows how a domain
    name can be checked for availability.<br>Freeform entry using all supported
    domain extensions. (e.g. domainname.com)<br></span></font><br><input type="text" value="<?php print($domain);?>" name="domain" maxlength="63">
<input type="submit" name="button1" value="Check">
</form>

<?php
if (($domain!="") && ($DoWhois==""))
{
	$Reg="*"; // Putting a * in $Reg flags to whoisphp not too bother getting full whois data just availablity.
    // With some registry databases this can speed up the request. (optional)
    $i=whoisphp($domain,$domext,$Reg);
    if ($i==4)
    {
      print("<font face=\"Arial\">Sorry but you are not allowed access to this page</font>");
    }
    if ($i==5)
    {
      print("<font face=\"Arial\">Could not contact registry for $domext domains</font>");
    }
    if ($i==0)
    {
      print("<font face=\"Arial\">$domain$domext is available for registration</font>");
    }
    if ($i==6)
    {
      print("<font face=\"Arial\">$domain$domext is available for registration at a premium cost of $".$Reg[count($Reg)-1]."</font>");
    }
    if ($i==1)
    {
      print("<font face=\"Arial\">Sorry but $domain$domext is already registered<BR><a href=\"checkdomain.php?domain=$domain&domext=$domext&DoWhois=1\">Click here to see who registered it</a></font>");
    }
    if ($i==2)
    {
      print("<font face=\"Arial\">Domain extension $domext not recognised</font>");
    }
    if ($i==3)
    {
      print("<font face=\"Arial\">$domain$domext is not a valid domain name</font>");
    }
}

if (($domain!="") && ($DoWhois=="1"))
{
    $i=whoisphp($domain,$domext,$Reg);
    if ($i==4)
    {
      print("<font face=\"Arial\">Sorry but you are not allowed access to this page</font>");
    }
    if ($i==5)
    {
      print("<font face=\"Arial\">Could not contact registry for $domext domains</font>");
    }
    if (($i==0) || ($i==1))
    {
      print("<font face=\"Arial\">Registration details for $domain$domext<BR><BR></font>");
      print("-----------------------------------------------------------------<BR>");
      for ($k=0;$k<count($Reg);$k++)
      {
        print ("$Reg[$k]<BR>");
      }
      print("-----------------------------------------------------------------<BR>");
    }
    if ($i==2)
    {
      print("<font face=\"Arial\">Domain extension $domext not recognised</font>");
    }
    if ($i==3)
    {
      print("<font face=\"Arial\">$domain$domext is not a valid domain name</font>");
    }
}
?>
</body>
</html>