<?php
include"inc/functions.php";
include"functions/verifica-logado.php";
include"functions/setform-configuracoes.php";

$acao_busca_id=mysql_query("select * from configuracoes where id='1'") or die (mysql_error());
while($r_busca_id=mysql_fetch_array($acao_busca_id)) 
    {
    $rec_sender=utf8_encode($r_busca_id['sender']);
    $rec_nome=$r_busca_id['nome'];
    $rec_reply=$r_busca_id['reply'];
    $rec_smtp=utf8_encode($r_busca_id['smtp']);
    $rec_porta=utf8_encode($r_busca_id['porta']);
    $rec_autentica=utf8_encode($r_busca_id['autentica']);
    $rec_user=utf8_encode($r_busca_id['user']);
    $rec_senha=utf8_encode($r_busca_id['senha']);
    }
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Gerenciador de Conteúdo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="vendor/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="vendor/summernote/dist/summernote.css">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="styles/roboto.css">
  <link rel="stylesheet" href="styles/font-awesome.css">
  <link rel="stylesheet" href="styles/panel.css">
  <link rel="stylesheet" href="styles/feather.css">
  <link rel="stylesheet" href="styles/animate.css">
  <link rel="stylesheet" href="styles/urban.css">
  <link rel="stylesheet" href="styles/urban.skins.css">
  <!-- endbuild -->
  
  <link rel="stylesheet" href="styles/styles-adicional.css">
  
    <script type="text/javascript" src="classes/ckeditor_simples/ckeditor.js"></script>
    <script src="classes/ckeditor_simples/sample/sample.js" type="text/javascript"></script>
    <link href="classes/ckeditor_simples/sample/sample.css" rel="stylesheet" type="text/css" />

</head>

<body>

  <div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

      <div class="brand">

        <!-- logo -->
        <div class="brand-logo">
          <img src="images/logo.png" height="19" alt="">
        </div>
        <!-- /logo -->

        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
          <span></span>
  	      <span></span>
  	      <span></span>
  	      <span></span>
        </a>
        <!-- /toggle small sidebar menu -->

      </div>

      <?php
	  include"inc/menu-nav.php";
	  ?>

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

      <?php
	  include"inc/header.php";
	  ?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
        <?php
        if ($sucesso_alterar == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Configurações alteradas com sucesso!</strong></p></div>
            <?php
            }
        if ($erro_form == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-erro.png" style="float: left; margin-right: 10px;"><strong><span style="color:red">Erro - revise os campos obrigatórios!</span></strong></p></div>
            <?php
            }
        
        //echo"site - ".$_SESSION['unificado']['session_site']."<BR>";
        ?>
          <div class="panel-heading">
                <ol class="breadcrumb">
                  <li>
                    <a href="./">Home</a>
                  </li>
                  <li class="active ng-binding">Configurações</li>
                </ol>
          </div>
          
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
                <form class="form-horizontal bordered-group" enctype="multipart/form-data" role="form" action="configuracoes.php" method="post">

                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_sender == 1)) { ?> style="color:red"<?php } ?>>E-mail sender</label>
                    <div class="col-sm-9">
                      <input id="sender" name="sender" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $sender; } else { echo $rec_sender; } ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_nome == 1)) { ?> style="color:red"<?php } ?>>Nome do remetente</label>
                    <div class="col-sm-9">
                      <input id="nome" name="nome" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $nome; } else { echo $rec_nome; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_smtp == 1)) { ?> style="color:red"<?php } ?>>Endereço do servidor SMTP</label>
                    <div class="col-sm-9">
                      <input id="smtp" name="smtp" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $smtp; } else { echo $rec_smtp; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_porta == 1)) { ?> style="color:red"<?php } ?>>Porta do servidor SMTP</label>
                    <div class="col-sm-9">
                      <input id="porta" name="porta" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $porta; } else { echo $rec_porta; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_user == 1)) { ?> style="color:red"<?php } ?>>Username para autenticação</label>
                    <div class="col-sm-9">
                      <input id="user" name="user" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $user; } else { echo $rec_user; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_senha == 1)) { ?> style="color:red"<?php } ?>>Senha</label>
                    <div class="col-sm-9">
                      <input id="senha" name="senha" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $senha; } else { echo $rec_senha; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_autentica == 1)) { ?> style="color:red"<?php } ?>>SMTP requer autenticação</label>
                    <div class="col-sm-9">
                      <div class="checkbox">
                        <label>
                          <input id="autentica" name="autentica" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($autentica == 1) { echo" checked=\"checked\""; }} else { if ($rec_autentica == 1) { echo" checked=\"checked\""; }} ?>>Sim</label>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_reply == 1)) { ?> style="color:red"<?php } ?>>E-mail para retorno</label>
                    <div class="col-sm-9">
                      <input id="reply" name="reply" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $reply; } else { echo $rec_reply; } ?>">
                    </div>
                  </div>
                  
                  <?php
                  //include"inc/form-sites.php";
                  ?>

                  <div class="form-group" style="text-align:right; padding-top:10px; padding-right:10px">
                    <button type="submit" name="alterar_configuracoes" value="on" class="btn btn-warning">Salvar</button>
		  </div>
                  
                </form>
              </div>
            </div>
          </div>
          
        </div>
        
                    
      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

    <?php
	include"inc/footer.php";
	?>

  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/extentions/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/onScreen/jquery.onscreen.js"></script>
  <script src="vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/ui/accordion.js"></script>
  <script src="scripts/ui/animate.js"></script>
  <script src="scripts/ui/link-transition.js"></script>
  <script src="scripts/ui/panel-controls.js"></script>
  <script src="scripts/ui/preloader.js"></script>
  <script src="scripts/ui/toggle.js"></script>
  <script src="scripts/urban-constants.js"></script>
  <script src="scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  <!-- page level scripts -->
  <script src="vendor/d3/d3.min.js"></script>
  <script src="vendor/rickshaw/rickshaw.min.js"></script>
  <script src="vendor/flot/jquery.flot.js"></script>
  <script src="vendor/flot/jquery.flot.resize.js"></script>
  <script src="vendor/flot/jquery.flot.categories.js"></script>
  <script src="vendor/flot/jquery.flot.pie.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="scripts/pages/dashboard.js"></script>
  <!-- /initialize page scripts -->
  
  <script src="scripts/validate/jquery.maskedinput.min.js"></script>
    <script src="scripts/validate/jquery.validate.min.js"></script>
    <script src="scripts/validate/additional-methods.js"></script>
    
    <script src="scripts/function-certeza.js"></script>
    
    <script>
    jQuery(function($){
    //defina as máscaras de seus campos, o 9 indica um caracter numérico qualquer
    $("#fone").mask("(99)9999-9999?9");
    $("#date").mask("99/99/9999");
    $("#data").mask("99/99/9999");
    $("#cep").mask("99999-999");
    });
    </script>

</body>

</html>