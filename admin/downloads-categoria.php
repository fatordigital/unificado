<?php
include"inc/functions.php";
include"functions/verifica-logado.php";
include"functions/setform-downloads.php";

if (($ver == "on") or ($alterar == "on"))
    {
    //echo"entrou1<BR>";
    if ($ver_id != "")
        {
        $acao_busca_id=mysql_query("select * from downloads_categorias where id='".$ver_id."'") or die (mysql_error());
        }
    else
        {
        $acao_busca_id=mysql_query("select * from downloads_categorias where id='".$set_id."'") or die (mysql_error());
        }
    $conta_busca_id=mysql_num_rows($acao_busca_id);
    //echo"conta_busca_id - $conta_busca_id<BR>";
    while($r_busca_id=mysql_fetch_array($acao_busca_id)) 
        {
        $rec_posicao=$r_busca_id['posicao'];
        $rec_titulo=$r_busca_id['titulo'];
        $rec_texto=$r_busca_id['texto'];
        }
    }
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Gerenciador de Conteúdo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="vendor/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="vendor/summernote/dist/summernote.css">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="styles/roboto.css">
  <link rel="stylesheet" href="styles/font-awesome.css">
  <link rel="stylesheet" href="styles/panel.css">
  <link rel="stylesheet" href="styles/feather.css">
  <link rel="stylesheet" href="styles/animate.css">
  <link rel="stylesheet" href="styles/urban.css">
  <link rel="stylesheet" href="styles/urban.skins.css">
  <!-- endbuild -->
  
    <script type="text/javascript" src="classes/ckeditor_simples/ckeditor.js"></script>
    <script src="classes/ckeditor_simples/sample/sample.js" type="text/javascript"></script>
    <link href="classes/ckeditor_simples/sample/sample.css" rel="stylesheet" type="text/css" />

</head>

<body>

  <div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

      <div class="brand">

        <!-- logo -->
        <div class="brand-logo">
          <img src="images/logo.png" height="19" alt="">
        </div>
        <!-- /logo -->

        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
          <span></span>
  	      <span></span>
  	      <span></span>
  	      <span></span>
        </a>
        <!-- /toggle small sidebar menu -->

      </div>

      <?php
	  include"inc/menu-nav.php";
	  ?>

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

      <?php
	  include"inc/header.php";
	  ?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
        <?php
        if ($sucesso_cadastro == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Categoria cadastrada com sucesso!</strong></p></div>
            <?php
            }
        if ($sucesso_alterar == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Categoria alterada com sucesso!</strong></p></div>
            <?php
            }
        if ($sucesso_remover == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Categoria removida com sucesso!</strong></p></div>
            <?php
            }
        if ($erro_form == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-erro.png" style="float: left; margin-right: 10px;"><strong><span style="color:red">Erro - revise os campos obrigatórios!</span></strong></p></div>
            <?php
            }
        ?>
          <div class="panel-heading border">
                <ol class="breadcrumb" style="margin-bottom:5px">
                  <li>
                    <a href="./">Home</a>
                  </li>
                  <li>
                    <a href="downloads.php">Downloads</a>
                  </li>
                  <li>
                    <a href="downloads-categorias.php">Categorias</a>
                  </li>
                  <li class="active ng-binding"><?php if ($alterar == "on") { echo"Alterar"; } elseif ($ver == "on") { echo"Ver"; } else { ?>Cadastrar<?php } ?> Categoria</li>
                </ol>
          </div>
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
                <form class="form-horizontal bordered-group" enctype="multipart/form-data" role="form" action="downloads-categoria.php" method="post">
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_posicao == 1)) { ?> style="color:red"<?php } ?>>Posição</label>
                    <div class="col-sm-10">
                      <input <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> id="posicao" name="posicao" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $posicao; } elseif (($ver == "on") or ($alterar == "on")) { echo utf8_encode($rec_posicao); } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_titulo == 1)) { ?> style="color:red"<?php } ?>>Título</label>
                    <div class="col-sm-10">
                      <input <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> id="titulo" name="titulo" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $titulo; } elseif (($ver == "on") or ($alterar == "on")) { echo utf8_encode($rec_titulo); } ?>">
                    </div>
                  </div>
                    
                    <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_texto == 1)) { ?> style="color:red"<?php } ?>>Texto</label>
                    <div class="col-sm-10">
                  <textarea <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> class="ckeditor" style="width:100%" id="texto" name="texto" rows="10"><?php if ($erro_form == 1) { echo $texto; } else { echo utf8_encode($rec_texto); } ?></textarea> 
			<script type="text/javascript">
			//<![CDATA[

				// This call can be placed at any point after the
				// <textarea>, or inside a <head><script> in a
				// window.onload event handler.

				// Replace the <textarea id="editor"> with an CKEditor
				// instance, using default configurations.
				CKEDITOR.replace( 'texto' );

			//]]>
			</script>
                  </div>
                  </div>
                  
                  <?php
                  //include"inc/form-sites.php";
                  ?>
                                  
                  <div class="form-group" style="text-align:right; padding-top:10px; padding-right:10px">
                  <?php 
                  if ($alterar == "on")
                        { 
                        ?>
                        <button type="submit" name="alterar_categoria" value="on" class="btn btn-warning">Salvar</button>
                        <button type="button" onclick="return Certeza(),location.href='downloads-categorias.php?remover_categoria=on&set_id=<?php echo $ver_id; ?>'" name="remover_categoria" value="on" class="btn btn-danger">Excluir</button>
                        <button type="button" onclick="location.href='downloads-categorias.php'" class="btn btn-group">Cancelar</button>
                        <?php if ($ver_id != "") { ?><input type="hidden" name="set_id" value="<?php echo $ver_id; ?>"></input><?php } else { ?><input type="hidden" name="set_id" value="<?php echo $set_id; ?>"></input><?php } ?>
                        <input type="hidden" name="alterar" value="on"></input>
                        <?php 
                        } 
                    elseif ($ver == "on")
                        { 
                        ?>
                        <button type="button" onclick="location.href='downloads-categoria.php?alterar=on&ver_id=<?php echo $ver_id; ?>'" class="btn btn-warning">Alterar</button>
                        <button type="button" onclick="return Certeza(),location.href='downloads-categorias.php?remover_categoria=on&set_id=<?php echo $ver_id; ?>'" name="remover_categoria" value="on" class="btn btn-danger">Excluir</button>
                        <input type="hidden" name="set_id" value="<?php echo $ver_id; ?>"></input>
                        <?php 
                        } 
                    
                    else
                        {
                        ?>
                        <button type="submit" name="cadastrar_categoria" value="on" class="btn btn-success">Incluir</button>
                        <?php
                        }
                    ?>
                    </div>
                  
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /main area -->

    </div>
    <!-- /content panel -->

    <?php
	include"inc/footer.php";
	?>

  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/extentions/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/onScreen/jquery.onscreen.js"></script>
  <script src="vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/ui/accordion.js"></script>
  <script src="scripts/ui/animate.js"></script>
  <script src="scripts/ui/link-transition.js"></script>
  <script src="scripts/ui/panel-controls.js"></script>
  <script src="scripts/ui/preloader.js"></script>
  <script src="scripts/ui/toggle.js"></script>
  <script src="scripts/urban-constants.js"></script>
  <script src="scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  <!-- page level scripts -->
  <script src="vendor/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js"></script>
  <script src="vendor/summernote/dist/summernote.min.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="scripts/pages/form-wysiwyg.js"></script>
  <!-- /initialize page scripts -->
  
  <script src="scripts/validate/jquery.maskedinput.min.js"></script>
    <script src="scripts/validate/jquery.validate.min.js"></script>
    <script src="scripts/validate/additional-methods.js"></script>
    
    <script src="scripts/function-certeza.js"></script>
    
    <script>
    jQuery(function($){
    //defina as máscaras de seus campos, o 9 indica um caracter numérico qualquer
    $("#fone").mask("(99)9999-9999?9");
    $("#date").mask("99/99/9999");
    $("#data").mask("99/99/9999");
    $("#cep").mask("99999-999");
    });
    </script>
  
</body>

</html>
