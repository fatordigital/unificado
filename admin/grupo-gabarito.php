<?php
include"inc/functions.php";
include"functions/verifica-logado.php";
include"functions/setform-grupo-unificado.php";

$acao_busca_id=mysql_query("select * from conheca where id='".$_SESSION['unificado']['session_site']."'") or die (mysql_error());
while($r_busca_id=mysql_fetch_array($acao_busca_id)) 
    {
    $rec_texto_peq5=utf8_encode($r_busca_id['texto_peq5']);
    $rec_texto5=utf8_encode($r_busca_id['texto5']);
    $rec_imagem5=$r_busca_id['imagem5'];
    $rec_imagem52=$r_busca_id['imagem52'];
    }
//echo"rec_texto1 - $rec_texto1<BR>";
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Gerenciador de Conteúdo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="styles/climacons-font.css">
  <link rel="stylesheet" href="vendor/rickshaw/rickshaw.min.css">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="styles/roboto.css">
  <link rel="stylesheet" href="styles/font-awesome.css">
  <link rel="stylesheet" href="styles/panel.css">
  <link rel="stylesheet" href="styles/feather.css">
  <link rel="stylesheet" href="styles/animate.css">
  <link rel="stylesheet" href="styles/urban.css">
  <link rel="stylesheet" href="styles/urban.skins.css">
  <!-- endbuild -->
  
  <link rel="stylesheet" href="styles/styles-adicional.css">
  
    <script type="text/javascript" src="classes/ckeditor_simples/ckeditor.js"></script>
    <script src="classes/ckeditor_simples/sample/sample.js" type="text/javascript"></script>
    <link href="classes/ckeditor_simples/sample/sample.css" rel="stylesheet" type="text/css" />

</head>

<body>

  <div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

      <div class="brand">

        <!-- logo -->
        <div class="brand-logo">
          <img src="images/logo.png" height="19" alt="">
        </div>
        <!-- /logo -->

        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
          <span></span>
  	      <span></span>
  	      <span></span>
  	      <span></span>
        </a>
        <!-- /toggle small sidebar menu -->

      </div>

      <?php
	  include"inc/menu-nav.php";
	  ?>

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

      <?php
	  include"inc/header.php";
	  ?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
        <?php
        if ($sucesso_alterar == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Texto alterado com sucesso!</strong></p></div>
            <?php
            }
        if ($erro_form == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-erro.png" style="float: left; margin-right: 10px;"><strong><span style="color:red">Erro - revise os campos obrigatórios!</span></strong></p></div>
            <?php
            }
        
        //echo"site - ".$_SESSION['unificado']['session_site']."<BR>";
        ?>
          <div class="panel-heading">
                <ol class="breadcrumb">
                  <li>
                    <a href="./">Home</a>
                  </li>
                  <li>Empresa</li>
                  <li class="active ng-binding">Chamadas</li>
                </ol>
          </div>
          
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
                <form class="form-horizontal bordered-group" enctype="multipart/form-data" role="form" action="grupo-gabarito.php" method="post">
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_texto_peq5 == 1)) { ?> style="color:red"<?php } ?>>Texto curto</label>
                    <div class="col-sm-9">
                  <textarea <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> class="ckeditor" style="width:100%" id="texto_peq5" name="texto_peq5" rows="10"><?php if ($erro_form == 1) { echo $texto_peq5; } else { echo utf8_encode($rec_texto_peq5); } ?></textarea> 
			<script type="text/javascript">
			//<![CDATA[

				// This call can be placed at any point after the
				// <textarea>, or inside a <head><script> in a
				// window.onload event handler.

				// Replace the <textarea id="editor"> with an CKEditor
				// instance, using default configurations.
				CKEDITOR.replace( 'texto_peq5' );

			//]]>
			</script>
                  </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_texto5 == 1)) { ?> style="color:red"<?php } ?>>Texto longo</label>
                    <div class="col-sm-9">
                  <textarea <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> class="ckeditor" style="width:100%" id="texto5" name="texto5" rows="10"><?php if ($erro_form == 1) { echo $texto5; } else { echo utf8_encode($rec_texto5); } ?></textarea> 
			<script type="text/javascript">
			//<![CDATA[

				// This call can be placed at any point after the
				// <textarea>, or inside a <head><script> in a
				// window.onload event handler.

				// Replace the <textarea id="editor"> with an CKEditor
				// instance, using default configurations.
				CKEDITOR.replace( 'texto5' );

			//]]>
			</script>
                  </div>
                  </div>
                  
                  <?php
                      //echo"rec_imagem0 - $rec_imagem<BR>";
                      
                      //echo"entrou alterar<BR>";
                        //echo"erro_form - $erro_form<BR>";
                        if ($erro_form == 1)
                            {
                            //echo"entrou erro form<BR>";
                            //echo"gravou_arquivo - $gravou_arquivo<BR>";
                            //echo"file_temp - $file_temp<BR>";
                            //echo"rec_imagem1 - $rec_imagem1<BR>";
                            if (($gravou_arquivo5 == 1) && ($file_temp5 != ""))
                                {
                                ?>
                                <div class="form-group">
                                <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file5 == 1) or ($erro_extensao5 == 1))) { ?> style="color:red"<?php } ?>>Imagem Gabarito</label>
                                <div class="col-sm-9">
                                <img src="imagens/<?php echo $file_temp5; ?>" style="max-width:300px">
                                </div>
                                </div>
                                
                                <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem5" name="del_imagem5" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem5 == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                                <?php
                                }
                            elseif ($rec_imagem5 != "")
                                {
                                ?>
                                <div class="form-group">
                                <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file5 == 1) or ($erro_extensao5 == 1))) { ?> style="color:red"<?php } ?>>Imagem Gabarito</label>
                                <div class="col-sm-9">
                                <img src="imagens/<?php echo $rec_imagem5; ?>" style="max-width:300px">
                                </div>
                                </div>
                                
                                <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem5" name="del_imagem5" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem5 == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                                <?php
                                }
                            }
                        elseif ($rec_imagem5 != "")
                            {
                            ?>
                            <div class="form-group">
                            <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file5 == 1) or ($erro_extensao5 == 1))) { ?> style="color:red"<?php } ?>>Imagem Gabarito</label>
                            <div class="col-sm-9">
                            <img src="imagens/<?php echo $rec_imagem5; ?>" style="max-width:300px">
                            </div>
                            </div>
                            <div class="form-group">                            
                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-9">
                              <div class="checkbox">
                                <label>
                                  <input id="del_imagem5" name="del_imagem5" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem5 == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                              </div>
                            </div>
                            </div>
                            <?php
                            }
                        
                        
                    ?>
                    <div class="form-group">
                    <label class="col-sm-3 control-label" style="<?php if (($erro_form == 1) && (($erro_file5 == 1) or ($erro_extensao5 == 1))) { ?> color:red<?php } ?>"><?php if (($file_temp5 != "") or ($rec_imagem5 != "")) { ?>Alterar <?php } ?>Imagem Gabarito</label>
                    <div class="col-sm-9">
                    <input id="file5" name="file5" type="file" style="margin-top:10px;">
                    </div>
                    </div>
                    
                    <?php
                      //echo"rec_imagem0 - $rec_imagem<BR>";
                      
                      //echo"entrou alterar<BR>";
                        //echo"erro_form - $erro_form<BR>";
                        if ($erro_form == 1)
                            {
                            //echo"entrou erro form<BR>";
                            //echo"gravou_arquivo - $gravou_arquivo<BR>";
                            //echo"file_temp - $file_temp<BR>";
                            //echo"rec_imagem1 - $rec_imagem1<BR>";
                            if (($gravou_arquivo52 == 1) && ($file_temp52 != ""))
                                {
                                ?>
                                <div class="form-group">
                                <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file52 == 1) or ($erro_extensao52 == 1))) { ?> style="color:red"<?php } ?>>Imagem 2</label>
                                <div class="col-sm-9">
                                <img src="imagens/<?php echo $file_temp52; ?>" style="max-width:300px">
                                </div>
                                </div>
                                
                                <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem52" name="del_imagem52" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem52 == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                                <?php
                                }
                            elseif ($rec_imagem52 != "")
                                {
                                ?>
                                <div class="form-group">
                                <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file52 == 1) or ($erro_extensao52 == 1))) { ?> style="color:red"<?php } ?>>Imagem 2</label>
                                <div class="col-sm-9">
                                <img src="imagens/<?php echo $rec_imagem52; ?>" style="max-width:300px">
                                </div>
                                </div>
                                
                                <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem52" name="del_imagem52" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem52 == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                                <?php
                                }
                            }
                        elseif ($rec_imagem52 != "")
                            {
                            ?>
                            <div class="form-group">
                            <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file52 == 1) or ($erro_extensao52 == 1))) { ?> style="color:red"<?php } ?>>Imagem 2</label>
                            <div class="col-sm-9">
                            <img src="imagens/<?php echo $rec_imagem52; ?>" style="max-width:300px">
                            </div>
                            </div>
                            <div class="form-group">                            
                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-9">
                              <div class="checkbox">
                                <label>
                                  <input id="del_imagem52" name="del_imagem52" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem52 == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                              </div>
                            </div>
                            </div>
                            <?php
                            }
                        
                        
                    ?>
                    <div class="form-group">
                    <label class="col-sm-3 control-label" style="<?php if (($erro_form == 1) && (($erro_file2 == 1) or ($erro_extensao2 == 1))) { ?> color:red<?php } ?>"><?php if (($file_temp52 != "") or ($rec_imagem52 != "")) { ?>Alterar <?php } ?>Imagem 2</label>
                    <div class="col-sm-9">
                    <input id="file52" name="file52" type="file" style="margin-top:10px;">
                    </div>
                    </div>

                  <?php
                  //include"inc/form-sites.php";
                  ?>

                  <div class="form-group" style="text-align:right; padding-top:10px; padding-right:10px">
                    <button type="submit" name="alterar5" value="on" class="btn btn-warning">Salvar</button>
		  </div>
                  
                </form>
              </div>
            </div>
          </div>
          
        </div>
        
                    
      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

    <?php
	include"inc/footer.php";
	?>

  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/extentions/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/onScreen/jquery.onscreen.js"></script>
  <script src="vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/ui/accordion.js"></script>
  <script src="scripts/ui/animate.js"></script>
  <script src="scripts/ui/link-transition.js"></script>
  <script src="scripts/ui/panel-controls.js"></script>
  <script src="scripts/ui/preloader.js"></script>
  <script src="scripts/ui/toggle.js"></script>
  <script src="scripts/urban-constants.js"></script>
  <script src="scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  <!-- page level scripts -->
  <script src="vendor/d3/d3.min.js"></script>
  <script src="vendor/rickshaw/rickshaw.min.js"></script>
  <script src="vendor/flot/jquery.flot.js"></script>
  <script src="vendor/flot/jquery.flot.resize.js"></script>
  <script src="vendor/flot/jquery.flot.categories.js"></script>
  <script src="vendor/flot/jquery.flot.pie.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="scripts/pages/dashboard.js"></script>
  <!-- /initialize page scripts -->
  
  <script src="scripts/validate/jquery.maskedinput.min.js"></script>
    <script src="scripts/validate/jquery.validate.min.js"></script>
    <script src="scripts/validate/additional-methods.js"></script>
    
    <script src="scripts/function-certeza.js"></script>
    
    <script>
    jQuery(function($){
    //defina as máscaras de seus campos, o 9 indica um caracter numérico qualquer
    $("#fone").mask("(99)9999-9999?9");
    $("#date").mask("99/99/9999");
    $("#data").mask("99/99/9999");
    $("#cep").mask("99999-999");
    });
    </script>

</body>

</html>
