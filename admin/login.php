<?php
$pagina_login = 1;
include"inc/functions.php";
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Gerenciador de Conteúdo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="styles/roboto.css">
  <link rel="stylesheet" href="styles/font-awesome.css">
  <link rel="stylesheet" href="styles/panel.css">
  <link rel="stylesheet" href="styles/feather.css">
  <link rel="stylesheet" href="styles/animate.css">
  <link rel="stylesheet" href="styles/urban.css">
  <link rel="stylesheet" href="styles/urban.skins.css">
  <!-- endbuild -->

</head>

<body>

  <div class="app layout-fixed-header bg-white usersession">
    <div class="full-height">
      <div class="center-wrapper">
        <div class="center-content">
          <div class="row no-margin">
            <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
              <form role="form" name="login" method="post" action="index.php" class="form-layout">
                <div class="text-center mb15">
                  <img src="images/logo-cinza.png" width="100%" />
                </div>
                <p class="text-center mb30">Bem vindo ao WeManager, faça seu login.</p>
                
                <?php
                if ($_SESSION['unificado']['erro_login'] == 1)
                    {
                    ?>
                    <p class="text-center mb30" style="color: red"><br /><br />Os dados informados não conferem.</p>
                    <?php
                    $erro_login = 1;
                    
                    $_SESSION['unificado']['erro_login'] = "";
                    unset($_SESSION['unificado']['erro_login']);
                    
                    $t_email = $_SESSION['unificado']['login_email'];
                    $_SESSION['unificado']['login_email'] = "";
                    unset($_SESSION['unificado']['login_email']);
                    
                    $t_senha = $_SESSION['unificado']['login_senha'];
                    $_SESSION['unificado']['login_senha'] = "";
                    unset($_SESSION['unificado']['login_senha']);
                    }
                
                if ($_SESSION['unificado']['derrubado'] == 1)
                    {
                    /*?>
                    <p class="text-center mb30" style="color: red"><br /><br />E necessário fazer login novamente no sistema.</p>
                    <?php*/
                    $_SESSION['unificado']['derrubado'] = "";
                    unset($_SESSION['unificado']['derrubado']);
                    }
                ?>
                <div class="form-inputs">
                  <input type="email" name="login-form-username" id="login-form-username" class="form-control input-lg" placeholder="Email" value="<?php if ($erro_login == 1) { echo $t_email; } ?>">
                  <input type="password" name="login-form-password" id="login-form-password" class="form-control input-lg" placeholder="Senha" value="<?php if ($erro_login == 1) { echo $t_senha; } ?>">
                </div>
                <button class="btn btn-success btn-block btn-lg mb15" type="submit">
                  <span>Login</span>
                </button>
                <p style="text-align:center">
                  <a href="forgot.php">Esqueceu a senha?</a>
                </p>
                <input type="hidden" name="enviar_login" value="on"></input>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/extentions/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/onScreen/jquery.onscreen.js"></script>
  <script src="vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/ui/accordion.js"></script>
  <script src="scripts/ui/animate.js"></script>
  <script src="scripts/ui/link-transition.js"></script>
  <script src="scripts/ui/panel-controls.js"></script>
  <script src="scripts/ui/preloader.js"></script>
  <script src="scripts/ui/toggle.js"></script>
  <script src="scripts/urban-constants.js"></script>
  <script src="scripts/extentions/lib.js"></script>
  <!-- endbuild -->
</body>

</html>
