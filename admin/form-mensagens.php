<?php
include"inc/functions.php";
include"functions/verifica-logado.php";
if ($form == 1)
    {
    if ($_SESSION['unificado']['session_site'] == 1)
        {
        $in_id = 1;    
        }
    if ($_SESSION['unificado']['session_site'] == 2)
        {
        $in_id = 2;    
        }
    if ($_SESSION['unificado']['session_site'] == 3)
        {
        $in_id = 3;    
        }
    if ($_SESSION['unificado']['session_site'] == 4)
        {
        $in_id = 4;    
        }
    }
if ($form == 2)
    {
     if ($_SESSION['unificado']['session_site'] == 1)
        {
        $in_id = 5;    
        }
    if ($_SESSION['unificado']['session_site'] == 2)
        {
        $in_id = 6;    
        }
    if ($_SESSION['unificado']['session_site'] == 3)
        {
        $in_id = 7;    
        }
    if ($_SESSION['unificado']['session_site'] == 4)
        {
        $in_id = 8;    
        }
    }
if ($form == 3)
    {
     if ($_SESSION['unificado']['session_site'] == 1)
        {
        $in_id = 9;    
        }
    if ($_SESSION['unificado']['session_site'] == 2)
        {
        $in_id = 10;    
        }
    if ($_SESSION['unificado']['session_site'] == 3)
        {
        $in_id = 11;    
        }
    if ($_SESSION['unificado']['session_site'] == 4)
        {
        $in_id = 12;    
        }
    }
include"functions/setform-form-mensagens.php";
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Gerenciador de Conteúdo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="styles/climacons-font.css">
  <link rel="stylesheet" href="vendor/rickshaw/rickshaw.min.css">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="styles/roboto.css">
  <link rel="stylesheet" href="styles/font-awesome.css">
  <link rel="stylesheet" href="styles/panel.css">
  <link rel="stylesheet" href="styles/feather.css">
  <link rel="stylesheet" href="styles/animate.css">
  <link rel="stylesheet" href="styles/urban.css">
  <link rel="stylesheet" href="styles/urban.skins.css">
  <!-- endbuild -->
  
  <link rel="stylesheet" href="styles/styles-adicional.css">
  
  <script type="text/javascript" src="classes/ckeditor_simples/ckeditor.js"></script>
    <script src="classes/ckeditor_simples/sample/sample.js" type="text/javascript"></script>
    <link href="classes/ckeditor_simples/sample/sample.css" rel="stylesheet" type="text/css" />

</head>

<body>

  <div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

      <div class="brand">

        <!-- logo -->
        <div class="brand-logo">
          <img src="images/logo.png" height="19" alt="">
        </div>
        <!-- /logo -->

        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
          <span></span>
  	      <span></span>
  	      <span></span>
  	      <span></span>
        </a>
        <!-- /toggle small sidebar menu -->

      </div>

      <?php
	  include"inc/menu-nav.php";
	  ?>

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

      <?php
	  include"inc/header.php";
	  ?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
          <div class="panel-heading">
                <ol class="breadcrumb">
                  <li>
                    <a href="./">Home</a>
                  </li>
                  <li>Formulários</li>
                  <li>
                  <?php
                  if ($form == 1)
                    {
                    ?>
                    Contato
                    <?php
                    }
                if ($form == 2)
                    {
                    ?>
                    Matrículas
                    <?php
                    }
                if ($form == 3)
                    {
                    ?>
                    Trabalhe conosco
                    <?php
                    }
                ?>
                
                  </li>
                  <li class="active ng-binding">Mensagens</li>
                </ol>
          </div>
          <div class="panel-body">
          
            <?php
            if (($pagina=="") or ($pagina == 0)) $pagina = 1;  
            else $pagina=$pagina;
            
            //echo"pagina - $pagina<BR>"; 
            
            $num_por_pagina = 10;
            
            //echo"tabela_site - $tabela_site<BR>";
            $acao_not_ver="select * from form_mensagens 
            where site_id='".$_SESSION['unificado']['session_site']."' && form='".$form."'
            order by id desc";
            //echo"acao_not_ver - $acao_not_ver<BR>";
            
            $consulta = $acao_not_ver;
            $res1 = mysql_query($consulta);
            $total=mysql_num_rows($res1);
            $total = $total/$num_por_pagina;
            
            $prev = $pagina - 1;
            $next = $pagina + 1;	
            if ($pagina > 1) 
                {
                $prev_link = "<li><a href=\"form-mensagens.php?form=".$form."pagina=".$prev."\">←</a></li>";
                } 
            else 
                { 
                $prev_link = "<li><a href=\"javascript:;\">←</a></li>";
                }
            if ($total > $pagina) 
                {
                $next_link = "<li><a href=\"form-mensagens.php?form=".$form."pagina=".$next."\">→</a></li>";
                } 
            else 
                { 
                $next_link = "<li><a href=\"javascript:;\">→</a></li>";
                }
            $total = ceil($total);
            $painel = "";
            for ($x=1; $x<=$total; $x++) 
                {
                if ($x==$pagina) 
                    {
                    $painel .= "<li><a style=\"background-color: #eee\" href=\"javascript:;\">".$x."</a></li>";
                    } 
                else 
                    {
                    $painel .= "<li><a href=\"form-mensagens.php?form=".$form."&pagina=".$x."\">".$x."</a></li>";
                    }
                }
                    
            $conta_acao_not_ver = $total;
            //echo" conta_acao_not_ver - $conta_acao_not_ver<BR>";
            
            $inicio = 1;
            $primeiro_registro = ($pagina*$num_por_pagina) - $num_por_pagina;
                    
            $inicio_txt = 0;
            $query_stringNot=mysql_query($acao_not_ver." LIMIT ".$primeiro_registro.", ".$num_por_pagina."");
            $total_stringNot=mysql_num_rows($query_stringNot);
            if ($total_stringNot > 0)
                {
                ?>
                <div class="table-responsive">
                <table class="table table-bordered table-striped mb0">
                <thead>
                  <tr>
                    <th>Data</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Telefone</th>
                    <th>Ações</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Data</th>
                    <th>Nome</th>
                    <th>Email</th>
                    <th>Telefone</th>
                    <th>Ações</th>
                </tfoot>
                <tbody>
                <?php
                $inot = 1;
                $i = 1;
                while($array=mysql_fetch_array($query_stringNot)) 
                    {
                    $ver_id=$array['id'];
                    $data_d=$array['data_d']; 
                    $data_m=$array['data_m']; 
                    $data_a=$array['data_a'];  
                    $hora=$array['hora']; 
                    $nome=utf8_encode($array['nome']); 
                    $email=utf8_encode($array['email']); 
                    $telefone=utf8_encode($array['telefone']); 
                    $comentario=utf8_encode($array['comentario']); 
                    $arquivo=$array['arquivo']; 
                    ?>
                    <tr>
                    <td class="linkpadrao"><a href="javascript:;" data-toggle="modal" data-target=".bs-modal-sm<?php echo $ver_id; ?>"><?php echo $data_d; ?>/<?php echo $data_m; ?>/<?php echo $data_a; ?></a></td>
                    <td class="linkpadrao"><a href="javascript:;" data-toggle="modal" data-target=".bs-modal-sm<?php echo $ver_id; ?>"><?php echo $nome; ?></a></td>
                    <td class="linkpadrao"><a href="javascript:;" data-toggle="modal" data-target=".bs-modal-sm<?php echo $ver_id; ?>"><?php echo $email; ?></a></td>
                    <td class="linkpadrao"><a href="javascript:;" data-toggle="modal" data-target=".bs-modal-sm<?php echo $ver_id; ?>"><?php if ($telefone == "") { echo"(-)"; } else { echo $telefone; } ?></a></td>
                    <td><a href="form-mensagens.php?form=<?php echo $form; ?>&pagina=<?php echo $pagina; ?>&set_id=<?php echo $ver_id; ?>&remover=on" onClick='return Certeza();'><i class="fa fa-close"></i></a>
                    
                    <div class="modal bs-modal-sm<?php echo $ver_id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          <h4 class="modal-title">Comentários</h4>
                        </div>
                        <div class="modal-body">
                          <div class="row mb25">
                            <div class="col-xs-12">
                              <p><?php echo $comentario; ?></p>
                            </div>
                          </div>
                          
                        </div>
                        <?php
                        if ($form == 3)
                            {
                            ?>
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          <h4 class="modal-title">Currículo</h4>
                        </div>
                        <div class="modal-body">
                          <div class="row mb25">
                            <div class="col-xs-12">
                              <p><a href="http://www.unificadoz.uni5.net/curriculos/<?php echo $arquivo; ?>" target="_blank"><?php echo $arquivo; ?></a></p>
                            </div>
                          </div>
                          
                        </div>
                            <?php
                            }
                        ?>
                        <div class="modal-footer no-border">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                      </div>
                    </div>
                    </div>
                    
                    </td>
                    </tr>
                    <?php
                    }
                ?> 
                </tbody>
                </table>
                </div>
                <?php
                }
            
            else
                {
                ?>
                <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 15px;"><p><img src="images/icon-erro.png" style="float: left; margin-right: 10px;">Nenhuma mensagem encontrada no banco de dados.</p></div>
                <?php
                }
            ?>

          </div>
        </div>
                <?php
            if ($total_stringNot > 0)
                {
                ?>
                <ul class="pagination clearfix block">
                  <?php
                  echo $prev_link;
                  echo $painel;
                  echo $next_link;
                  ?>
                </ul>
                <?php
                }
            ?>
      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

    <?php
	include"inc/footer.php";
	?>

  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/extentions/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/onScreen/jquery.onscreen.js"></script>
  <script src="vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/ui/accordion.js"></script>
  <script src="scripts/ui/animate.js"></script>
  <script src="scripts/ui/link-transition.js"></script>
  <script src="scripts/ui/panel-controls.js"></script>
  <script src="scripts/ui/preloader.js"></script>
  <script src="scripts/ui/toggle.js"></script>
  <script src="scripts/urban-constants.js"></script>
  <script src="scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  <!-- page level scripts -->
  <script src="vendor/d3/d3.min.js"></script>
  <script src="vendor/rickshaw/rickshaw.min.js"></script>
  <script src="vendor/flot/jquery.flot.js"></script>
  <script src="vendor/flot/jquery.flot.resize.js"></script>
  <script src="vendor/flot/jquery.flot.categories.js"></script>
  <script src="vendor/flot/jquery.flot.pie.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="scripts/pages/dashboard.js"></script>
  <!-- /initialize page scripts -->
  
  <script src="scripts/validate/jquery.maskedinput.min.js"></script>
    <script src="scripts/validate/jquery.validate.min.js"></script>
    <script src="scripts/validate/additional-methods.js"></script>
    
    <script src="scripts/function-certeza.js"></script>
    
    <script>
    jQuery(function($){
    //defina as máscaras de seus campos, o 9 indica um caracter numérico qualquer
    $("#fone").mask("(99)9999-9999?9");
    $("#date").mask("99/99/9999");
    $("#data").mask("99/99/9999");
    $("#cep").mask("99999-999");
    });
    </script>

</body>

</html>
