<?php
include"inc/functions.php";
include"functions/verifica-logado.php";
include"functions/setform-grupo-unificado.php";

$acao_busca_id=mysql_query("select * from conheca where id='".$_SESSION['unificado']['session_site']."'") or die (mysql_error());
while($r_busca_id=mysql_fetch_array($acao_busca_id)) 
    {
    $rec_texto_peq1=utf8_encode($r_busca_id['texto_peq1']);
    $rec_texto1=utf8_encode($r_busca_id['texto1']);
    $rec_imagem1=$r_busca_id['imagem1'];
    $rec_imagem12=$r_busca_id['imagem12'];
    }
//echo"rec_texto1 - $rec_texto1<BR>";
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Gerenciador de Conteúdo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="styles/climacons-font.css">
  <link rel="stylesheet" href="vendor/rickshaw/rickshaw.min.css">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="styles/roboto.css">
  <link rel="stylesheet" href="styles/font-awesome.css">
  <link rel="stylesheet" href="styles/panel.css">
  <link rel="stylesheet" href="styles/feather.css">
  <link rel="stylesheet" href="styles/animate.css">
  <link rel="stylesheet" href="styles/urban.css">
  <link rel="stylesheet" href="styles/urban.skins.css">
  <!-- endbuild -->
  
  <link rel="stylesheet" href="styles/styles-adicional.css">
  
    <script type="text/javascript" src="classes/ckeditor_simples/ckeditor.js"></script>
    <script src="classes/ckeditor_simples/sample/sample.js" type="text/javascript"></script>
    <link href="classes/ckeditor_simples/sample/sample.css" rel="stylesheet" type="text/css" />

</head>

<body>

  <div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

      <div class="brand">

        <!-- logo -->
        <div class="brand-logo">
          <img src="images/logo.png" height="19" alt="">
        </div>
        <!-- /logo -->

        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
          <span></span>
  	      <span></span>
  	      <span></span>
  	      <span></span>
        </a>
        <!-- /toggle small sidebar menu -->

      </div>

      <?php
	  include"inc/menu-nav.php";
	  ?>

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

      <?php
	  include"inc/header.php";
	  ?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
        <?php
        if ($sucesso_alterar == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Texto alterado com sucesso!</strong></p></div>
            <?php
            }
        if ($erro_form == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-erro.png" style="float: left; margin-right: 10px;"><strong><span style="color:red">Erro - revise os campos obrigatórios!</span></strong></p></div>
            <?php
            }
        
        //echo"site - ".$_SESSION['unificado']['session_site']."<BR>";
        ?>
          <div class="panel-heading">
                <ol class="breadcrumb">
                  <li>
                    <a href="./">Home</a>
                  </li>
                  <li>Empresa</li>
                  <li class="active ng-binding">Chamadas</li>
                </ol>
          </div>
          
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
                <form class="form-horizontal bordered-group" enctype="multipart/form-data" role="form" action="grupo-unificado-z.php" method="post">
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_texto_peq1 == 1)) { ?> style="color:red"<?php } ?>>Texto curto</label>
                    <div class="col-sm-9">
                  <textarea <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> class="ckeditor" style="width:100%" id="texto_peq1" name="texto_peq1" rows="10"><?php if ($erro_form == 1) { echo $texto_peq1; } else { echo utf8_encode($rec_texto_peq1); } ?></textarea> 
			<script type="text/javascript">
			//<![CDATA[

				// This call can be placed at any point after the
				// <textarea>, or inside a <head><script> in a
				// window.onload event handler.

				// Replace the <textarea id="editor"> with an CKEditor
				// instance, using default configurations.
				CKEDITOR.replace( 'texto_peq1' );

			//]]>
			</script>
                  </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && ($erro_texto1 == 1)) { ?> style="color:red"<?php } ?>>Texto longo</label>
                    <div class="col-sm-9">
                  <textarea <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> class="ckeditor" style="width:100%" id="texto1" name="texto1" rows="10"><?php if ($erro_form == 1) { echo $texto1; } else { echo utf8_encode($rec_texto1); } ?></textarea> 
			<script type="text/javascript">
			//<![CDATA[

				// This call can be placed at any point after the
				// <textarea>, or inside a <head><script> in a
				// window.onload event handler.

				// Replace the <textarea id="editor"> with an CKEditor
				// instance, using default configurations.
				CKEDITOR.replace( 'texto1' );

			//]]>
			</script>
                  </div>
                  </div>
                  
                  <?php
                      //echo"rec_imagem0 - $rec_imagem<BR>";
                      
                      //echo"entrou alterar<BR>";
                        //echo"erro_form - $erro_form<BR>";
                        if ($erro_form == 1)
                            {
                            //echo"entrou erro form<BR>";
                            //echo"gravou_arquivo - $gravou_arquivo<BR>";
                            //echo"file_temp - $file_temp<BR>";
                            //echo"rec_imagem1 - $rec_imagem1<BR>";
                            if (($gravou_arquivo == 1) && ($file_temp != ""))
                                {
                                ?>
                                <div class="form-group">
                                <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file == 1) or ($erro_extensao == 1))) { ?> style="color:red"<?php } ?>>Imagem 1</label>
                                <div class="col-sm-9">
                                <img src="imagens/<?php echo $file_temp; ?>" style="max-width:300px">
                                </div>
                                </div>
                                
                                <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem" name="del_imagem" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                                <?php
                                }
                            elseif ($rec_imagem1 != "")
                                {
                                ?>
                                <div class="form-group">
                                <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file == 1) or ($erro_extensao == 1))) { ?> style="color:red"<?php } ?>>Imagem 1</label>
                                <div class="col-sm-9">
                                <img src="imagens/<?php echo $rec_imagem1; ?>" style="max-width:300px">
                                </div>
                                </div>
                                
                                <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem" name="del_imagem" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                                <?php
                                }
                            }
                        elseif ($rec_imagem1 != "")
                            {
                            ?>
                            <div class="form-group">
                            <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file == 1) or ($erro_extensao == 1))) { ?> style="color:red"<?php } ?>>Imagem 1</label>
                            <div class="col-sm-9">
                            <img src="imagens/<?php echo $rec_imagem1; ?>" style="max-width:300px">
                            </div>
                            </div>
                            <div class="form-group">                            
                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-9">
                              <div class="checkbox">
                                <label>
                                  <input id="del_imagem" name="del_imagem" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                              </div>
                            </div>
                            </div>
                            <?php
                            }
                        
                        
                    ?>
                    <div class="form-group">
                    <label class="col-sm-3 control-label" style="<?php if (($erro_form == 1) && (($erro_file == 1) or ($erro_extensao == 1))) { ?> color:red<?php } ?>"><?php if (($file_temp != "") or ($rec_imagem1 != "")) { ?>Alterar <?php } ?>Imagem 1</label>
                    <div class="col-sm-9">
                    <input id="file" name="file" type="file" style="margin-top:10px;">
                    </div>
                    </div>
                    
                    
                    <?php
                      //echo"rec_imagem0 - $rec_imagem<BR>";
                      
                      //echo"entrou alterar<BR>";
                        //echo"erro_form - $erro_form<BR>";
                        if ($erro_form == 1)
                            {
                            //echo"entrou erro form<BR>";
                            //echo"gravou_arquivo - $gravou_arquivo<BR>";
                            //echo"file_temp - $file_temp<BR>";
                            //echo"rec_imagem1 - $rec_imagem1<BR>";
                            if (($gravou_arquivo12 == 1) && ($file_temp12 != ""))
                                {
                                ?>
                                <div class="form-group">
                                <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file12 == 1) or ($erro_extensao12 == 1))) { ?> style="color:red"<?php } ?>>Imagem 2</label>
                                <div class="col-sm-9">
                                <img src="imagens/<?php echo $file_temp12; ?>" style="max-width:300px">
                                </div>
                                </div>
                                
                                <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem12" name="del_imagem12" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem12 == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                                <?php
                                }
                            elseif ($rec_imagem12 != "")
                                {
                                ?>
                                <div class="form-group">
                                <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file12 == 1) or ($erro_extensao12 == 1))) { ?> style="color:red"<?php } ?>>Imagem 2</label>
                                <div class="col-sm-9">
                                <img src="imagens/<?php echo $rec_imagem12; ?>" style="max-width:300px">
                                </div>
                                </div>
                                
                                <div class="form-group">
                                <label class="col-sm-3 control-label"></label>
                                <div class="col-sm-9">
                                  <div class="checkbox">
                                    <label>
                                      <input id="del_imagem12" name="del_imagem12" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem12 == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                                  </div>
                                </div>
                                </div>
                                <?php
                                }
                            }
                        elseif ($rec_imagem12 != "")
                            {
                            ?>
                            <div class="form-group">
                            <label class="col-sm-3 control-label"<?php if (($erro_form == 1) && (($erro_file12 == 1) or ($erro_extensao12 == 1))) { ?> style="color:red"<?php } ?>>Imagem 2</label>
                            <div class="col-sm-9">
                            <img src="imagens/<?php echo $rec_imagem12; ?>" style="max-width:300px">
                            </div>
                            </div>
                            <div class="form-group">                            
                            <label class="col-sm-3 control-label"></label>
                            <div class="col-sm-9">
                              <div class="checkbox">
                                <label>
                                  <input id="del_imagem12" name="del_imagem12" type="checkbox" value="1"<?php if ($erro_form == 1) { if ($del_imagem12 == 1) { echo" checked=\"checked\""; }} ?>>remover imagem</label>
                              </div>
                            </div>
                            </div>
                            <?php
                            }
                        
                        
                    ?>
                    <div class="form-group">
                    <label class="col-sm-3 control-label" style="<?php if (($erro_form == 1) && (($erro_file12 == 1) or ($erro_extensao12 == 1))) { ?> color:red<?php } ?>"><?php if (($file_temp12 != "") or ($rec_imagem12 != "")) { ?>Alterar <?php } ?>Imagem 2</label>
                    <div class="col-sm-9">
                    <input id="file12" name="file12" type="file" style="margin-top:10px;">
                    </div>
                    </div>
                    
                  
                  

                  <?php
                  //include"inc/form-sites.php";
                  ?>

                  <div class="form-group" style="text-align:right; padding-top:10px; padding-right:10px">
                    <button type="submit" name="alterar" value="on" class="btn btn-warning">Salvar</button>
		  </div>
                  
                </form>
              </div>
            </div>
          </div>
          
        </div>
        
                    
      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

    <?php
	include"inc/footer.php";
	?>

  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/extentions/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/onScreen/jquery.onscreen.js"></script>
  <script src="vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/ui/accordion.js"></script>
  <script src="scripts/ui/animate.js"></script>
  <script src="scripts/ui/link-transition.js"></script>
  <script src="scripts/ui/panel-controls.js"></script>
  <script src="scripts/ui/preloader.js"></script>
  <script src="scripts/ui/toggle.js"></script>
  <script src="scripts/urban-constants.js"></script>
  <script src="scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  <!-- page level scripts -->
  <script src="vendor/d3/d3.min.js"></script>
  <script src="vendor/rickshaw/rickshaw.min.js"></script>
  <script src="vendor/flot/jquery.flot.js"></script>
  <script src="vendor/flot/jquery.flot.resize.js"></script>
  <script src="vendor/flot/jquery.flot.categories.js"></script>
  <script src="vendor/flot/jquery.flot.pie.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="scripts/pages/dashboard.js"></script>
  <!-- /initialize page scripts -->
  
  <script src="scripts/validate/jquery.maskedinput.min.js"></script>
    <script src="scripts/validate/jquery.validate.min.js"></script>
    <script src="scripts/validate/additional-methods.js"></script>
    
    <script src="scripts/function-certeza.js"></script>
    
    <script>
    jQuery(function($){
    //defina as máscaras de seus campos, o 9 indica um caracter numérico qualquer
    $("#fone").mask("(99)9999-9999?9");
    $("#date").mask("99/99/9999");
    $("#data").mask("99/99/9999");
    $("#cep").mask("99999-999");
    });
    </script>

</body>

</html>
