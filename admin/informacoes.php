<?php
include"inc/functions.php";
include"functions/verifica-logado.php";
include"functions/setform-informacoes.php";

$acao_busca_id=mysql_query("select * from informacoes where id='".$_SESSION['unificado']['session_site']."'") or die (mysql_error());
while($r_busca_id=mysql_fetch_array($acao_busca_id)) 
    {
    $rec_endereco=utf8_encode($r_busca_id['endereco']);
    $rec_complemento=utf8_encode($r_busca_id['complemento']);
    $rec_cep=$r_busca_id['cep'];
    $rec_cidade=utf8_encode($r_busca_id['cidade']);
    $rec_estado=utf8_encode($r_busca_id['estado']);
    $rec_telefone=utf8_encode($r_busca_id['telefone']);
    $rec_email=utf8_encode($r_busca_id['email']);
    $rec_mapa_latitude=$r_busca_id['mapa_latitude'];
    $rec_mapa_longitude=$r_busca_id['mapa_longitude'];
    $rec_email=utf8_encode($r_busca_id['email']);
    $rec_facebook=utf8_encode($r_busca_id['facebook']);
    $rec_twitter=utf8_encode($r_busca_id['twitter']);
    $rec_instagram=utf8_encode($r_busca_id['instagram']);
    $rec_gplus=utf8_encode($r_busca_id['gplus']);
    $rec_linkedin=utf8_encode($r_busca_id['linkedin']);
    $rec_flickr=utf8_encode($r_busca_id['flickr']);
    $rec_tumblr=utf8_encode($r_busca_id['tumblr']);
    $rec_youtube=utf8_encode($r_busca_id['youtube']);
    $rec_vimeo=utf8_encode($r_busca_id['vimeo']);
    $rec_texto_rodape=utf8_encode($r_busca_id['texto_rodape']);
    }
?>
<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Gerenciador de Conteúdo</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

  <!-- page level plugin styles -->
  <link rel="stylesheet" href="styles/climacons-font.css">
  <link rel="stylesheet" href="vendor/rickshaw/rickshaw.min.css">
  <!-- /page level plugin styles -->

  <!-- build:css({.tmp,app}) styles/app.min.css -->
  <link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
  <link rel="stylesheet" href="styles/roboto.css">
  <link rel="stylesheet" href="styles/font-awesome.css">
  <link rel="stylesheet" href="styles/panel.css">
  <link rel="stylesheet" href="styles/feather.css">
  <link rel="stylesheet" href="styles/animate.css">
  <link rel="stylesheet" href="styles/urban.css">
  <link rel="stylesheet" href="styles/urban.skins.css">
  <!-- endbuild -->
  
  <link rel="stylesheet" href="styles/styles-adicional.css">
  
    <script type="text/javascript" src="classes/ckeditor_simples/ckeditor.js"></script>
    <script src="classes/ckeditor_simples/sample/sample.js" type="text/javascript"></script>
    <link href="classes/ckeditor_simples/sample/sample.css" rel="stylesheet" type="text/css" />

</head>

<body>

  <div class="app layout-fixed-header">

    <!-- sidebar panel -->
    <div class="sidebar-panel offscreen-left">

      <div class="brand">

        <!-- logo -->
        <div class="brand-logo">
          <img src="images/logo.png" height="19" alt="">
        </div>
        <!-- /logo -->

        <!-- toggle small sidebar menu -->
        <a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
          <span></span>
  	      <span></span>
  	      <span></span>
  	      <span></span>
        </a>
        <!-- /toggle small sidebar menu -->

      </div>

      <?php
	  include"inc/menu-nav.php";
	  ?>

    </div>
    <!-- /sidebar panel -->

    <!-- content panel -->
    <div class="main-panel">

      <?php
	  include"inc/header.php";
	  ?>

      <!-- main area -->
      <div class="main-content">
        <div class="panel mb25">
        <?php
        if ($sucesso_alterar == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-ok.png" style="float: left; margin-right: 10px;"><strong>Texto alterado com sucesso!</strong></p></div>
            <?php
            }
        if ($erro_form == 1)
            {
            ?>
            <div style="padding-top: 20px; margin-bottom: 10px; margin-left: 30px;"><p><img src="images/icon-erro.png" style="float: left; margin-right: 10px;"><strong><span style="color:red">Erro - revise os campos obrigatórios!</span></strong></p></div>
            <?php
            }
        
        //echo"site - ".$_SESSION['unificado']['session_site']."<BR>";
        ?>
          <div class="panel-heading">
                <ol class="breadcrumb">
                  <li>
                    <a href="./">Home</a>
                  </li>
                  <li class="active ng-binding">Informações</li>
                </ol>
          </div>
          
          <div class="panel-body">
            <div class="row no-margin">
              <div class="col-lg-12">
                <form class="form-horizontal bordered-group" enctype="multipart/form-data" role="form" action="informacoes.php" method="post">

                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_endereco == 1)) { ?> style="color:red"<?php } ?>>Endereço</label>
                    <div class="col-sm-10">
                      <input id="endereco" name="endereco" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $endereco; } else { echo $rec_endereco; } ?>">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_complemento == 1)) { ?> style="color:red"<?php } ?>>Complemento</label>
                    <div class="col-sm-10">
                      <input id="complemento" name="complemento" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $complemento; } else { echo $rec_complemento; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_cep == 1)) { ?> style="color:red"<?php } ?>>CEP</label>
                    <div class="col-sm-10">
                      <input id="cep" name="cep" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $cep; } else { echo $rec_cep; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_cidade == 1)) { ?> style="color:red"<?php } ?>>Cidade</label>
                    <div class="col-sm-10">
                      <input id="cidade" name="cidade" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $cidade; } else { echo $rec_cidade; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_estado == 1)) { ?> style="color:red"<?php } ?>>Estado</label>
                    <div class="col-sm-10">
                      <input id="estado" name="estado" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $estado; } else { echo $rec_estado; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_telefone == 1)) { ?> style="color:red"<?php } ?>>Telefone</label>
                    <div class="col-sm-10">
                      <input id="telefone" name="telefone" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $telefone; } else { echo $rec_telefone; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_email == 1)) { ?> style="color:red"<?php } ?>>E-mail</label>
                    <div class="col-sm-10">
                      <input id="email" name="email" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $email; } else { echo $rec_email; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_mapa_latitude == 1)) { ?> style="color:red"<?php } ?>>Mapa Latitude</label>
                    <div class="col-sm-10">
                      <input id="mapa_latitude" name="mapa_latitude" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $mapa_latitude; } else { echo $rec_mapa_latitude; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_mapa_longitude == 1)) { ?> style="color:red"<?php } ?>>Mapa longitude</label>
                    <div class="col-sm-10">
                      <input id="mapa_longitude" name="mapa_longitude" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $mapa_longitude; } else { echo $rec_mapa_longitude; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_facebook == 1)) { ?> style="color:red"<?php } ?>>Facebook</label>
                    <div class="col-sm-10">
                      <input id="facebook" name="facebook" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $facebook; } else { echo $rec_facebook; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_twitter == 1)) { ?> style="color:red"<?php } ?>>Twitter</label>
                    <div class="col-sm-10">
                      <input id="twitter" name="twitter" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $twitter; } else { echo $rec_twitter; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_instagram == 1)) { ?> style="color:red"<?php } ?>>Instagram</label>
                    <div class="col-sm-10">
                      <input id="instagram" name="instagram" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $instagram; } else { echo $rec_instagram; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_gplus == 1)) { ?> style="color:red"<?php } ?>>Google+</label>
                    <div class="col-sm-10">
                      <input id="gplus" name="gplus" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $gplus; } else { echo $rec_gplus; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_linkedin == 1)) { ?> style="color:red"<?php } ?>>Linkedin</label>
                    <div class="col-sm-10">
                      <input id="linkedin" name="linkedin" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $linkedin; } else { echo $rec_linkedin; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_flickr == 1)) { ?> style="color:red"<?php } ?>>Flickr</label>
                    <div class="col-sm-10">
                      <input id="flickr" name="flickr" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $flickr; } else { echo $rec_flickr; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_tumblr == 1)) { ?> style="color:red"<?php } ?>>Tumblr</label>
                    <div class="col-sm-10">
                      <input id="tumblr" name="tumblr" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $tumblr; } else { echo $rec_tumblr; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_youtube == 1)) { ?> style="color:red"<?php } ?>>Youtube</label>
                    <div class="col-sm-10">
                      <input id="youtube" name="youtube" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $youtube; } else { echo $rec_youtube; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_vimeo == 1)) { ?> style="color:red"<?php } ?>>Vimeo</label>
                    <div class="col-sm-10">
                      <input id="vimeo" name="vimeo" type="text" class="form-control input-rounded" value="<?php if ($erro_form == 1) { echo $vimeo; } else { echo $rec_vimeo; } ?>">
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label class="col-sm-2 control-label"<?php if (($erro_form == 1) && ($erro_texto_rodape == 1)) { ?> style="color:red"<?php } ?>>Texto rodapé</label>
                    <div class="col-sm-10">
                  <textarea <?php if ($ver == "on") { ?>disabled="disabled" <?php } ?> class="ckeditor" style="width:100%" id="texto_rodape" name="texto_rodape" rows="10"><?php if ($erro_form == 1) { echo $texto_rodape; } else { echo utf8_encode($rec_texto_rodape); } ?></textarea> 
			<script type="text/javascript">
			//<![CDATA[

				// This call can be placed at any point after the
				// <textarea>, or inside a <head><script> in a
				// window.onload event handler.

				// Replace the <textarea id="editor"> with an CKEditor
				// instance, using default configurations.
				CKEDITOR.replace( 'texto_rodape' );

			//]]>
			</script>
                  </div>
                  </div>

                  <?php
                  //include"inc/form-sites.php";
                  ?>

                  <div class="form-group" style="text-align:right; padding-top:10px; padding-right:10px">
                    <button type="submit" name="alterar_informacoes" value="on" class="btn btn-warning">Salvar</button>
		  </div>
                  
                </form>
              </div>
            </div>
          </div>
          
        </div>
        
                    
      </div>
      <!-- /main area -->
    </div>
    <!-- /content panel -->

    <?php
	include"inc/footer.php";
	?>

  </div>

  <!-- build:js({.tmp,app}) scripts/app.min.js -->
  <script src="scripts/extentions/modernizr.js"></script>
  <script src="vendor/jquery/dist/jquery.js"></script>
  <script src="vendor/bootstrap/dist/js/bootstrap.js"></script>
  <script src="vendor/jquery.easing/jquery.easing.js"></script>
  <script src="vendor/fastclick/lib/fastclick.js"></script>
  <script src="vendor/onScreen/jquery.onscreen.js"></script>
  <script src="vendor/jquery-countTo/jquery.countTo.js"></script>
  <script src="vendor/perfect-scrollbar/js/perfect-scrollbar.jquery.js"></script>
  <script src="scripts/ui/accordion.js"></script>
  <script src="scripts/ui/animate.js"></script>
  <script src="scripts/ui/link-transition.js"></script>
  <script src="scripts/ui/panel-controls.js"></script>
  <script src="scripts/ui/preloader.js"></script>
  <script src="scripts/ui/toggle.js"></script>
  <script src="scripts/urban-constants.js"></script>
  <script src="scripts/extentions/lib.js"></script>
  <!-- endbuild -->

  <!-- page level scripts -->
  <script src="vendor/d3/d3.min.js"></script>
  <script src="vendor/rickshaw/rickshaw.min.js"></script>
  <script src="vendor/flot/jquery.flot.js"></script>
  <script src="vendor/flot/jquery.flot.resize.js"></script>
  <script src="vendor/flot/jquery.flot.categories.js"></script>
  <script src="vendor/flot/jquery.flot.pie.js"></script>
  <!-- /page level scripts -->

  <!-- initialize page scripts -->
  <script src="scripts/pages/dashboard.js"></script>
  <!-- /initialize page scripts -->
  
  <script src="scripts/validate/jquery.maskedinput.min.js"></script>
    <script src="scripts/validate/jquery.validate.min.js"></script>
    <script src="scripts/validate/additional-methods.js"></script>
    
    <script src="scripts/function-certeza.js"></script>
    
    <script>
    jQuery(function($){
    //defina as máscaras de seus campos, o 9 indica um caracter numérico qualquer
    $("#fone").mask("(99)9999-9999?9");
    $("#date").mask("99/99/9999");
    $("#data").mask("99/99/9999");
    $("#cep").mask("99999-999");
    });
    </script>

</body>

</html>
