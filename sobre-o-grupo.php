<?php
$acao=mysql_query("select * from empresa_sobre where id='1'") or die (mysql_error());
while($r=mysql_fetch_array($acao)) 
    {
    $rec_texto=utf8_encode($r['texto']);
	
	$rec_texto = str_replace("<hr />","<div style=\"padding-top:20px; margin-bottom:20px; border-bottom:1px dotted rgba(0,0,0,.3)\"></div>",$rec_texto);
	$rec_texto = str_replace("<h2>","<h2 class=\"upper\">",$rec_texto);
	$rec_texto = str_replace("<h3>","<h4>",$rec_texto);
	$rec_texto = str_replace("</h3>","</h4>",$rec_texto);
    }
?>
<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="pt"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	
	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title>Grupo Unificado</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta itemprop="name" content="Grupo Unificado">
	<meta name="title" content="Grupo Unificado" />
	<meta http-equiv="content-language" content="pt-br" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-store" />
	<meta http-equiv="refresh" content="none" />
	<meta name="reply-to" content="contato@unificado.com.br">
	<meta name="generator" content="Adobe Dreamweaver Macromedia 6.0">
    <meta itemprop="description" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">
    <meta name="keywords" content="grupo unficado, unificado, unificado z, unificado med, curso revisão, curso maio, curso agosto, pré-vestibular, enem, vestibular, cursinho pré-vestibular, colégio unificado, colégio" />
    <meta itemprop="image" content="http://www.unificado.com.br/img/meta-imagem.jpg">
	<meta name="abstract" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">    
	<meta name="author" content="WE MAKE | Marketing Digital" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="general" />
	<meta name="copyright" content="Copyright Grupo Unificado 2016. All Rights Reserved." />    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.unificado.com.br/grupo-unificado" />
    <meta property="og:image" content="http://www.unificado.com.br/img/meta-imagem.jpg" />
    <meta property="og:title" content="Grupo Unificado"/>
    <meta property="og:description" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">
    <meta property="og:site_name" content="Grupo Unificado" />
    <meta property="og:author" content="WE MAKE Marketing Digital" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    
	<!-- Google Analytics Settings -->
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-72130153-1', 'auto');
      ga('send', 'pageview');
    
    </script>

	<!-- CSS -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/animate.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />

</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class="">Sobre o Grupo</h1>
			<div class="breadcrumbs">
				<a href="file:///Macintosh HD/Applications/XAMPP/xamppfiles/htdocs/unificado">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<a href="/grupo-unificado">Grupo Unificado</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Sobre o Grupo</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->

	
	<!-- Intro Banner -->
	<section class="content_section">
		<div class="container row_spacer2">
			<div class="container">
				<div class="content clearfix">
                    <div class="col-md-8">
                    
						<?php
						echo $rec_texto;
						?>
                        					
                        <div class="clearfix"></div>
                        <div class="divider"></div>
                        <div class="line"></div>
                        <a class="btn_c color4 large_btn" href="/grupo-unificado/historia">
								<span class="btn_c_ic_a"><i class="ico-arrow-forward"></i></span>
								<span class="btn_c_t">CONHEÇA O HISTÓRICO DO GRUPO UNIFICADO</span>
								<span class="btn_c_ic_b"><i class="ico-arrow-forward"></i></span>
					  </a>
                	</div>
                    <div class="col-md-4">
                    
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        
                        <div class="fb-page" data-href="https://www.facebook.com/unificado" data-tabs="timeline" data-height="470" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/unificado"><a href="https://www.facebook.com/unificado">Unificado Z</a></blockquote></div></div>
                    
                        
                    </div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Intro Banner -->
	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>