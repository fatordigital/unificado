<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="pt"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	
	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title>Sedes</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta itemprop="name" content="Grupo Unificado">
	<meta name="title" content="Sedes" />
	<meta http-equiv="content-language" content="pt-br" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-store" />
	<meta http-equiv="refresh" content="none" />
	<meta name="reply-to" content="contato@unificado.com.br">
	<meta name="generator" content="Adobe Dreamweaver Macromedia 6.0">
    <meta itemprop="description" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">
    <meta name="keywords" content="grupo unficado, unificado, unificado z, unificado med, curso revisão, curso maio, curso agosto, pré-vestibular, enem, vestibular, cursinho pré-vestibular, colégio unificado, colégio" />
    <meta itemprop="image" content="http://www.unificado.com.br/img/meta-imagem.jpg">
	<meta name="abstract" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">    
	<meta name="author" content="WE MAKE | Marketing Digital" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="general" />
	<meta name="copyright" content="Copyright Grupo Unificado 2016. All Rights Reserved." />    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.unificado.com.br/sedes" />
    <meta property="og:image" content="http://www.unificado.com.br/img/meta-imagem.jpg" />
    <meta property="og:title" content="Sedes"/>
    <meta property="og:description" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">
    <meta property="og:site_name" content="Grupo Unificado" />
    <meta property="og:author" content="WE MAKE Marketing Digital" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    
	<!-- Google Analytics Settings -->
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-72130153-1', 'auto');
      ga('send', 'pageview');
    
    </script>

	<!-- CSS -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/animate.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />


</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class="">Sedes</h1>
			<div class="breadcrumbs">
				<a href="/">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Sedes</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->
	
	<!-- Intro Banner -->
	<section class="content_section">
		<div class="container row_spacer2">
			<div class="container">
				<div class="content clearfix">
                    <div class="col-md-8">
                    
                    <h3 class="unit-title">Unificado PRÉ-VESTIBULAR</h3><br>
            			<ul class="unit-list">
                        <?php
						$i = 0;
						$acao=mysql_query("select * from empresa_sedes where site2='1' order by posicao asc,id asc") or die (mysql_error());
						$conta=mysql_num_rows($acao);
						if ($conta > 0)
							{
							while($r=mysql_fetch_array($acao)) 
								{
								$rec_titulo=utf8_encode($r['titulo']);
								$rec_subtitulo=utf8_encode($r['subtitulo']);
								$rec_subtitulo2=utf8_encode($r['subtitulo2']);
								$rec_subtitulo3=utf8_encode($r['subtitulo3']);
								
								$i = $i + 1;
								$it = $it + 1;
								?>
                            	<li style="margin-bottom:20px">
                                <span style="text-transform:uppercase; font-weight:700">
                                <?php echo $rec_titulo; ?></span><br/>
                                <?php echo $rec_subtitulo; ?><br>
                                <a href="mailto:<?php echo $rec_subtitulo3; ?>"><?php echo $rec_subtitulo3; ?></a> - <?php echo $rec_subtitulo2; ?><br/>
                                <a href="http://www.unificado.com.br/" target="_blank">http://www.unificado.com.br/</a><br>
                                </li>
                                <?php
								}
							}
						?>          
            			</ul>
                    	
                        <h3 class="unit-title">Unificado Med</h3>
            			<ul class="unit-list">
                        <?php
						$i = 0;
						$acao=mysql_query("select * from empresa_sedes where site3='1' order by posicao asc,id asc") or die (mysql_error());
						$conta=mysql_num_rows($acao);
						if ($conta > 0)
							{
							while($r=mysql_fetch_array($acao)) 
								{
								$rec_titulo=utf8_encode($r['titulo']);
								$rec_subtitulo=utf8_encode($r['subtitulo']);
								$rec_subtitulo2=utf8_encode($r['subtitulo2']);
								$rec_subtitulo3=utf8_encode($r['subtitulo3']);
								
								$i = $i + 1;
								$it = $it + 1;
								?>
                            	<li style="margin-bottom:20px">
                                <span style="text-transform:uppercase; font-weight:700">
                                <?php echo $rec_titulo; ?></span><br/>
                                <?php echo $rec_subtitulo; ?><br>
                                <a href="mailto:<?php echo $rec_subtitulo3; ?>"><?php echo $rec_subtitulo3; ?></a> - <?php echo $rec_subtitulo2; ?><br/>
                                <a href="http://www.unificadomed.com.br/" target="_blank">http://www.unificadomed.com.br/</a><br>
                                </li>
                                <?php
								}
							}
						?>
            		</ul>
                    
                    <h3 class="unit-title">Colégio Unificado</h3><br>
            		<ul class="unit-list">
                    <?php
					$i = 0;
					$acao=mysql_query("select * from empresa_sedes where site4='1' order by posicao asc,id asc") or die (mysql_error());
					$conta=mysql_num_rows($acao);
					if ($conta > 0)
						{
						while($r=mysql_fetch_array($acao)) 
							{
							$rec_titulo=utf8_encode($r['titulo']);
							$rec_subtitulo=utf8_encode($r['subtitulo']);
							$rec_subtitulo2=utf8_encode($r['subtitulo2']);
							$rec_subtitulo3=utf8_encode($r['subtitulo3']);
							
							$i = $i + 1;
							$it = $it + 1;
							?>
							<li style="margin-bottom:20px">
							<span style="text-transform:uppercase; font-weight:700">
							<?php echo $rec_titulo; ?></span><br/>
							<?php echo $rec_subtitulo; ?><br>
							<a href="mailto:<?php echo $rec_subtitulo3; ?>"><?php echo $rec_subtitulo3; ?></a> - <?php echo $rec_subtitulo2; ?><br/>
							<a href="http://www.unificadomed.com.br/" target="_blank">http://www.unificadomed.com.br/</a><br>
							</li>
							<?php
							}
						}
					?>
            		</ul>
                    <h3 class="unit-title">Colégio Unificado - EJA</h3><br>
            		<ul class="unit-list">
                    <?php
					$i = 0;
					$acao=mysql_query("select * from empresa_sedes where site5='1' order by posicao asc,id asc") or die (mysql_error());
					$conta=mysql_num_rows($acao);
					if ($conta > 0)
						{
						while($r=mysql_fetch_array($acao)) 
							{
							$rec_titulo=utf8_encode($r['titulo']);
							$rec_subtitulo=utf8_encode($r['subtitulo']);
							$rec_subtitulo2=utf8_encode($r['subtitulo2']);
							$rec_subtitulo3=utf8_encode($r['subtitulo3']);
							
							$i = $i + 1;
							$it = $it + 1;
							?>
							<li style="margin-bottom:20px">
							<span style="text-transform:uppercase; font-weight:700">
							<?php echo $rec_titulo; ?></span><br/>
							<?php echo $rec_subtitulo; ?><br>
							<a href="mailto:<?php echo $rec_subtitulo3; ?>"><?php echo $rec_subtitulo3; ?></a> - <?php echo $rec_subtitulo2; ?>
							<!--<br/><a href="http://www.unificadocolegio.com.br/" target="_blank">http://www.unificadocolegio.com.br/</a><br>-->
							</li>
							<?php
							}
						}
					?>
            		</ul>
                    
                    <h3 class="unit-title">Gabarito Pré-Vestibular</h3><br>
            		<ul class="unit-list">
                    <?php
					$i = 0;
					$acao=mysql_query("select * from empresa_sedes where site6='1' order by posicao asc,id asc") or die (mysql_error());
					$conta=mysql_num_rows($acao);
					if ($conta > 0)
						{
						while($r=mysql_fetch_array($acao)) 
							{
							$rec_titulo=utf8_encode($r['titulo']);
							$rec_subtitulo=utf8_encode($r['subtitulo']);
							$rec_subtitulo2=utf8_encode($r['subtitulo2']);
							$rec_subtitulo3=utf8_encode($r['subtitulo3']);
							
							$i = $i + 1;
							$it = $it + 1;
							?>
							<li style="margin-bottom:20px">
							<span style="text-transform:uppercase; font-weight:700">
							<?php echo $rec_titulo; ?></span><br/>
							<?php echo $rec_subtitulo; ?><br>
							<a href="mailto:<?php echo $rec_subtitulo3; ?>"><?php echo $rec_subtitulo3; ?></a> - <?php echo $rec_subtitulo2; ?>
							<br/><a href="http://www.gabaritovestibular.com.br/" target="_blank">http://www.gabaritovestibular.com.br/</a><br>
							</li>
							<?php
							}
						}
					?>
            		</ul>


                        <div class="clearfix"></div>
                        <div class="line" style="padding:15px"></div>

                	</div>
                    <div class="col-md-4">

                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        
                        <div class="fb-page" data-href="https://www.facebook.com/unificado" data-tabs="timeline" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/unificado"><a href="https://www.facebook.com/unificado">Unificado Z</a></blockquote></div></div>

                        <!-- Gallery 
                        <div class="thumbs_gall_slider_con content_thumbs_gall gall_arrow2 clearfix">
                            <div class="thumbs_gall_slider_larg owl-carousel">
                                <div class="item">
                                    <a href="/images/blog/blog1.jpg">
                                        <img src="/images/blog/blog1.jpg" alt="Image Title">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="/images/blog/blog1.jpg">
                                        <img src="/images/blog/blog1.jpg" alt="Image Title">
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="/images/blog/blog1.jpg">
                                        <img src="/images/blog/blog1.jpg" alt="Image Title">
                                    </a>
                                </div>
                            </div>
                        </div>
                        -->
                    </div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Intro Banner -->
	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>