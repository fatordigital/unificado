<div style="margin-bottom:100px; padding:18px; background:#f7f7f7">
    <h2 class="title1">Envie uma mensagem</h2>
    <p>Nossa equipe entrará em contato contigo</p>
    <br>
    <?php if ($sucesso_contato == 1) { ?><p>Mensagem enviada com sucesso!</p><br /><?php } ?>
    <?php if ($erro_form > 0) { ?><p style="color:red">Preencha os campos obrigatórios!</p><br /><?php } ?>
                
    <form action="<?php echo $action; ?>" method="post" name="contato" id="contato"> 

        <div class="form_row clearfix">
            <input<?php if ($erro_nome == 1) { ?> style="border:1px solid red; border-collapse:collapse"<?php } ?> class="form_fill_fields hm_input_text" type="text" name="nome" id="nome" placeholder="Full Name" value="<?php if ($erro_form == 1) { echo $nome; } ?>">
        </div>
        <div class="form_row clearfix">
            <input<?php if ($erro_email == 1) { ?> style="border:1px solid red; border-collapse:collapse"<?php } ?> class="form_fill_fields hm_input_text" type="text" name="email" id="email" placeholder="mail@nomedosite.com" value="<?php if ($erro_form == 1) { echo $email; } ?>">
        </div>
        <div class="form_row clearfix">
            <input<?php if ($erro_telefone == 1) { ?> style="border:1px solid red; border-collapse:collapse"<?php } ?> type="text" name="telefone" id="telefone" placeholder="Telefone" value="<?php if ($erro_form == 1) { echo $telefone; } ?>">
        </div>
        <div class="form_row clearfix">
            <textarea<?php if ($erro_comentario == 1) { ?> style="border:1px solid red; border-collapse:collapse"<?php } ?> class="form_fill_fields hm_textarea" name="comentario" id="comentario" placeholder="Comentários"><?php if ($erro_form == 1) { echo $comentario; } ?></textarea>
        </div>
        <div class="form_row clearfix">
            <button type="submit" class="send_button full_button" name="contact-us-submit" id="contact-us-submit" value="submit">
                <i class="ico-check3"></i>
                <span>Enviar Mensagem</span>
            </button>
        </div>
    <input type="hidden" name="enviar_contato" value="on">
    </form>
</div>