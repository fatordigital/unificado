<?php
$cto_acao_mail5=mysql_query("select * from informacoes where id='1'");
while($cto_r_mail5=mysql_fetch_array($cto_acao_mail5)) 
	{
	$info_texto_rodape=utf8_encode($cto_r_mail5['texto_rodape']);
	}
?>
		<!-- footer -->
	<footer id="footer">
		<div class="container row_spacer clearfix">
			<div class="rows_container clearfix">
				<div class="col-md-3">
					<div class="footer_row">
						<img alt="Grupo Unificado" src="/images/logo-light.png" style="max-width:70%">
						<span class="footer_desc">
                        <?php echo $info_texto_rodape; ?>
						</span>
						<a href="/grupo-unificado" class="black_button">
							<i class="ico-angle-right"></i><span>Saiba Mais</span>
						</a>
					</div>
				</div><!-- Grid -->
				
				<div class="col-md-3">
					<div class="footer_row">
						<h6 class="footer_title">Últimas Notícias</h6>
						<ul class="recent_posts_list">
							<?php
							$acao=mysql_query("select * from noticias where site1='1' order by data_i desc,id desc limit 2") or die (mysql_error());
							while($r=mysql_fetch_array($acao)) 
								{
								$rec_id=$r['id'];
								$rec_titulo=utf8_encode($r['titulo']);
								$rec_data_d=$r['data_d'];
								$rec_data_m=$r['data_m'];
								$rec_data_a=$r['data_a'];
								$monta_url = MontaURLAmigavel($rec_titulo, 0, 0)
								?>
								<li class="clearfix">
									<span class="recent_post_detail"><?php echo $rec_data_d; ?>/<?php echo $rec_data_m; ?>/<?php echo $rec_data_a; ?></span>
									<a href="noticias/titulo/id">
										<span><?php echo $rec_titulo; ?></span>
									</a>
								</li>
								<?php
								}
							?>
						</ul>
						<a href="/noticias" class="black_button">
							<i class="ico-angle-right"></i><span>Ver todas notícias</span>
						</a>
					</div>
				</div><!-- Grid -->
				
				<div class="col-md-3">
					<div class="footer_row">
						<h6 class="footer_title">Links</h6>
						<div>
							<ul class="list-group-footer">
                            	<li><a href="/"><i class="ico-arrow-forward"></i> Home</a></li>
                            	<li><a href="/grupo-unificado"><i class="ico-arrow-forward"></i> Grupo</a></li>
                            	<li><a href="/empresas"><i class="ico-arrow-forward"></i> Empresas</a></li>
                            	<li><a href="/noticias"><i class="ico-arrow-forward"></i> Notícias</a></li>
                            	<li><a href="/grupo-unificado/sedes"><i class="ico-arrow-forward"></i> Sedes</a></li>
                            	<li><a href="/contato"><i class="ico-arrow-forward"></i> Contato</a></li>
						</div>
					</div>
				</div><!-- Grid -->
				
				<div class="col-md-3">
					<div class="footer_row">
						<h6 class="footer_title">Contatos</h6>
						<span class="footer_desc">
							<?php echo $info_endereco; ?><br>
                            <?php echo $info_cidade; ?>, <?php echo $info_estado; ?><br>
							 (51) 3295-3838<br><br>
							<a href="mailto:<?php echo $info_email; ?>"><?php echo $info_email; ?></a>
    					</span>
						<a href="/grupo-unificado/sedes" class="black_button">
							<i class="ico-angle-right"></i><span>Ver todas sedes</span>
						</a>
                    </div>
				</div><!-- Grid -->
			</div>
		</div>
		<div class="footer_copyright">
			<div class="container clearfix">
				<div class="col-md-6">
					<span class="footer_copy_text">Copyright &copy; 2015 GRUPO UNIFICADO - Todos os direitos reservados</span>
				</div>
				<div class="col-md-6 clearfix text-right">
                	<p>Website produzido por <a href="http://wemake-md.com" target="_blank">WEMAKE Marketing Digital</a></p>
				</div>
			</div>
		</div>
	</footer>
	<!-- End footer -->

<!--<script type="text/javascript">-->
<!--    var _fdq = _fdq || [];-->
<!--    _fdq._setAccount = 'FD_0009';-->
<!---->
<!--    (function() {-->
<!--        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;-->
<!--        ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'www.fatordigital.com.br/metriks.min.js';-->
<!--        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);-->
<!--    })();-->
<!--</script>-->