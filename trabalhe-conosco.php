<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="pt"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	
	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title>Trabalhe Conosco</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta itemprop="name" content="Grupo Unificado">
	<meta name="title" content="Trabalhe Conosco" />
	<meta http-equiv="content-language" content="pt-br" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-store" />
	<meta http-equiv="refresh" content="none" />
	<meta name="reply-to" content="contato@unificado.com.br">
	<meta name="generator" content="Adobe Dreamweaver Macromedia 6.0">
    <meta itemprop="description" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">
    <meta name="keywords" content="grupo unficado, unificado, unificado z, unificado med, curso revisão, curso maio, curso agosto, pré-vestibular, enem, vestibular, cursinho pré-vestibular, colégio unificado, colégio" />
    <meta itemprop="image" content="http://www.unificado.com.br/img/meta-imagem.jpg">
	<meta name="abstract" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">    
	<meta name="author" content="WE MAKE | Marketing Digital" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="general" />
	<meta name="copyright" content="Copyright Grupo Unificado 2016. All Rights Reserved." />    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.unificado.com.br/trabalhe-conosco" />
    <meta property="og:image" content="http://www.unificado.com.br/img/meta-imagem.jpg" />
    <meta property="og:title" content="Trabalhe Conosco"/>
    <meta property="og:description" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">
    <meta property="og:site_name" content="Grupo Unificado" />
    <meta property="og:author" content="WE MAKE Marketing Digital" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    
	<!-- Google Analytics Settings -->
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-72130153-1', 'auto');
      ga('send', 'pageview');
    
    </script>

	<!-- CSS -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/animate.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />


</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class="">Trabalhe Conosco</h1>
			<div class="breadcrumbs">
				<a href="/">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Trabalhe Conosco</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->

	
	<!-- Contact Us -->
	<section class="content_section">
		<div class="content row_spacer no_padding">	
			<div class="rows_container clearfix">				
				<div class="col-md-8">
					<?php if ($sucesso_contato == 1) { ?><h4 style="margin-top:0px; padding-top:0px; margin-bottom:60px;">Currículo enviado com sucesso!</h4><?php } ?>
                    <?php if ($erro_form > 0) { ?><h4 style="margin-top:0px; padding-top:0px; margin-bottom:60px; color:red">Preencha os campos obrigatórios!</h4><?php } ?>
                
					
					<h2 class="title1 upper"><i class="ico-envelope3"></i>Envie seu Currículo</h2><br>
					<form action="http://wemake-md.com/gerenciador-de-leads/insert.php?hash=aWQ9NzgmZW1haWwxPW5pY29sZS5hbmdvbmVzZUB1bmlmaWNhZG8uY29tLmJyJmVtYWlsMj1jb250YXRvQHVuaWZpY2Fkby5jb20uYnImaW5zZXJ0PTE=" method="post" onsubmit="var w=window.open('about:blank','Popup_Window','toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=600,height=300,left = 312,top = 234'); this.target = 'Popup_Window'; inputc()"> 
						<div class="form_row clearfix">
							<label for="contact-us-name">
								<span class="hm_field_name">Nome</span>
								<span class="hm_requires_star">*</span>
							</label>
							<input class="form_fill_fields hm_input_text" type="text" name="wmnome" id="wmnome" placeholder="Nome">
						</div>
						<div class="form_row clearfix">
							<label for="contact-us-mail">
								<span class="hm_field_name">Email</span>
								<span class="hm_requires_star">*</span>
							</label>
							<input class="form_fill_fields hm_input_text" type="email" name="wmemail" id="wmemail" placeholder="mail@nomedosite.com">
						</div>
						<div class="form_row clearfix">
							<label for="contact-us-phone">
								<span class="hm_field_name">Telefone</span>
							</label>
							<input class="form_fill_fields hm_input_text" type="text" name="wmtelefone" id="wmtelefone" placeholder="Telefone">
						</div>
						<div class="form_row clearfix">
							<label for="contact-us-message">
								<span class="hm_field_name">Comentários</span>
								<span class="hm_requires_star">*</span>
							</label>
							<textarea class="form_fill_fields hm_textarea" name="wmcomentario" id="wmcomentario" placeholder="Comentários"></textarea>
						</div>
						<div class="form_row clearfix">
							<button type="submit" class="send_button full_button" name="contact-us-submit" id="contact-us-submit" value="submit">
								<i class="ico-check3"></i>
								<span>Enviar Mensagem</span>
							</button>
						</div>
					</form>
				</div><!-- Grid -->
				<div class="col-md-4">
					<div class="contact_details_row clearfix">
						<span class="icon">
							<i class="ico-location5"></i>
						</span>
						<div class="c_con">
							<span class="c_title">Endereço</span>
							<span class="c_detail">
								<span class="c_name">Matriz</span><br>
								<span class="c_desc"><?php echo $info_endereco; ?><br />
								<span class="c_desc"><?php echo $info_cidade; ?>, <?php echo $info_estado; ?></span>
							</span>
						</div>
					</div>
					
					<div class="contact_details_row clearfix">
						<span class="icon">
							<i class="ico-bubble4"></i>
						</span>
						<div class="c_con">
							<span class="c_title">Telefones</span>
							<span class="c_detail">
								<span class="c_name">Matriz</span><br>
								<span class="c_desc"><?php echo $info_telefone; ?></span>
							</span>
						</div>
					</div>
					
					<div class="contact_details_row clearfix">
						<span class="icon">
							<i class="ico-paperplane"></i>
						</span>
						<div class="c_con">
							<span class="c_title">Email</span>
							<span class="c_detail">
								<span class="c_name">Atendimento</span><br>
								<span class="c_desc"><?php echo $info_email; ?></span>
							</span>
						</div>
					</div>
                    
                    <a href="/grupo-unificado/sedes" class="main_button" style="margin-left:80px">
							<i class="ico-angle-right"></i><span>Ver todas sedes</span>
						</a>
                    
				</div><!-- Grid -->
			</div>
		</div>
	</section>
	<!-- End Contact Us -->
        
	<!-- Google Map -->
	<section class="content_section">
		<div class="title_banner t_b_color1 upper centered">
			<div class="content">
				<h2>Localização</h2>
			</div>
		</div>
		<div class="bordered_content">
			<div class="google_map" data-lat="-30.028145" data-long="-51.221927" data-main="yes" data-text="UNIFICADO">
				<span class="location" data-lat="-30.028145" data-long="-51.221927" data-text="UNIFICADO"></span>
			</div>
		</div>
	</section>
	<!-- End Google Map -->
	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script src="/js/plugins.js"></script>
<script src="/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>
<script src="/js/gmaps.js"></script>
<!-- this is where we put our custom functions -->
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>