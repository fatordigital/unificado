<?php
date_default_timezone_set('America/Sao_Paulo');

include"classes/php.php";

// $var1
// $var2
// $var3

//a variavel atual, vai receber o que estiver na variável pag
//se não tiver nada, ela recebe o valor: principal“”
$atual = (isset($_GET['pag'])) ? $_GET['pag'] : 'principal';
//echo"atual - $atual<BR>";
//aqui setamos um retório onde ficarão as páginas internas do site
 
//vamos testar se a variável pag possui alguma “/”
//ou seja, caso a url seja: /noticia/2
$barra = substr_count($atual, '/'); 

//utilizamos o explode para separar os valores depois de cada “/”
if ($barra > 0)
    {
    $atual = explode('/', $atual);
    $var1 = $atual[0];
    $var2 = $atual[1];
    $var3 = $atual[2];
    $var4 = $atual[3];
    $var5 = $atual[4];
    //echo"entrou barra<BR>";
    }
else
    {
    $var1 = $atual;
    //echo"entrou sem barra<BR>";
    }

//echo"var1 - $var1<BR>";
//echo"var2 - $var2<BR>";
//echo"var3 - $var3<BR>";
//echo"var4 - $var4<BR>";

//echo"var1 - $var1<BR>";
//echo"var2 - $var2<BR>";
//echo"var3 - $var3<BR>";
//echo"var4 - $var4<BR>";

//echo"teste<BR>";

if ($var1 == "grupo-unificado")
	{
	if ($var2 == "sobre-o-grupo")
		{
		include"sobre-o-grupo.php";
		}
	elseif ($var2 == "historia")
		{
		include"historia.php";
		}
	elseif ($var2 == "responsabilidade-social")
		{
		include"responsabilidade-social.php";
		}
	elseif ($var2 == "sedes")
		{
		include"sedes.php";
		}
	else
		{
		include"grupo-unificado.php";
		}
	}
elseif ($var1 == "empresas")
	{
	if ($var2 == "unificado-z")
		{
		include"unificado-z.php";
		}
	elseif ($var2 == "unificado-med")
		{
		include"unificado-med.php";
		}
	elseif ($var2 == "colegio-unificado")
		{
		include"colegio-unificado.php";
		}
	elseif ($var2 == "colegio-unificado-eja")
		{
		include"colegio-unificado-eja.php";
		}
	elseif ($var2 == "gabarito")
		{
		include"gabarito.php";
		}
	else
		{
		include"empresas.php";
		}
	}
elseif ($var1 == "noticias")
	{
	if (($var2 != "pagina") && ($var2 != ""))
		{
		// verifica se tem ponto com $var2
		$acao_ver_ponto=mysql_query("select * from noticias where id='".$var3."'") or die (mysql_error());
		$conta_ver_ponto=mysql_num_rows($acao_ver_ponto);
		if ($conta_ver_ponto > 0)
			{
			include"noticias-ver.php";	
			}
		else
			{
			include"noticias.php";
			}
		}
	else
		{
		include"noticias.php";
		}
	}
elseif ($var1 == "contato")
	{
	if ($var2 == "trabalhe-conosco")
		{
		include"trabalhe-conosco.php";
		}
	else
		{
		include"contato.php";
		}
	}
else
	{
	include"home.php";
	}
?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88256288-2', 'auto');
  ga('send', 'pageview');

</script>
