<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="en"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	
	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title>Notícias</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	
	<!-- Don't forget to set your site up: http://google.com/webmasters -->
	<meta name="google-site-verification" content="" />
	<meta name="Copyright" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Use Iconifyer to generate all the favicons and touch icons you need: http://iconifier.net -->
	<link rel="shortcut icon" href="/images/favicon/favicon.ico" type="image/x-icon" />
	<link rel="apple-touch-icon" href="/images/favicon/apple-touch-icon.png" />
	<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-touch-icon-152x152.png" />
	
	<!-- concatenate and minify for production -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/animate.min.css" type="text/css" media="all" />
	
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />

	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class="">Notícias</h1>
			<div class="breadcrumbs">
				<a href="/">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Notícias</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->

	
	<!-- Our Blog Grids -->
	<section class="content_section">
		<div class="content row_spacer">	
                        
			<!-- All Content -->
			<div class="content_spacer clearfix" style="padding-top:0">
				<div class="content_block col-md-9 f_left">
					<div class="hm_blog_list clearfix">
						
                        <?php
					if (($var3=="") or ($var3 == 0)) $pagina = 1;  
					else $pagina=$var3;
					
					//echo"pagina - $pagina<BR>"; 
					
					$num_por_pagina = 10;
					
					$acao_not_ver="select * from noticias 
					where site1='1' 
					order by data_i desc,id desc";
					//echo"acao_not_ver - $acao_not_ver<BR>";
				
					$consulta = $acao_not_ver;
					$res1 = mysql_query($consulta);
					$total=mysql_num_rows($res1);
					$total = $total/$num_por_pagina;
					
					$prev = $pagina - 1;
					$next = $pagina + 1;	
					if ($pagina > 1) 
						{
						$prev_link = "<li><a href=\"/noticias/pagina/".$prev."\">←</a></li>";
						} 
					else 
						{ 
						$prev_link = "<li><a href=\"javascript:;\">←</a></li>";
						}
					if ($total > $pagina) 
						{
						$next_link = "<li><a href=\"/noticias/pagina/".$next."\">→</a></li>";
						} 
					else 
						{ 
						$next_link = "<li><a href=\"javascript:;\">→</a></li>";
						}
					$total = ceil($total);
					$painel = "";
					for ($x=1; $x<=$total; $x++) 
						{
						if ($x==$pagina) 
							{
							$painel .= "<li><a style=\"background-color: #eee\" href=\"javascript:;\">".$x."</a></li>";
							} 
						else 
							{
							$painel .= "<li><a href=\"/noticias/pagina/".$x."\">".$x."</a></li>";
							}
						}
							
					$conta_acao_not_ver = $total;
					//echo" conta_acao_not_ver - $conta_acao_not_ver<BR>";
					
					$inicio = 1;
					$primeiro_registro = ($pagina*$num_por_pagina) - $num_por_pagina;
							
					$inicio_txt = 0;
					$query_stringNot=mysql_query($acao_not_ver." LIMIT ".$primeiro_registro.", ".$num_por_pagina."");
					$total_stringNot=mysql_num_rows($query_stringNot);
					if ($total_stringNot > 0)
						{
						$inot = 1;
						$i = 1;
						while($array=mysql_fetch_array($query_stringNot)) 
							{
							$ver_id=$array['id'];
							$ver_data_d=$array['data_d']; 
							$ver_data_m=$array['data_m']; 
							$ver_data_a=$array['data_a']; 
							$titulo=utf8_encode($array['titulo']); 
							$imagem=$array['imagem'];
							$link_incorporador=$array['link_incorporador'];
							$texto=utf8_encode(strip_tags($array['texto']));
							$caracteres = strlen($texto);
							if ($caracteres > 100)
								{
								$texto = substr($texto, 0, strrpos(substr($texto, 0, 200), ' ')) . '...';
								}
							$monta_url = MontaURLAmigavel($titulo, 0, 0);
							
							
							?>

                        	<div class="blog_grid_block clearfix">
							<div class="feature_inner">
								<div class="feature_inner_corners">
									<?php
									if ($imagem != "")
										{
										?>
                                        <div class="feature_inner_btns">
                                            <a href="/noticias/<?php echo $monta_url; ?>/<?php echo $ver_id; ?>/<?php echo $pagina; ?>" class="icon_link"><i class="ico-link3"></i></a>
                                        </div>
                                        <a href="/noticias/<?php echo $monta_url; ?>/<?php echo $ver_id; ?>/<?php echo $pagina; ?>" class="feature_inner_ling" data-rel="magnific-popup">
                                            <img src="http://www.unificado.com.br/admin/imagens/<?php echo $imagem; ?>" alt="<?php echo $titulo; ?>">
                                        </a>
                                        <?php
										}
									elseif ($link_incorporador != "")
										{
										?>
                                        <div class="embed">
										<?php echo $link_incorporador; ?>
                                        </div>
                                        <?php
										}
									?>
								</div><!--<div class="feature_inner_corners">-->
							</div><!--<div class="feature_inner">-->
							<div class="blog_grid_con">
								<h6 class="title"><a href="/noticias/<?php echo $monta_url; ?>/<?php echo $ver_id; ?>/<?php echo $pagina; ?>" title="<?php echo $titulo; ?>"><?php echo $titulo; ?></a></h6>
								<span class="meta">
									<span class="meta_part"><i class="ico-clock7"></i><span>Publicado em <?php echo $ver_data_d; ?> de <?php if ($ver_data_m == "01") { echo"janeiro"; } if ($ver_data_m == "02") { echo"fevereiro"; } if ($ver_data_m == "03") { echo"março"; } if ($ver_data_m == "04") { echo"abril"; } if ($ver_data_m == "05") { echo"maio"; } if ($ver_data_m == "06") { echo"junho"; } if ($ver_data_m == "07") { echo"julho"; } if ($ver_data_m == "08") { echo"agosto"; } if ($ver_data_m == "09") { echo"setembro"; } if ($ver_data_m == "10") { echo"outubro"; } if ($ver_data_m == "11") { echo"novembro"; } if ($ver_data_m == "12") { echo"dezembro"; } ?> de <?php echo $ver_data_a; ?></span></span>
									<span class="meta_part"><i class="ico-folder-open-o"></i><span>Unificado Z</a></span></span>
								</span>
								<p class="desc"><?php echo $texto; ?></p>
								<a class="btn_a" href="/noticias/<?php echo $monta_url; ?>/<?php echo $ver_id; ?>/<?php echo $pagina; ?>">
									<span>
										<i class="in_left ico-angle-right"></i>
										<span>Continuar lendo</span>
										<i class="in_right ico-angle-right"></i>
									</span>
								</a>
							</div><!--<div class="blog_grid_con">-->
							</div><!--<div class="blog_grid_block clearfix">-->
                            <?php
							}
						}
					?>


					</div>
					<!-- End blog List -->
                    
                    <ul style="margin-top:50px;" class="pagination clearfix block">
                  <?php
                  echo $prev_link;
                  echo $painel;
                  echo $next_link;
                  ?>
                </ul>
		
				</div>
				<!-- End Content Block -->
				
				<!-- sidebar -->
				<aside id="sidebar" class="col-md-3 right_sidebar">
										
					<?php include_once 'includes/sidebar.php'; ?>
										
				</aside>
				<!-- End sidebar -->
			</div>
			<!-- All Content -->
		</div>
	</section>
	<!-- End Our Blog Grids -->
	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script type="text/javascript" src="/js/flexslider-min.js"></script>
<script src="/js/plugins.js"></script>
<script src="/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>
<script src="/js/gmaps.js"></script>
<script type="text/javascript" src="/js/progressbar.min.js"></script>
<!-- this is where we put our custom functions -->
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>