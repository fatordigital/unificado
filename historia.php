<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="pt"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	
	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title>Grupo Unificado</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta itemprop="name" content="Grupo Unificado">
	<meta name="title" content="Histórico" />
	<meta http-equiv="content-language" content="pt-br" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-store" />
	<meta http-equiv="refresh" content="none" />
	<meta name="reply-to" content="contato@unificado.com.br">
	<meta name="generator" content="Adobe Dreamweaver Macromedia 6.0">
    <meta itemprop="description" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">
    <meta name="keywords" content="grupo unficado, unificado, unificado z, unificado med, curso revisão, curso maio, curso agosto, pré-vestibular, enem, vestibular, cursinho pré-vestibular, colégio unificado, colégio" />
    <meta itemprop="image" content="http://www.unificado.com.br/img/meta-imagem.jpg">
	<meta name="abstract" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">    
	<meta name="author" content="WE MAKE | Marketing Digital" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="general" />
	<meta name="copyright" content="Copyright Grupo Unificado 2016. All Rights Reserved." />    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.unificado.com.br/grupo-unificado/historico" />
    <meta property="og:image" content="http://www.unificado.com.br/img/meta-imagem.jpg" />
    <meta property="og:title" content="Histórico"/>
    <meta property="og:description" content="O Grupo Unificado iniciou com o pré-vestibular, em 1977, por um grupo de professores que acreditaram na ideia de oferecer um ensino de qualidade baseado num método pedagógico inovador.">
    <meta property="og:site_name" content="Grupo Unificado" />
    <meta property="og:author" content="WE MAKE Marketing Digital" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    
	<!-- Google Analytics Settings -->
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-72130153-1', 'auto');
      ga('send', 'pageview');
    
    </script>

	<!-- CSS -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/animate.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />

</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class="">Histórico</h1>
			<div class="breadcrumbs">
				<a href="/">Home</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<a href="/grupo-unificado">Grupo Unificado</a>
				<span class="crumbs-spacer"><i class="ico-angle-right"></i></span>
				<span>Histórico</span>
			</div>
		</div>

	</section>

	<!-- End Page Title -->

	
	<!-- Intro Banner -->
	<section class="content_section">
		<div class="container row_spacer2">
			<div class="container">
				<div class="content clearfix">
                    <div class="col-md-12 text-center">
                    
<p>O Grupo Educacional Unificado, voltado para a educação de jovens e adultos, vem preparando seus alunos para um futuro de sucesso há mais de 34 anos. Além da reconhecida qualidade de suas escolas de Ensino Fundamental e Ensino Médio, do EJA e dos cursos de preparação para concursos, o Unificado se destaca como o melhor curso pré-vestibular do sul do país, alcançando um patamar ímpar no número de aprovados nos vestibulares do Estado.</p>

                    </div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Intro Banner -->

	<!-- Our Blog Grids -->
	<section class="content_section" style="padding:0">
		<div class="content row_spacer no_padding" style="padding:0 0 80px 0">	
			<!-- Filter Content -->
			<div class="hm_filter_wrapper timeline">  
				<ul class="hm_filter_wrapper_con timeline">
					<li class="filter_item_block animated" data-animation-delay="300" data-animation="bounceInUp">
						<div class="timeline_block clearfix" style="text-align:right">
							<h6 class="timeline_title" class="text-right">1977</h6>
							<div class="article">Nasce o Pré-Vestibular Unificado.</div>
							<a href="#" class="timeline_post_format image"><i class="ico-arrow-left3"></i></a>
							<div class="timeline_feature"> 
								<a data-rel="magnific-popup" href="/images/blog/blog5.jpg"> 
									<span class="image-zoom"><i class="ico-plus3"></i></span> 
									<img alt="Responsive Design" title="Responsive Design" src="/images/blog/blog5.jpg"> 
								</a>
							</div>
						</div>
					</li><!-- Item -->
					<li class="filter_item_block animated" data-animation-delay="600" data-animation="bounceInUp">
						<div class="timeline_block clearfix">
							<h6 class="timeline_title">1985</h6>
							<div class="article">Nascimento do supletivo do Unificado, atual EJA (Educação de Jovens e Adultos).</div>
							<a href="#" class="timeline_post_format image"><i class="ico-arrow-right3"></i></a>
							<div class="timeline_feature"> 
								<a data-rel="magnific-popup" href="/images/blog/blog5.jpg"> 
									<span class="image-zoom"><i class="ico-plus3"></i></span> 
									<img alt="Responsive Design" title="Responsive Design" src="/images/blog/blog5.jpg"> 
								</a>
							</div>
						</div>
					</li><!-- Item -->
					<li class="filter_item_block animated" data-animation-delay="300" data-animation="bounceInUp">
						<div class="timeline_block clearfix" style="text-align:right">
							<h6 class="timeline_title" class="text-right">1993</h6>
							<div class="article">Inauguração do Colégio Leonardo da Vinci - sedes Alfa e Beta.</div>
							<a href="#" class="timeline_post_format image"><i class="ico-arrow-left3"></i></a>
							<div class="timeline_feature"> 
								<a data-rel="magnific-popup" href="/images/blog/blog5.jpg"> 
									<span class="image-zoom"><i class="ico-plus3"></i></span> 
									<img alt="Responsive Design" title="Responsive Design" src="/images/blog/blog5.jpg"> 
								</a>
							</div>
						</div>
					</li><!-- Item -->
					<li class="filter_item_block animated" data-animation-delay="600" data-animation="bounceInUp">
						<div class="timeline_block clearfix">
							<h6 class="timeline_title">1995</h6>
							<div class="article">Inauguração do Pré-Vestibular Unificado sede Nilo Peçanha.</div>
							<a href="#" class="timeline_post_format image"><i class="ico-arrow-right3"></i></a>
							<div class="timeline_feature"> 
								<a data-rel="magnific-popup" href="/images/blog/blog5.jpg"> 
									<span class="image-zoom"><i class="ico-plus3"></i></span> 
									<img alt="Responsive Design" title="Responsive Design" src="/images/blog/blog5.jpg"> 
								</a>
							</div>
						</div>
					</li><!-- Item -->
					<li class="filter_item_block animated" data-animation-delay="300" data-animation="bounceInUp">
						<div class="timeline_block clearfix" style="text-align:right">
							<h6 class="timeline_title" class="text-right">1998</h6>
							<div class="article">Nasce o Unificado Concursos.</div>
							<a href="#" class="timeline_post_format image"><i class="ico-arrow-left3"></i></a>
							<div class="timeline_feature"> 
								<a data-rel="magnific-popup" href="/images/blog/blog5.jpg"> 
									<span class="image-zoom"><i class="ico-plus3"></i></span> 
									<img alt="Responsive Design" title="Responsive Design" src="/images/blog/blog5.jpg"> 
								</a>
							</div>
						</div>
					</li><!-- Item -->
					<li class="filter_item_block animated" data-animation-delay="600" data-animation="bounceInUp">
						<div class="timeline_block clearfix">
							<h6 class="timeline_title">1999</h6>
							<div class="article">Surge o conceito de ensino integrado, com 3º ano e pré-vestibular no mesmo turno. Inaugurado o pré-vestibular em Canoas.</div>
							<a href="#" class="timeline_post_format image"><i class="ico-arrow-right3"></i></a>
							<div class="timeline_feature"> 
								<a data-rel="magnific-popup" href="/images/blog/blog5.jpg"> 
									<span class="image-zoom"><i class="ico-plus3"></i></span> 
									<img alt="Responsive Design" title="Responsive Design" src="/images/blog/blog5.jpg"> 
								</a>
							</div>
						</div>
					</li><!-- Item -->
					<li class="filter_item_block animated" data-animation-delay="300" data-animation="bounceInUp">
						<div class="timeline_block clearfix" style="text-align:right">
							<h6 class="timeline_title" class="text-right">2000</h6>
							<div class="article">A sede da Nilo passa a oferecer o conceito de Colégio Unificado.</div>
							<a href="#" class="timeline_post_format image"><i class="ico-arrow-left3"></i></a>
							<div class="timeline_feature"> 
								<a data-rel="magnific-popup" href="/images/blog/blog5.jpg"> 
									<span class="image-zoom"><i class="ico-plus3"></i></span> 
									<img alt="Responsive Design" title="Responsive Design" src="/images/blog/blog5.jpg"> 
								</a>
							</div>
						</div>
					</li><!-- Item -->
					<li class="filter_item_block animated" data-animation-delay="600" data-animation="bounceInUp">
						<div class="timeline_block clearfix">
							<h6 class="timeline_title">2001</h6>
							<div class="article">Inauguração da sede Strip Center Assis Brasil e da primeira franquia do Grupo em Viamão.</div>
							<a href="#" class="timeline_post_format image"><i class="ico-arrow-right3"></i></a>
							<div class="timeline_feature"> 
								<a data-rel="magnific-popup" href="/images/blog/blog5.jpg"> 
									<span class="image-zoom"><i class="ico-plus3"></i></span> 
									<img alt="Responsive Design" title="Responsive Design" src="/images/blog/blog5.jpg"> 
								</a>
							</div>
						</div>
					</li><!-- Item -->
					<li class="filter_item_block animated" data-animation-delay="300" data-animation="bounceInUp">
						<div class="timeline_block clearfix" style="text-align:right">
							<h6 class="timeline_title" class="text-right">2011</h6>
							<div class="article">Por anos consecutivos, o Unificado é vencedor no Top of Mind e é a marca mais lembrada no Marcas de Quem Decide.</div>
							<a href="#" class="timeline_post_format image"><i class="ico-arrow-left3"></i></a>
							<div class="timeline_feature"> 
								<a data-rel="magnific-popup" href="/images/blog/blog5.jpg"> 
									<span class="image-zoom"><i class="ico-plus3"></i></span> 
									<img alt="Responsive Design" title="Responsive Design" src="/images/blog/blog5.jpg"> 
								</a>
							</div>
						</div>
					</li><!-- Item -->
				</ul>
                
                <div style="clear:both; height:20px;"></div>
                
				<div class="centered">
					<a href="../sobre-o-grupo" class="btn_c">
						<span class="btn_c_ic_a"><i class="ico-refresh4"></i></span>
						<span class="btn_c_t">Sobre o Grupo</span>
						<span class="btn_c_ic_b"><i class="ico-refresh4"></i></span>
					</a>
				</div>
			</div>
			<!-- End Filter Content -->
		</div>
	</section>
	<!-- End Our Blog Grids -->
	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script src="/js/plugins.js"></script>
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>