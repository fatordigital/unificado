<?php
$acao=mysql_query("select * from noticias where id='".$var3."'") or die (mysql_error());
$conta=mysql_num_rows($acao);
if ($conta > 0)
	{
	while($r=mysql_fetch_array($acao)) 
		{
		$ver_data_d=$r['data_d']; 
		$ver_data_m=$r['data_m']; 
		$ver_data_a=$r['data_a']; 
		$titulo=utf8_encode($r['titulo']); 
		$imagem=$r['imagem'];
		$link_incorporador=$r['link_incorporador'];
		$texto=utf8_encode($r['texto']);
		$monta_url = MontaURLAmigavel($titulo, 0, 0);
		
		$textotag=strip_tags($texto);
		$caracteres = strlen($textotag);
		if ($caracteres > 100)
			{
			$textotag = substr($textotag, 0, strrpos(substr($textotag, 0, 200), ' ')) . '...';
			}
		else
			{
			$textotag = $texto;
			}
		}
	}
?>
<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--><html class="no-js" lang="pt"><!--<![endif]-->
<!-- the "no-js" class is for Modernizr. --> 
<head>

	
	<!-- Important stuff for SEO, don't neglect. (And don't dupicate values across your site!) -->
	<title><?php echo $titulo; ?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />    
    <meta itemprop="name" content="Unificado">
	<meta name="title" content="<?php echo $titulo; ?>" />
	<meta http-equiv="content-language" content="pt-br" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="pragma" content="no-store" />
	<meta http-equiv="refresh" content="none" />
	<meta name="reply-to" content="contato@unificadoz.com.br">
	<meta name="generator" content="Adobe Dreamweaver Macromedia 6.0">
    <meta itemprop="description" content="<?php echo $textotag; ?>">
    <meta name="keywords" content="grupo unficado, unificado, unificado z, unificado med, curso revisão, curso maio, curso agosto, pré-vestibular, enem, vestibular, cursinho pré-vestibular, colégio unificado, colégio" />
    <meta itemprop="image" content="http://www.unificado.com.br/img/meta-imagem.jpg">
	<meta name="abstract" content="<?php echo $textotag; ?>">    
	<meta name="author" content="WE MAKE | Marketing Digital" />
	<meta name="robots" content="index, follow" />
	<meta name="rating" content="general" />
	<meta name="copyright" content="Copyright Grupo Unificado 2016. All Rights Reserved." />    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="http://www.unificado.com.br/noticias/<?php echo $monta_url; ?>/<?php echo $ver_id; ?>" />
    <meta property="og:image" content="http://www.unificado.com.br/img/meta-imagem.jpg" />
    <meta property="og:title" content="<?php echo $titulo; ?>"/>
    <meta property="og:description" content="<?php echo $textotag; ?>">
    <meta property="og:site_name" content="Unificado Z" />
    <meta property="og:author" content="WE MAKE Marketing Digital" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    
	<!-- Google Analytics Settings -->
	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-72108470-1', 'auto');
      ga('send', 'pageview');
    
    </script>

	<!-- CSS -->
	<link rel="stylesheet" href="/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/flexslider.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/bootstrap.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/animate.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/magnific-popup.css" type="text/css" media="all" />
	<link rel="stylesheet" href="/css/icon-fonts.css" type="text/css" media="all" />


</head>

<!-- Class ( site_boxed - dark - preloader1 - preloader2 - preloader3 - light_header - dark_sup_menu - menu_button_mode - transparent_header - header_on_side ) -->
<body class="preloader3">

<div id="main_wrapper">

	<?php include_once 'includes/header.php'; ?>
		
	<!-- Page Title -->

	<section class="content_section page_title">

		<div class="content clearfix">

			<h1 class=""><?php echo $titulo; ?></h1>
		</div>

	</section>

	<!-- End Page Title -->

	
	<!-- Our Blog Grids -->
	<section class="content_section">
		<div class="content row_spacer" style="padding-top:40px">	
                        
			<!-- All Content -->
			<div class="content_spacer clearfix" style="padding-top:0">
				<div class="content_block col-md-9 f_left">
					<div class="hm_blog_full_list hm_blog_list clearfix">
						
						<!-- Post Container -->
						<div id="post-1" class="post-1 post type-post status-publish format-gallery has-post-thumbnail category-media tag-photos clearfix">
                        
							<div class="post_title_con">
								<span class="meta">
									<span class="meta_part">
										<i class="ico-clock7"></i> <span>Publicado em <?php echo $ver_data_d; ?> de <?php if ($ver_data_m == "01") { echo"janeiro"; } if ($ver_data_m == "02") { echo"fevereiro"; } if ($ver_data_m == "03") { echo"março"; } if ($ver_data_m == "04") { echo"abril"; } if ($ver_data_m == "05") { echo"maio"; } if ($ver_data_m == "06") { echo"junho"; } if ($ver_data_m == "07") { echo"julho"; } if ($ver_data_m == "08") { echo"agosto"; } if ($ver_data_m == "09") { echo"setembro"; } if ($ver_data_m == "10") { echo"outubro"; } if ($ver_data_m == "11") { echo"novembro"; } if ($ver_data_m == "12") { echo"dezembro"; } ?> de <?php echo $ver_data_a; ?></span>
									</span>
									<span class="meta_part">
										<i class="ico-folder-open-o"></i> 
										<span>Unificado Z</span>
									</span>
								</span>
							</div>
							
							<?php
							if ($imagem != "")
								{
								?>
								<div class="feature_inner">
								<div class="feature_inner_corners">
									<a href="http://www.unificado.com.br/admin/imagens/<?php echo $imagem; ?>" class="feature_inner_ling" data-rel="magnific-popup">
										<img src="http://www.unificado.com.br/admin/imagens/<?php echo $imagem; ?>" alt="<?php echo $titulo; ?>">
									</a>
								</div>
								</div>
                                <?php
								}
							?>
                            
							<div class="blog_grid_con">
								
							<?php echo $texto; ?>

							</div>
                            
						</div>
                        
                        <a class="btn_a" href="/noticias/pagina/<?php echo $var4; ?>">
									<span>
										<i class="in_left ico-angle-right"></i>
										<span>Voltar</span>
										<i class="in_right ico-angle-right"></i>
									</span>
								</a>
                        
					</div>
                    
				</div>
				<!-- End Content Block -->
				
				<!-- sidebar -->
				<aside id="sidebar" class="col-md-3 right_sidebar">
										
					<?php include_once 'includes/sidebar.php'; ?>
										
				</aside>
				<!-- End sidebar -->
			</div>
			<!-- All Content -->
		</div>
	</section>
	<!-- End Our Blog Grids -->
	
	<?php include_once 'includes/footer.php'; ?>

	<a href="#0" class="hm_go_top"></a>
</div>
<!-- End wrapper -->

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/js/jquery.js"><\/script>')</script>
<script type="text/javascript" src="/js/flexslider-min.js"></script>
<script src="/js/plugins.js"></script>
<script src="/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>
<script src="/js/gmaps.js"></script>
<script type="text/javascript" src="/js/progressbar.min.js"></script>
<!-- this is where we put our custom functions -->
<script type="text/javascript" src="/js/functions.js"></script>
</body>
</html>