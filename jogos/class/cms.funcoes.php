<?php
    /**
     * funcoes.php
     *
     * Classe de funções
     *  
     */
    class funcoes {

         /* Construtor da classe */
        function __construct(){
            /* Define o tipo de codificação */
            setlocale(LC_ALL, 'ptb', 'pt_BR', 'portuguese-brazil', 'bra', 'brazil', 'pt_BR.utf-8', 'pt_BR.iso-8859-1', 'br');
        }

        /**
         * Função para converter em letras minusculas
         * @param <string> texto a ajustar
         * @return <string> texto ajustado
         */
        public function ajustaTexto($texto) {
            $smallwordsarray = array(
                'de','a','da','das','e','o','os','as','do','dos','em','na','nas','no','nos','p/'
            );
            $text = strtolower(utf8_decode($texto));
            $words = explode(' ', $text);
            foreach ($words as $key => $word)
            {
                if ($key == 0 or !in_array($word, $smallwordsarray))
                    $words[$key] = ucwords(strtolower($word));
            }
            $newtitle = implode(' ', $words);
            return utf8_encode($newtitle);
        }

        /**
         * Função para remover acentuação, retornando em UTF-8
         * @param <string> texto a ajustar
         * @return <string> texto ajustado
         */
        public function removeAcentos($string) {
            $string = utf8_decode($string);
            $string = strtolower($string);
            $string = ereg_replace(utf8_decode("[áàâãäª]"), "a", $string);
            $string = ereg_replace(utf8_decode("[éèêë]"), "e", $string);
            $string = ereg_replace(utf8_decode("[óòôõöº]"), "o", $string);
            $string = ereg_replace(utf8_decode("[íìîï]"), "i", $string);
            $string = ereg_replace(utf8_decode("[úùûü]"), "u", $string);
            $string = ereg_replace(utf8_decode("[ñ]"), "n", $string);
            $string = ereg_replace(utf8_decode("[ç]"), "c", $string);
            $string = ereg_replace(utf8_decode("[ ]"), "_", $string);

            /* Percorre todos os caracteres da string, testando os caracteres que não sao alfanumericos, numericos ou _(substituiu os espaços) */
            for($i = 0; $i < strlen($string); $i++){
                if (!((ord(substr($string,$i,1)) >= 97) && (ord(substr($string,$i,1)) <= 122)) &&  !((ord(substr($string,$i,1)) >= 48) && (ord(substr($string,$i,1)) <= 57)) && (ord(substr($string,$i,1)) <> 97) ){
                    $string = str_replace(substr($string,$i,1), "_" , $string);
                }
            }

            return utf8_encode($string);
        }

         /**
         * Função para evitar SQL Injection
         * @param <string> texto a ajustar
         * @return <string> texto ajustado
         */
        public function antiInjection($campo) {
            /* faz a substituição do conteudo do campo */
            //$campo = preg_replace(sql_regcase("/(from|select|insert|delete|update|where|drop table|show tables|create table|#|\*|--|\\\\)/"), "", $campo);
            /* utf8 decode */
            $campo = utf8_decode($campo);
            /* remove os espaços remanescentes */
            $campo = trim($campo);
            /* utf8 remove as barras de escape */
            $campo = stripslashes($campo);
            /* converte tags html */
            $campo = htmlentities($campo);
            /* converte tags html */
            //$campo = strip_tags($campo);
            /* adiciona barras de escape */
            //$campo = addslashes($campo);
            /* retorna o conteudo */
            return utf8_encode($campo);
        }

        /**
         * Função para exibir o conteúdo do Post
         * @return <string> variáveis e seus respectivos valores
         */
        public function exibePost() {
            echo '<br><b>Conteudo do POST:</b><br><br>';
            /* lista as variaveis e seus respectivos conteudos recebidos pelo post */
            foreach($_POST as $nome_campo => $valor){
               $comando = "\$<b>" . $nome_campo . "</b>=<i>'" . $valor . "'</i>;<br>";
               echo $comando;
            }
        }

        /**
         * Função para retornar a extensão de um determinado arquivo
         * @param <string> arquivo
         * @return <type> extensão
         */
        public function separaExt($arquivo) {
            $arquivo = strtolower($arquivo);
            $exts = split("[/\\.]", $arquivo);
            $n = count($exts) -1;
            $exts = $exts[$n];
            return $exts;
        }

        /**
         * Exibe alerta e redireciona a página
         * @param <string> mensagem de alerta
         * @param <string> destino do redirecionamento. '<' volta à página anterior
         */
        public function alertaRedir($alerta, $redir)
        {
            echo '<script type="text/javascript">';
            /* se ha mensagem de alerta definida */
            if ($alerta != ""){
                echo '  alert("' . $alerta . '");';
            }
            /* se ha link de redirecionamento definido */
            if ($redir != ""){
              /* se o primeiro caractere for um "-" */
              if (substr($redir, 0, 1) == "<"){
                echo '  history.go(-1);';
              } else {
                echo '  document.location="' . $redir . '";';
              }
            }
            echo '</script>';
        }

        /**
         * Exibe alerta e fecha a janela em seguida
         * @param <string> mensagem de alerta
         */
        public function alertaFecha($alerta)
        {
            echo '<script type="text/javascript">';
            /* se ha mensagem de alerta definida */
            if ($alerta != ""){
                echo '  alert("' . $alerta . '");';
            }
            echo '  window.close();';
            echo '</script>';
        }

        /**
         * Função de redirecionamento de página
         * @param <string> destino do redirecionamento. '<' volta à página anterior
         */
        public function redireciona($redir)
        {
          $redir_ = trim($redir);
          echo '<script type="text/javascript">';
          /* se ha link de redirecionamento definido */
          if ($redir != ""){
            /* se o primeiro caractere for um "-" */
            if (substr($redir, 0, 1) == "<"){
              echo '  history.go(-1);';
            } else {
              echo '  document.location="' . $redir . '";';
            }
          }
          echo '</script>';
        }

        /**
         * Função de retornar o mes por extenso
         * @param <string> Mes (Janeiro, Fevereiro...)
         */
        public function getMesExtenso($mes)
        {
            switch($mes) {
                case '1':
                     return 'Janeiro';
                     break;
                case '2':
                     return 'Fevereiro';
                     break;
                case '3':
                     return 'Março';
                     break;
                case '4':
                     return 'Abril';
                     break;
                case '5':
                     return 'Maio';
                     break;
                case '6':
                     return 'Junho';
                     break;
                case '7':
                     return 'Julho';
                     break;
                case '8':
                     return 'Agosto';
                     break;
                case '9':
                     return 'Setembro';
                     break;
                case '10':
                     return 'Outubro';
                     break;
                case '11':
                     return 'Novembro';
                     break;
                case '12':
                     return 'Dezembro';
                     break;
                 default:
                     return '';
            }
        }

        /**
         * Função para retornar o dia da semana por extenso
         * @param <string> Dia da Semana
         */
        public function getSemanaExtenso($semana)
        {
            switch($semana) {
                case '0':
                     return 'Domingo';
                     break;
                case '1':
                     return 'Segunda-feira';
                     break;
                case '2':
                     return 'Terça-feira';
                     break;
                case '3':
                     return 'Quarta-feira';
                     break;
                case '4':
                     return 'Quinta-feira';
                     break;
                case '5':
                     return 'Sexta-feira';
                     break;
                case '6':
                     return 'Sábado';
                     break;
            }
        }


        /**
         * Função para separar o dia da data do mySql
         * @param <string> data (0000-00-00)
         * @return <string> dia (01,02,03...)
         */
        public function getDia($data)
        {
            $dia = substr($data, 8, 2);
            return $dia;
        }


        /**
         * Função para separar o mes da data do mySql
         * @param <string> data (0000-00-00)
         * @return <string> mes (01,02,03...)
         */
        public function getMes($data)
        {
            $mes = substr($data, 5, 2);
            return $mes;
        }


        /**
         * Função para separar o ano da data do mySql
         * @param <string> data (0000-00-00)
         * @return <string> ano (2008,2009,2010...)
         */
        public function getAno($data)
        {
            $ano = substr($data, 0, 4);
            return $ano;
        }


        /**
         * Função para formatar data para mySql
         * @param <string> data recebida (00/00/0000)
         * @return <string> data ajustada (0000-00-00)
         */
        public function dataMysql($data) {
            $dia = substr($data, 0, 2);
            $mes = substr($data, 3, 2);
            $ano = substr($data, 6, 4);
            return $ano . '-' . $mes . '-' . $dia;
        }

        /**
         * Função para formatar data do mySql para exibição
         * @param <string> data recebida (0000-00-00)
         * @return <string> data ajustada (00/00/0000)
         */
        public function mysqlData($data) {
            $dia = substr($data, 8, 2);
            $mes = substr($data, 5, 2);
            $ano = substr($data, 0, 4);
            return $dia . '/' . $mes . '/' . $ano;
        }

        /**
         * Função para converter em maiúscula     *
         * @return STRING
         */
        public function maiusculo($string) {
            $string = utf8_decode($string);
            return utf8_encode(strtoupper($string));
        }

        /**
         * Função para converter em minuscula     *
         * @return string
         */
        public function minusculo($string) {
            $string = utf8_decode($string);
            return utf8_encode(strtolower($string));
        }

    }
?>