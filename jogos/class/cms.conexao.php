<?php
    /* inclui o arquivo com o path */
    include_once "path.php";
    /* inclui o arquivo de conexão do ADOdb */
    include RAIZ_SITE . "/lib/adodb5/adodb.inc.php";

    /**
     * conexao.php
     *
     * Classe de conexão ao banco de dados
     * 
     * @version    0.2
     */
    class conexao {
        /* ID do XML de configurações */
        private $idXML;
        /* ID da conexão com o banco de dados */
        private $idCON;
        /* Dados de acesso ao banco de dados */
        private $sServidor;
        private $sUsuario;
        private $sSenha;
        private $sBanco;

        /*
         * Construtor da classe
         */
        function __construct(){
            /* Faz acesso ao arquivo xml de conexão com o banco de dados */
            $this->idXML = simplexml_load_file(RAIZ_SITE . "/xml/conexao.xml");
            /* Dados de acesso ao banco de dados */
            $this->sServidor = trim($this->idXML->conexao->servidor);
            $this->sUsuario = trim($this->idXML->conexao->usuario);
            $this->sSenha = trim($this->idXML->conexao->senha);
            $this->sBanco = trim($this->idXML->conexao->bancodados);
            $this->sDriver = trim($this->idXML->conexao->driver);
            $this->sDebug = trim($this->idXML->conexao->debug);
        }

        /*
         * Destrutor da classe
         */
        function __destruct(){
            /* Libera os objetos */
            unset($this->idCON);
            unset($this->idXML);
        }

        /**
         * Desliga o controle de debug independente da configuração atual
         */
        public function debugOff() {
            /* Desliga o controle de debug */
            $this->sDebug = '0';
        }

        /**
         * Conecta ao banco de dados
         * @return <integer> ID da conexão
         */
        public function conectar() {
            /* Instancia a classe */
            $this->idCON = ADONewConnection($this->sDriver);
            /* Se deve colocar em modo de debug */
            if ($this->sDebug == '1') {
                $this->idCON->debug = true;
            }
            /* Conecta ao banco de dados */
            $this->idCON->Connect($this->sServidor, $this->sUsuario, $this->sSenha, $this->sBanco);
            /* executa a query, definindo a codificação para UTF-8 */
            $this->idCON->Execute("SET NAMES 'utf8'");
            /* Retorna o id da conexão ao banco de dados */
            return $this->idCON;
        }

        /**
         * Retorna o record set da query executada
         * @param <string>
         * @return <object> Record Set
         */
        public function executar($sql) {
            /* executa a query e retorna a record set */
            return $this->idCON->Execute($sql);
        }

        /**
         * Retorna o último ID inserido no banco
         * @return <integer> ID
         */
        public function ultimoId() {
            /* executa a query e retorna a record set */
            return $this->idCON->insert_ID();
        }

        /**
         * Desconecta da base de dados
         */
        public function desconectar() {
            /* Encerra a conexão com o banco de dados */
            $this->idCON->close();
            $this->idCON = "";
            $this->idXML = "";
        }

        /**
         * Retorna o SQL de inserção do registro
         * @param <string> $rs Record Set
         * @param <array> $registro Registro
         * @return <string> SQL de inserção do registro
         */
        public function sqlInsercao($rs, $registro) {
            /* executa a query e retorna o SQL de inserção do registro */
            return $this->idCON->GetInsertSQL($rs, $registro);
        }

        /**
         * Retorna o SQL de ideração do registro
         * @param <string> $rs Record Set
         * @param <array> $registro Registro
         * @return <string> SQL de ideração do registro
         */
        public function sqlAlteracao($rs, $registro) {
            /* executa a query e retorna o SQL de update do registro */
            return $this->idCON->GetUpdateSQL($rs, $registro);
        }

        /**
         * Retorna o id da conexão do banco de dados
         * @return <integer> ID da conexão
         */
        public function id() {
            /* Retorna o id da conexão ao banco de dados */
            return $this->idCON;
        }

    }
?>