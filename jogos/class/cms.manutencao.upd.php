<?php
    include_once "path.php";
    include_once RAIZ_SITE . "class/cms.conexao.php";

    include_once RAIZ_SITE . "class/cms.funcoes.php";
    $funcoes = new  funcoes();
    
    /* pega o nome da tabela */
    $_nomeTabela = "";
    if (isset($_REQUEST['_nomeTabela'])) {
        $_nomeTabela = $_REQUEST['_nomeTabela'];
    }
    /* pega o nome do campo chave da tabela */
    $_nomeChave = "";
    if (isset($_REQUEST['_nomeChave'])) {
        $_nomeChave = $_REQUEST['_nomeChave'];
    }
    /* pega o valor do campo chave da tabela */
    $_valorChave = "";
    if (isset($_REQUEST['_valorChave'])) {
        $_valorChave = $_REQUEST['_valorChave'];
    }
    /* se não especificou o nome da tabela, ou seu campo chave, ou o valor da chave */
    if (($_nomeTabela == "") || ($_nomeChave == "") || ($_valorChave == "")) {
        exit;
    }
    /* pega os nomes dos campos vindos no post */
    $i = 0;
    foreach($_REQUEST as $campo => $valor){
        /* procura por campo com nome iniciado em "_" */
        $pos = strpos($campo, "_");
        /* se o nome do campo não inicia em "_" */
        if (($pos === false) || ($pos > 0 )) {
            $campos[$i] = $campo;
            $valores[$i] = $valor;
            $i++;
        }
    }
    /* efetua a conexão ao banco de dados */
    $con = new conexao();
    $con->conectar();
    /* obtem da base de dados, o registro a ser editado */
    $sql = "SELECT * FROM " . $_nomeTabela . " WHERE " . $_nomeChave . " = '" . $_valorChave . "'";
    $rs = $con->executar($sql);
    /* inicializa um array para os dados do update */
    $registro = array();
     /* popula o array com os nomes dos campos e seus valores */
    for ($i = 0 ; $i < count($campos) ; $i++) {
        $sqlTipoCampo = "SELECT tipo FROM dicionario WHERE campo = '" . trim($campos[$i]) . "'";
        $rsTipoCampo = $con->executar($sqlTipoCampo);
        $tipoCampo = $rsTipoCampo->fields["tipo"];
        switch ($tipoCampo) {
            case 'D':
                $conteudo = $funcoes->dataMysql($valores[$i]);
                break;
            default:
                 $conteudo = $funcoes->antiInjection($valores[$i]);
        }
        $registro[$campos[$i]] = $conteudo;
    }
    /* passa o record set e e o array do registro, para obter o SQL de ideração */
    $updateSQL = $con->sqlAlteracao($rs, $registro);

    if ($updateSQL == "") {
        /* executa o SQL de ideração */
        $con->executar($updateSQL);
        echo "Registro atualizado com sucesso! *";
    } else {
        /* executa o SQL de ideração */
        $con->executar($updateSQL);
        echo "Registro atualizado com sucesso!";
    }

?>
