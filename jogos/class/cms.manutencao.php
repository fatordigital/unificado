<?php
    /* inclui o arquivo com o path */
    include "path.php";
    /* inclui o arquivo de conexão */
    include_once RAIZ_SITE . "class/cms.conexao.php";
    /* inclui o arquivo de funções */
    include_once RAIZ_SITE . "class/cms.funcoes.php";

    /**
     * manutencao.php
     *
     * Classe de manutenção automatica de tabelas
     *
     * @author     Maicon Gabriel Schmitz <maicongabriel@maqina.com.br>
     * @version    0.1a
     */
    class manutencao {
        /* ID do XML de configurações */
        private $idXML;
        /* declaração do objeto de funções */
        private $fun;
        /* campos do cadastro */
        private $camposCadastro;
        /* validação dos campos do cadastro */
        private $validacaoCadastro;
        /* mascara dos campos do cadastro */
        private $mascarasCadastro;

        /* Construtor da classe */
        function __construct(){
            /* Faz acesso ao arquivo xml de configurações */
            $this->idXML = simplexml_load_file(RAIZ_SITE . "xml/configuracoes.xml");
            /* cria o objeto de funções */
            $this->fun = new funcoes();
            /* inicializa os campos do cadastro */
            $this->camposCadastro = "";
            /* validação dos campos do cadastro */
            $this->validacaoCadastro = "";
        }

        /* Destrutor da classe */
        function __destruct(){
            /* destroi o objeto de funções */
            unset($this->fun);
        }

        /**
         * Função para converter o tipo do campo
         * @param <string> $tipo Tipo do campo
         * @return <string> Tipo do campo ajustado
         */
        function ajustaTipo($tipo) {
            switch ($tipo) {
                case 'I':
                    return 'N';
                    break;
                case 'X':
                    return 'A';
                    break;
                case 'D':
                    return 'D';
                    break;
                case 'N':
                    return 'N';
                    break;
                case 'C':
                    return 'T';
                    break;
                default:
                    return 'T';
                    break;
            }
        }


        function criaCampos($tabela, $chave, $campos, $obrigatorios, $registro) {

            /* cria o array para as informações do campo */
            $infoCampo = array();
            /* cria o array para os tipos dos campos */
            $tipoCampo = array();
            /* pega a lista dos campos obrigatorios */
            $obrCampo = $obrigatorios;
            /* guarda o nome da tabela e da chave principal */
            $this->camposCadastro = $this->camposCadastro . '<input type="hidden" name="_nomeTabela" id="_nomeTabela" value="' . $tabela . '" />';
            $this->camposCadastro = $this->camposCadastro . '<input type="hidden" name="_nomeChave" id="_nomeChave" value="' . $chave . '" />';
            /* efetua a conexão ao banco de dados */
            $con = new conexao();
            $con->conectar();
            /* seleciona os campos da tabela */
            $cam = $con->executar("SELECT " . implode(",", $campos) . " FROM " . $tabela);
            /* seleciona o registro da tabela */
            $reg = $con->executar("SELECT " . implode(",", $campos) . " FROM " . $tabela . " WHERE " . $chave . " = '" . $registro . "'");
            /* percorre os campos selecionados */
            for ($i = 0 ; $i < $cam->FieldCount(); $i++) {
                /* pega informações sobre o campo */
                $infoCampo[$i] = $cam->FetchField($i);
                $tipoCampo[$i] = $cam->MetaType($infoCampo[$i]->type, $infoCampo[$i]->max_length);
                $nome = $infoCampo[$i]->name;
                /* procura o campo no dicionário de dados */
                $dic = $con->executar("SELECT tamanho, mascara, legenda, tipo FROM dicionario WHERE campo = '" . $nome . "'");
                /* se encontrou o campo no dicionário de dados */
                if ($dic->RecordCount() > 0) {
                    /* pega as informações do campo no dicionário de dados */
                    $tamanho = $dic->fields["tamanho"];
                    $mascara = $dic->fields["mascara"];
                    $legenda = $dic->fields["legenda"];
                    $tipo = $dic->fields["tipo"];
                } else {
                    /* se for campo do tipo texto */
                    if ($tipoCampo[$i] == 'C') {
                        /* divide o tamanho por 3, por se tratar de um campo do tipo Unicode (UTF-8) */
                        $tamanho = ($infoCampo[$i]->max_length / 3);
                    } else {
                        $tamanho = $infoCampo[$i]->max_length;
                    }
                    $mascara = '';
                    $legenda = $this->fun->ajustaTexto($infoCampo[$i]->name);
                    $tipo = $this->ajustaTipo($tipoCampo[$i]);
                }
                if ($tamanho == 0 ){
                    $tamanho = "";
                }
                if (in_array($nome, $obrCampo)) {
                    $cmpObr = true;
                    $litCmpObr = " *";
                } else {
                    $cmpObr = false;
                    $litCmpObr = "";
                }

                switch ($tipo) {
                    /* numérico */
                    case 'N':
                         /* cria o campo */
                        $this->camposCadastro = $this->camposCadastro . '<label for="' . $nome . '">' . $legenda . $litCmpObr . '</label>';
                        $this->camposCadastro = $this->camposCadastro . '<p><input type="text" name="' . $nome . '" id="' . $nome . '" value="' . $reg->fields[$nome] . '"  maxlength="' . $tamanho . '"></p>';
                        /* se o campo for do tipo obrigatório */
                        if ($cmpObr) {
                            /* validação dos campos do cadastro */
                            $this->validacaoCadastro = $this->validacaoCadastro . " if (!valInputNum('" . $nome . "', '" . $legenda . "')) { return false; } ";
                        }
                        break;
                    /* area de texto (text area) */
                    case 'A':
                    /* cria o campo */
                        $this->camposCadastro = $this->camposCadastro . '<label for="' . $nome . '">' . $legenda . $litCmpObr . '</label>';
                        $this->camposCadastro = $this->camposCadastro . '<p><textarea name="' . $nome . '" id="' . $nome . '" class="textAreaMedio">' . $reg->fields[$nome] . '</textarea></p>';
                        /* se o campo for do tipo obrigatório */
                        if ($cmpObr) {
                            /* validação dos campos do cadastro */
                            $this->validacaoCadastro = $this->validacaoCadastro . " if (!valInputText('" . $nome . "', '" . $legenda . "')) { return false; } ";
                        }
                        break;
                    /* links, site, etc */
                    case 'L':
                    /* cria o campo */
                        $this->camposCadastro = $this->camposCadastro . '<label for="' . $nome . '">' . $legenda . $litCmpObr . '</label>';
                        $this->camposCadastro = $this->camposCadastro . '<p><input type="text" name="' . $nome . '" id="' . $nome . '" value="' . $reg->fields[$nome] . '" maxlength="' . $tamanho . '" class="inputMedio"></p>';
                        /* se o campo for do tipo obrigatório */
                        if ($cmpObr) {
                            /* validação dos campos do cadastro */
                            $this->validacaoCadastro = $this->validacaoCadastro . " if (!valInputDominio('" . $nome . "', '" . $legenda . "')) { return false; } ";
                        }
                        break;
                    /* data */
                    case 'D':
                        /* cria o campo */
                        $this->camposCadastro = $this->camposCadastro . '<label for="' . $nome . '">' . $legenda . $litCmpObr . '</label>';
                        $this->camposCadastro = $this->camposCadastro . '<p><input type="text" name="' . $nome . '" id="' . $nome . '" value="' . $this->fun->mysqlData($reg->fields[$nome]) . '" maxlength="' . $tamanho . '" class="inpData"></p>';
                        /* se o campo for do tipo obrigatório */
                        if ($cmpObr) {
                            /* validação dos campos do cadastro */
                            $this->validacaoCadastro = $this->validacaoCadastro . " if (!valInputData('" . $nome . "', '" . $legenda . "')) { return false; } ";
                        }
                        break;              
                    /* texto (input text)*/
                    case 'T':
                        /* cria o campo */
                        $this->camposCadastro = $this->camposCadastro . '<label for="' . $nome . '">' . $legenda . $litCmpObr . '</label>';
                        $this->camposCadastro = $this->camposCadastro . '<p><input type="text" name="' . $nome . '" id="' . $nome . '" value="' . $reg->fields[$nome] . '" maxlength="' . $tamanho . '" class="inputMedio"></p>';
                        /* se o campo for do tipo obrigatório */
                        if ($cmpObr) {
                            /* validação dos campos do cadastro */
                            $this->validacaoCadastro = $this->validacaoCadastro . " if (!valInputText('" . $nome . "', '" . $legenda . "')) { return false; } ";
                        }
                        break;
                    /* telefone */
                    case 'F':
                        /* cria o campo */
                        $this->camposCadastro = $this->camposCadastro . '<label for="' . $nome . '">' . $legenda . $litCmpObr . '</label>';
                        $this->camposCadastro = $this->camposCadastro . '<p><input type="text" name="' . $nome . '" id="' . $nome . '" value="' . $reg->fields[$nome] . '" class="inpTelefone"></p>';
                        /* se o campo for do tipo obrigatório */
                        if ($cmpObr) {
                            /* validação dos campos do cadastro */
                            $this->validacaoCadastro = $this->validacaoCadastro . " if (!valInputText('" . $nome . "', '" . $legenda . "')) { return false; } ";
                        }
                        break;
                    /* email */
                    case 'E':
                        /* cria o campo */
                        $this->camposCadastro = $this->camposCadastro . '<label for="' . $nome . '">' . $legenda . $litCmpObr . '</label>';
                        $this->camposCadastro = $this->camposCadastro . '<p><input type="text" name="' . $nome . '" id="' . $nome . '" value="' . $reg->fields[$nome] . '" maxlength="' . $tamanho . '" class="inputMedio"></p>';
                        /* se o campo for do tipo obrigatório */
                        if ($cmpObr) {
                            /* validação dos campos do cadastro */
                            $this->validacaoCadastro = $this->validacaoCadastro . " if (!valInputEmail('" . $nome . "', '" . $legenda . "')) { return false; } ";
                        }
                        break;
                    /* sexo */
                    case 'S':
                        /* cria o campo */
                        if ($reg->fields[$nome] == 1) {
                            $checkedMasculino = 'selected="selected"';
                            $checkedFeminino = '';
                        }
                        if ($reg->fields[$nome] == 2) {
                            $checkedFeminino = 'selected="selected"';
                            $checkedMasculino = '';
                        }
                        $this->camposCadastro = $this->camposCadastro . '<label for="' . $nome . '">' . $legenda . $litCmpObr . '</label>';
                        $this->camposCadastro = $this->camposCadastro . '<p><select name="' . $nome . '" id="' . $nome . '" class="inp200px"><option value="">Selecione</option><option value="1" ' . $checkedMasculino . '>Masculino</option><option value="2" ' . $checkedFeminino . '>Feminino</option></select></p>';
                        /* se o campo for do tipo obrigatório */
                        if ($cmpObr) {
                            /* validação dos campos do cadastro */
                            $this->validacaoCadastro = $this->validacaoCadastro . " if (!valSelect('" . $nome . "', '" . $legenda . "')) { return false; } ";
                        }
                        break;

                    default:

                        break;
                }

                /* insere a mascara do campo */
                if($mascara != ""){
                    $this->mascarasCadastro = $this->mascarasCadastro . '$("#' . $nome . '").mask("' . $mascara . '")';
                }



                //"(99) 9999-9999"
                //"99/99/9999"


                /* se o tipo do campo for numérico */
                //if ($tipo == 'I') {
                //    /* imprime a legenda do campo */
                //    echo $legenda . ' ';
                //    /* cria um imput com o nome do campo e o tamanho maximo do mesmo */
                //    echo '<input type="text" name="' . $nome . '" value="" size="' . $tamanho . '" />';
                //}

                /* se o tipo do campo for de texto */
                //if ($tipoCampo[$i] == 'C') {
                //   /* imprime a legenda do campo */
                //    echo $legenda . ' ';
                //    /* cria um imput com o nome do campo e o tamanho maximo do mesmo */
                //    echo '<input type="text" name="' . $nome . '" value="" size="' . $tamanho . '" />';
                //}

            }

        }

        /**
         * Função para criar manutenção automática de tabelas - Cadastro
         * @param <string> tabela Nome da tabela
         * @param <string> chave Nome do campo chave da tabela
         * @param <array> campos Nome dos campos a manutenir
         * @param <array> obrigatorios Nome dos campos obrigatórios
         * @return <string> "HTML da manutenção"
         */
        public function cadastro($tabela, $chave, $campos, $obrigatorios) {
            $this->criaCampos($tabela, $chave, $campos, $obrigatorios, -1);
        }

        /**
         * Função para criar manutenção automática de tabelas - ideração
         * @param <string> tabela Nome da tabela
         * @param <string> chave Nome do campo chave da tabela
         * @param <array> campos Nome dos campos a manutenir
         * @param <array> obrigatorios Nome dos campos obrigatórios
         * @return <string> "HTML da manutenção"
         */
        public function Alteracao($tabela, $chave, $campos, $obrigatorios, $registro) {
            $this->criaCampos($tabela, $chave, $campos, $obrigatorios, $registro);
        }

        /**
         * Função para criar listagem automática de tabelas
         * @param <string> tabela Nome da tabela
         * @param <string> chave Nome do campo chave da tabela
         * @param <array> campo Nome do campo principal a listar
         * @return <string> "HTML da manutenção"
         */
        public function lista($tabela, $chave, $campo) {
            //...
        }

        /**
         * Retorna os campos do cadastro
         * @return <string> Campos do cadastro
         */
        public function camposCadastro() {
            return $this->camposCadastro;
        }

        /**
         * Retorna o script de validação dos campos
         * @return <string> Campos do cadastro
         */
        public function validacaoCadastro() {
            static $script;
            /* */
            $script = $script . '<script type="text/javascript">';
            $script = $script . 'function validaCadastro() {';
            $script = $script . $this->validacaoCadastro;
            $script = $script . 'return true;';
            $script = $script . '}';
            $script = $script . '</script>';
            /* */
            return $script;
        }
        /**
         * Retorna o script de criação das mascaras
         * @return <string> Mascaras
         */
        public function mascaraCadastro() {
            static $script;
            if(!empty($this->mascarasCadastro)){
                $script = $script . '<script type="text/javascript">';
                $script = $script . 'jQuery(function($){';
                $script = $script . $this->mascarasCadastro;
                $script = $script . '});';
                $script = $script . '</script>';
                /* */
                return $script;
            } else {
                return '';
            }
        }

        /**
         * Função para listar os campos de uma tabela
         * @param <string> tabela Nome da tabela
         * @return <string> "HTML da listagem"
         */
        public function listaCampos($tabela) {
            /* cria o array para as informações do campo */
            $infoCampo = array();
            /* cria o array para os tipos dos campos */
            $tipoCampo = array();
            /* efetua a conexão ao banco de dados */
            $con = new conexao();
            $con->conectar();
            /* seleciona os campos da tabela */
            $cam = $con->executar('SELECT * FROM ' . $tabela);

            /* percorre os campos da tabela */
            for ($i = 0 ; $i < $cam->FieldCount(); $i++) {
                /* pega informações sobre o campo */
                $infoCampo[$i] = $cam->FetchField($i);
                $tipoCampo[$i] = $cam->MetaType($infoCampo[$i]->type, $infoCampo[$i]->max_length);

                /* exibe dados do campo */
                echo 'Campo: ' . $infoCampo[$i]->name . ' - Tipo  ' . $tipoCampo[$i] . '<br>';
            }

        }

    }
?>