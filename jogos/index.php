<?php
    session_start();
    include_once "class/path.php";
    include_once "configuracoes.php";
    /* inclui o arquivo de funções */
    include_once RAIZ_SITE . "class/cms.funcoes.php";
    /* inclui o arquivo de conxao */
    include_once RAIZ_SITE . "class/cms.conexao.php";
    /* conecta ao banco de dados */
    $con = new conexao;
    $con->conectar();
   
    $configuracoes = new configuracoes();    
    if((isset($_GET["secao"])) && ($_GET["secao"] <> "")){
        $secao = $_GET["secao"];
    } else {
        $secao = 'home';
    }  
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Jogos do Unificado 2016</title>
        <meta http-equiv="Content-Type" content="text/html">
        <meta charset="utf-8">
        <meta name="robots" content="index,follow" />
        <meta name="description" content="Jogos do Unificado 2016 - SESC" />
        <meta name="keywords" content="unificado, jogos, primavera, futebol, volei, vôlei, masculino, feminino, sesc, porto alebre" />
        <meta name="revisit-after" content="7 days" />
        <!--[if lt IE 9]>
        <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
        <link href="public/css/screen.css" rel="stylesheet" media="screen" />
        <script type="text/javascript" src="public/js/jquery-1.6.1.min.js"></script>
        <script type="text/javascript" src="public/js/jquery.maskedinput-1.3.min.js"></script>
        <script type="text/javascript" src="public/js/functions.js"></script>
        <script type="text/javascript" src="public/js/swfobject.js"></script>
        <script type="text/javascript" src="public/js/random.js"></script>
    </head>
    <body id="<?=$secao?>">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
        <div id="wheader">
            <div id="header">
                <a href="<?=$configuracoes->urlSite()?>" title="Voltar para Home"><h1>Jogos da Primavera 2015</h1></a>
                <ul>
                    <li id="equipes"><a href="<?=$configuracoes->urlSite()?>equipes" title="Equipes"><div class="border"><div class="align">Equipes</div></div></a></li>
                    <!--
                    <li id="equipes"><a href="<?=$configuracoes->urlSite()?>equipes" title="Equipes"><div class="border"><div class="align">Resultados</div></div></a></li>
                    -->
                    <li id="competicoes"><a href="<?=$configuracoes->urlSite()?>competicoes" title="Competições"><div class="border"><div class="align">Competições</div></div></a></li>
                    <li id="regulamento"><a href="<?=$configuracoes->urlSite()?>termometro" title="Termômetro da Solidariedade"><div class="border"><div class="align">Termômetro da Solidariedade</div></div></a></li>
                    <!--<li id="muro"><a href="<?=$configuracoes->urlSite()?>muro-de-pichacoes" title="Muro de Pichações"><div class="border"><div class="align">Muro de Pichações</div></div></a></li>-->
                    <li id="retrospectiva"><a href="<?=$configuracoes->urlSite()?>retrospectiva" title="Retrospectiva"><div class="border"><div class="align">Retrospectiva</div></div></a></li>
                    <li id="regulamento"><a href="<?=$configuracoes->urlSite()?>regulamento" title="Regulamento"><div class="border"><div class="align">Regulamento e downloads</div></div></a></li>
                    <li id="calendario"><a href="<?=$configuracoes->urlSite()?>calendario" title="Calendário"><div class="border"><div class="align">Calendário</div></div></a></li>
                    <li id="mapa"><a href="<?=$configuracoes->urlSite()?>mapa" title="Mapa"><div class="border"><div class="align">Mapas e transportes</div></div></a></li>
                </ul>
            </div>
        </div>
        <div id="wcontent">
            <?php include 'inc/' . $secao . '.php'; ?>
        </div>
        <div id="wfooter">
            <div id="footer">
            	<p>
                	<img src="public/imgs/banner_patrocinadores.gif" style="width:186px; height:60px; float:left; margin-right:20px; margin-left:60px;">
                    <img src="public/imgs/banner_jogos.jpg" style="width:172px; height:54px; float:left; margin-right:20px; margin-left:60px;">
                    <a href="http://unificado.com.br/" target="_blank"><img src="public/imgs/banner_grupo_unificado.jpg" border="0" style="width:330px; height:54px; float:left; margin-left:64px;"></a>
                </p>
                <br /><br /><br /><br /><br />
                <p>Copyright &reg; Unificado - Todos os direitos reservados.</p>
            </div>
        </div>
        
		<script type="text/javascript">
        
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-1234727-9']);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        
        </script>
    </body>
</html>
