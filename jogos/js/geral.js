/*----------------------------------------------------------------------------
 *                             genéricos
 *-----------------------------------------------------------------------------*/
function overMenu(id) {
    $("#" + id).attr("src", 'img/menu_' + id + "_over.gif");
}

function outMenu(id) {
    $("#" + id).attr("src", 'img/menu_' + id + ".gif");
}

function overSubMenu(id) {
    $("#" + id).attr("src", 'img/submenu_' + id + "_over.gif");
}

function outSubMenu(id) {
    $("#" + id).attr("src", 'img/submenu_' + id + ".gif");
}

function efetuaFiltragem(url){
    if($("#dataFiltragem").val() != "" ){
        document.location = url + '/' + $("#dataFiltragem").val();
    }
}

/*----------------------------------------------------------------------------
 *                             trabalhe conosco
 *-----------------------------------------------------------------------------*/
function waterMarkTrabalheConosco(){
    $("#nome").Watermark("Nome");
    $("#email").Watermark("E-mail");
    $("#telefone").Watermark("Telefone");
    $("#mensagem").Watermark("Mensagem");
    $("#nascimento").Watermark("Data de Nascimento");
}

function mascTrabalheConosco(){
    $("#telefone").mask("(99) 9999-9999");
    $("#nascimento").mask("99/99/9999");
}
function validaTrabalheConosco() {    
    if (!valInputText('nome', 'Nome')) {
        return false;
    }
    if (($("#email").val() == "" || $("#email").val() == "E-mail") && ($("#telefone").val() == "" || $("#telefone").val() == "Telefone")) {
        alert("Informe pelo menos uma forma de contato");
        return false;
    }
    if($("#email").val() != "" && $("#email").val() != "E-mail") {
        if (!valInputEmail('email', 'E-mail')) {
            return false;
        }
    }   
    if($("#nascimento").val() != "" || $("#nascimento").val() != "Data de Nascimento") {
        if(!valInputData("nascimento", "Data de Nascimento")) {
            return false;
        }
    }    
    if(!valRadioCheckBox("sexo", "Sexo")) {
        return false;
    }   
    if (!valInputText('mensagem', 'Mensagem')) {
        return false;
    }
    return true;
}


/*----------------------------------------------------------------------------
 *                             fale conosco
 *-----------------------------------------------------------------------------*/
function waterMarkFaleConosco(){
    $("#nome").Watermark("Nome");
    $("#email").Watermark("E-mail");
    $("#telefone").Watermark("Telefone");
    $("#mensagem").Watermark("Mensagem");
    $("#cidade").Watermark("Cidade");
    $("#assunto").Watermark("Assunto");
}

function mascFaleConosco(){
    $("#telefone").mask("(99) 9999-9999");
}

function validaFaleConosco() {
    if (!valInputText('nome', 'Nome')) {
        return false;
    }
    if (($("#email").val() == "" || $("#email").val() == "E-mail") && ($("#telefone").val() == "" || $("#telefone").val() == "Telefone")) {
        alert("Informe pelo menos uma forma de contato");
        return false;
    }
    if($("#email").val() != "" && $("#email").val() != "E-mail") {
        if (!valInputEmail('email', 'E-mail')) {
            return false;
        }
    }
    if (!valInputText('assunto', 'Assunto')) {
        return false;
    }
    if (!valInputText('mensagem', 'Mensagem')) {
        return false;
    }
    return true;
}

