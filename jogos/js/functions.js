function validaEmail(mail){
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if(er.test(mail)){
        return true;
    } else {
       return false;
    }
}
function valida() {
    if ($("#teamname").val() == ""){
        alert('Informe o nome do time');
        $("#teamname").focus();
        return false;
    } else if ($("#matrícula").val() == ""){
        alert('Informe a matrícula do capitão do time');
        $("#capitainregister").focus();
        return false;    
    } else if ($("#capitainname").val() == ""){
        alert('Informe o nome do capitão');
        $("#capitainname").focus();
        return false;   
    } else if ($("#capitainemail").val() == ""){
        alert('Informe o e-mail do capitão');
        $("#capitainemail").focus();
        return false;   
    } else if (!validaEmail($("#capitainemail").val())){
        alert('E-mail inválido');
        $("#capitainemail").focus();
        return false;
    } else if ($("#capitainfone").val() == ""){
        alert('Informe o telefone do capitão');
        $("#capitainfone").focus();
        return false;
    } else {
        var nome = false;
        $('input[name="matricula[]"]').each(function(){
            if ($(this).val() == $("#capitainregister").val()) {
                nome = true;
            }          
        });
        if (!nome) {
            alert("Cadastre o capitão em uma posição no time");
            return false;
        } else {
            var erro = false;
            $('.teammember').each(function(){
                var matricula = $(this).find('input[name="matricula[]"]').val();
                var nome = $(this).find('input[name="nome[]"]').val();
                var curso = $(this).find('select[name="curso[]"]').val();               
                if (matricula != '' && (nome == '' || curso == '' || curso == 'Selecione')) {
                    erro = true;
                } else if ((curso != '' && curso != 'Selecione') && (matricula == '' || nome == '')){
                    erro = true;
                } else if(nome != '' && (matricula == '' || curso == '' || curso == 'Selecione')) {
                    erro = true;
                }
            });
            
            if (erro) {
                alert("Para cadastrar um jogador você deve informar todos os dados dele");
                return false;
            } else {
                var ig = false;
                var n = 0;
                var cur = new Array();
                $('select[name="curso[]"]').each(function(){
                    var c = $(this).val();
                    if (c != '' && c != 'Selecione') {
                        cur.push(c);
                        n++;
                        if (!ig) {
                            var i = 0;
                            var d = 0;
                            $('select[name="curso[]"]').each(function(){
                                if (c == $(this).val()) {
                                    i++;
                                } 
                            });        
                            if (i > 3) {
                                ig = true;                       
                            }

                        }
                    }
                });
                if (ig) {
                    alert('Não podem haver mais de 3 jogadores no mesmo curso');
                    return false;
                } else{
                    var k;
                    var j;
                    var s = cur.length;
                    var d = false;                  
                    for(k = 0; k < s; k++) {
                        var dif = 0;
                        for(j = 0; j < s; j++) {
                            if (cur[k] != cur[j]){
                                dif++; 
                            }
                        }
                        if (dif >= 5 ) {
                            d = true;                       
                        }
                    }
                     alert(d);
                    return false;                    
                     
                    alert('São necessários jogadores de pelo menos 5 cursos diferentes');
                }
            }
        }
    } 
    
    return true;
    
}

function validaContato(){
     if ($("#nome").val() == ""){
        alert('Informe o nome');
        $("#nome").focus();
        return false;
    } else if ($("#email").val() == ""){
        alert('Informe o email');
        $("#email").focus();
        return false;        
    } else if (!validaEmail($("#email").val())){
        alert('E-mail inválido');
        $("#email").focus();
        return false;
    } else if ($("#mensagem").val() == ""){
        alert('Informe a mensagem');
        $("#mensagem").focus();
        return false;        
    } 
    return true;
}


function validaClassificado(){
     if ($("#matricula").val() == ""){
        alert('Informe a sua matrícula');
        $("#matricula").focus();
        return false;
    } else if ($("#name").val() == ""){
        alert('Informe seu nome');
        $("#name").focus();
        return false;        
    } else if (!validaEmail($("#email").val())){
        alert('E-mail inválido');
        $("#email").focus();
        return false;
    } else if ($("#curso").val() == ""){
        alert('Informe o curso');
        $("#curso").focus();
        return false;        
    } 
    return true;
}

$(document).ready(function(){
    $("#capitainfone").mask('(99) 9999-9999');
    $("#telefone").mask('(99) 9999-9999');
});
