/**
 * função para validar data
 */
function validaData(sData, MenorMaior) {
  /* regular expression para validar data do tipo 'dd/mm/aaaa' */
  var reData = /^((0[1-9]|[12]\d)\/(0[1-9]|1[0-2])|30\/(0[13-9]|1[0-2])|31\/(0[13578]|1[02]))\/\d{4}$/;
  /* avalia a data com a regular expression */
  var reDate;
  reDate = reData;
  var bValida;
  if (reDate.test(sData)) {
    bValida = true;
  } else if (sData != null && sData != "") {
    bValida = false;
  }
  /* se a data é valida, executa o restante das validações */
  if (bValida == true) {
    /* recebe a data no formato DD/MM/AAAA */
    var dia = sData.value.substring(0,2);
    var mes = sData.value.substring(3,2);
    var ano = sData.value.substring(6,4);
    /* cria um objeto de data */
    var dataDigitada = new Date(ano, mes, dia);
    dataDigitada.setHours(0);
    /* pega o horario do objeto criado */
    var mDatDig = dataDigitada.getTime();
    /* cria um objeto de data (com a data atual) */
    var dataAtual = new Date()
    dataAtual.setHours(0);
    var mDatAtu = dataAtual.getTime();
    /* testa se a data digitada é maior que a atual */
    if ( MenorMaior == "<") {
      if (mDatDig > mDatAtu) {
        alert("A data informada é superior à data atual.");
        bValida = false;
      }
    } else {
      if ( MenorMaior == "=") {
        if (mDatDig != mDatAtu) {
          alert("A data informada é diferente da data atual.");
            bValida = false;
        }
      } else {
        if (mDatDig < mDatAtu) {
          alert("A data informada é inferior à data atual.");
          bValida = false;
        }
      }
    }
  }
  return bValida;
}

/**
 * gera chave randômica para utilizar em POST e GET e vitar cache de URL
 * @return string Retorna uma chave randômica
 */
function geraChave() {
    var data = new Date();
    return data.getTime();
}

/**
 * função para confirmação de exclusão de registro
 */
function cnfExcluir() {
    /* exibe a mensagem de confirmação de exclusão */
    var exc = confirm("Você tem certeza de que deseja realmente excluir?\nRegistros excluidos não poderão ser recuperados!");
    /* se confirmou a exclusão */
    if(exc) {
        return true;
    } else {
        return false;
    }
}

/**
 * função para confirmação de cancelamento de edição de registro
 */
function cnfCancelar() {
    /* exibe a mensagem de confirmação de cancelamento de edição de registro */
    var can = confirm("Você tem certeza de que deseja realmente cancelar a edição deste registro?");
    /* se confirmou o cancelamento */
    if(can) {
        return true;
    } else {
        return false;
    }
}

/**
 * função para validar endereço de e-mail
 */
function validaEmail(mail){
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if(er.test(mail)){
        return true;
    } else {
       return false;
    }
}
/**
 * função para validar dominio
 */
function validaDominio(dominio){
    var er = new RegExp(/^[A-Za-z0-9_\:\/\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if(er.test(dominio)){       
        return true;
    } else {        
        return false;
    }
}


/**
 * função para validar campo do tipo input text
 */
function valInputText(campo, legenda) {
    if (($.trim($('#' + campo).val()) == "") ||
        ($.trim($('#' + campo).val()) == legenda)) {
    	alert("Informe um conteúdo válido para o campo " + legenda + "!");
        $('#' + campo).focus();
    	return false;
    } else {
        return true;
    }
}

/**
 * função para validar campo do tipo input text numérico
 */
function valInputNum(campo, legenda) {
    if ($.trim($('#' + campo).val()) == "") {
    	alert("Informe um conteúdo válido para o campo " + legenda + "!");
        $('#' + campo).focus();
    	return false;
    } else {
        if(isNaN($('#' + campo).val())){
            alert("O campo " + legenda + " deve conter apenas números!");
            $('#' + campo).focus();
            return false;
        } else {
            return true;
        }
    }
}

/**
 * funcao para validar, links, dominios e sites
 */
function valInputDominio(campo, legenda) {
    if (!validaDominio($('#' + campo).val())) {
        alert("Informe um conteúdo válido para o campo " + legenda + "!");
        $('#' + campo).focus();
        return false;
    }
    return true;
}


/**
 * função para validar campo do tipo input text
 */
function valInputTextMin(campo, legenda, tamanho) {
    if (valInputText(campo, legenda)) {
        if ( $('#' + campo).val().length < tamanho ) {
            alert("O conteúdo informado para o campo " + legenda + ", deve ser de no mínimo " + tamanho + " caracteres!");
            $('#' + campo).focus();
            return false;
        } else {
            return true;
        }    
    } else {
        return false;
    }
}

/**
 * função para validar campo do tipo input text de valor
 */
function valInputValor(campo, legenda) {
    var er = new RegExp(/^[-+]?\d{1,3}(\.\d{3})*,\d{2}$/);
    /* pega o conteudo do campo */
    var valor = $('#' + campo).val();
    var retorno;
    /* se possui conteudo no campo */
    if (valor != "") {
        /* valida o conteudo do campo */
        if(er.test(valor)){
            retorno = true;
        } else {
            alert("O campo " + legenda + " não contem um valor válido!");
            $('#' + campo).focus();
            retorno = false;
        }
    } else {
        retorno = true;
    }
    return retorno;
}

/**
 * função para validar campo do tipo input text de valor obrigatório
 */
function valInputValorObr(campo, legenda) {
    if ($('#' + campo).val() == "") {
    	alert("Informe um conteúdo válido para o campo " + legenda + "!");
        $('#' + campo).focus();
    	return false;
    } else {
        return valInputValor(campo, legenda);
    }
}

/**
 * função para validar campo tipo input text (para senha)
 */
function valInputTextSenha(senha, legSenha, contraSenha, legContraSenha) {

    if ($('#' + senha).val() == "") {
    	alert("Informe um conteúdo válido para o campo " + legSenha + "!");
        $('#' + senha).focus();
    	return false;
    }

    if ($('#' + contraSenha).val() == "") {
    	alert("Informe um conteúdo válido para o campo " + legContraSenha + "!");
        $('#' + contraSenha).focus();
    	return false;
    }

    if (($('#' + senha).val()) != ($('#' + contraSenha).val())) {
    	alert("O conteúdo informado não campos " + legSenha + " e " + legContraSenha + " não são iguais!");
        $('#' + senha).focus();
    	return false;
    }

    return true;
}

/**
 * função para validar campo tipo input text (para senha)
 */
function valInputTextSenhaMin(senha, legSenha, contraSenha, legContraSenha, tamanho) {
    if (valInputTextSenha(senha, legSenha, contraSenha, legContraSenha)) {
        if ( $('#' + senha).val().length < tamanho ) {
            alert("O conteúdo informado para o campo " + legSenha + ", deve ser de no mínimo " + tamanho + " caracteres!");
            $('#' + senha).focus();
            return false;
        } else {
            return true;
        }
    } else {
        return false;
    }
}

/**
 * função para valida campo de e-mail
 */
function valInputEmail(campo, legenda) {
    var mail = $('#' + campo).val();
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if(er.test(mail)){
        return true;
    } else {
       alert("Informe um endereço de e-mail válido para o campo " + legenda + "!");
       $('#' + campo).focus();
       return false;
    }    
}

/**
 * função para validar campo do tipo select
 */
function valSelect(campo, legenda) {
    if ($('#' + campo).val() == "") {
    	alert("Selecione uma opção para o campo " + legenda + "!");
        $('#' + campo).focus();
    	return false;
    } else {
        return true;
    }
}

/**
 * função para validar campo do tipo radio
 */
function valRadioCheckBox(campo, legenda) {
    if ($("input[name='" + campo + "']:checked").val() == undefined) {
    	alert("Selecione uma opção para o campo " + legenda + "!");      
    	return false;
    } else {
        return true;
    }    
}


/**
 * função para validar campos de data
 */

/* */
function valInputData(campo, legenda) {
    if(!verificaData($("#" + campo).val())){
        alert("Informe uma data válida para o campo " + legenda);
        $("#" + campo).focus();
        return false;
    }
    return true;
}

function verificaData(data){
    if (data != "") {
        /* separa dia, mes e ano*/
        var dia = data.substr(0, 2);
        var mes = data.substr(3, 2);
        var ano = data.substr(6, 4);

        /* verifica os dias*/
        if (dia > 31) {
            return false;
        }
        if ((dia == 31) && ((mes == '02') || (mes == '04') || (mes == '06') || (mes == '09') || (mes == '11'))) {
            return false;
        }
        if (dia < 01) {
            return false;
        }
        /* verifica os meses*/
        if (mes > 12) {
            return false;
        }
        if (mes < 01) {
            return false;
        }
        /* verifica os anos*/
        if (ano < 1900) {
            return false;
        }
        if ((dia > 29) && (mes == 02)) {
            return false;
        }
        /* verifica se o ano é bissesxto */
        if ((dia == '29') && (mes == '02')){
            var resto = ano % 4;
            if (resto != 0){
                 return false;
            } else {
                if (ano.substr(2,2) == '00'){
                   resto = ano % 400;
                   if (resto != 0){
                        return false;
                   }
                }
            }
        }
        return true;
    } else {
         return false;
    }
}
/**
 * função para validar CNPJ
 */



function valCnpj(campo, legenda) {
    cnpj = $("#" + campo).val();
    function msg(){
        alert("Informe um CNPJ válido para o campo " + legenda);
        $("#" + campo).focus();
        return false;       
    }
    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais, cnpj = cnpj.replace(/\D+/g, '');
    digitos_iguais = 1;
    if (cnpj.length != 14){
         msg();
    } else {
        for (i = 0; i < cnpj.length - 1; i++){
            if (cnpj.charAt(i) != cnpj.charAt(i + 1)) {
                digitos_iguais = 0;
                break;
            }
        }
        if (!digitos_iguais){
            tamanho = cnpj.length - 2
            numeros = cnpj.substring(0,tamanho);
            digitos = cnpj.substring(tamanho);
            soma = 0;
            pos = tamanho - 7;
            for (i = tamanho; i >= 1; i--){
                soma += numeros.charAt(tamanho - i) * pos--;
                if (pos < 2) {
                    pos = 9;
                }
            }
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0)){
                msg();
            } else {
                tamanho = tamanho + 1;
                numeros = cnpj.substring(0,tamanho);
                soma = 0;
                pos = tamanho - 7;
                for (i = tamanho; i >= 1; i--){
                    soma += numeros.charAt(tamanho - i) * pos--;
                    if (pos < 2) {
                        pos = 9;
                    }
                }
                resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != digitos.charAt(1)){
                    msg();
                } else {
                    return true;
                }
            }
        } else{
            msg();
        }
    }
}


/**
 * função para validar CPF
 */
function valCpf(campo, legenda){
    function msg() {       
            alert("Informe um CPF válido para o campo " + legenda + "!");
            $('#' + campo).focus();        
    }
    var cpf = $('#' + campo).val();
    var filtro = /^\d{3}.\d{3}.\d{3}-\d{2}$/i;
    if(!filtro.test(cpf)){
        msg();
        return false;
    }

    cpf = cpf.replace('.', '');
    cpf = cpf.replace('.', '');
    cpf = cpf.replace('-', '');
    if((cpf.length != 11) || (cpf == "00000000000") || (cpf == "11111111111") ||
       (cpf == "55555555555") || (cpf == "66666666666") || (cpf == "77777777777") ||
       (cpf == "22222222222") || (cpf == "33333333333") || (cpf == "44444444444") ||
       (cpf == "88888888888") || (cpf == "99999999999")){
       msg();
       return false;
    }

    var soma = 0;
    for(i = 0; i < 9; i++){
        soma += parseInt(cpf.charAt(i)) * (10 - i);
    }
    var resto = 11 - (soma % 11);
    if(resto == 10 || resto == 11){
        resto = 0;
    }
    if(resto != parseInt(cpf.charAt(9))){
        msg();
        return false;
    }
    soma = 0;
    for(i = 0; i < 10; i ++) {
        soma += parseInt(cpf.charAt(i)) * (11 - i);
    }
    resto = 11 - (soma % 11);

    if(resto == 10 || resto == 11)
        resto = 0;
    if(resto != parseInt(cpf.charAt(10))){
        msg();
        return false;
    }
    return true;
}

/**
 * função para setar foco no primeiro campo (input) existente
 */
function setFocPri() {
    $("input:text:first").focus();
    return;
}

/**
 * função para ir para uma URL específica
 */
function gotoURL(URL) {
    window.location.href = URL; 
}

/**
 * função para exibir popup
 */
function popup(URL, janela, largura, altura) {
    var destino = URL;
    var nomejanela = janela;
    /* abre o popup */
    window.open(destino, nomejanela);
}
/**
 * função para remover acentos
 */
function removeAcentos(texto) {
    var string = texto.toLowerCase();
    string = string.replace(/[áàâãäª]/, "a");
    string = string.replace(/[éèêë]/, "e");
    string = string.replace(/[óòôõöº]/, "o");
    string = string.replace(/[íìîï]/, "i");
    string = string.replace(/[úùûü]/, "u");
    string = string.replace(/[ñ]/, "n");
    string = string.replace(/[ç]/, "c");
    string = string.replace(" ", "");
    return string.toUpperCase();
}
/**
 * função para retornar o mes por extenso
 */

function getMesExtenso(mes) {  
    switch(mes) {
        case '01':
             return 'Janeiro';
             break;
        case '02':
             return 'Fevereiro';
             break;
        case '03':
             return 'Março';
             break;
        case '04':
             return 'Abril';
             break;
        case '05':
             return 'Maio';
             break;
        case '06':
             return 'Junho';
             break;
        case '07':
             return 'Julho';
             break;
        case '08':
             return 'Agosto';
             break;
        case '09':
             return 'Setembro';
             break;
        case '10':
             return 'Outubro';
             break;
        case '11':
             return 'Novembro';
             break;
        case '12':
             return 'Dezembro';
             break;
         default:
             return '';
    }
}

/**
 * Função para montar data de exibição à partir de um array de datas
 * @param [arrayDatas] Array de datas em formato 00/00/0000
 * @return retorna as datas por extenso Ex: 01, 02 e 03 de Janeiro de 2008
 */

function dataExibicao(arrayDatas){
    if((typeof(arrayDatas.length) != undefined) && (arrayDatas != "") && (arrayDatas != 0) ) {
        var datasN = new Array();
        var ano = new Array();
        var mes = new Array();
        var dia = new Array();
        var posicaoAno = new Array();
        var string = '';
        var adicionaE = '';
        var juntaAnos = '';
        var i = 0;
        var cont = 0;
        var j = 0;
        var k = 0;
        var qtdArrData = arrayDatas.length;
        var validas = 0;
        for(i = 0; i < qtdArrData; i++){
            if ($.trim(arrayDatas[i]) != ""){
                /* verifica se a data foi totalmente preenchida */
                if (arrayDatas[i].replace('_', '').length == 10 ){
                    var data = arrayDatas[i].split('/');
                    datasN.push(data[2] + '/' + data[1] + '/' + data[0]);
                    validas++;
                }
            }
        }
        /* se não ouverem datas válidas ( se só houverem datas com __/__/____)*/
        if (validas > 0) {
            delete(data);
            var datas = datasN.sort().unique();
            var qtdDatas = datas.length;
            for(i = 0; i < qtdDatas; i++){
                if (($.trim(datas[i]) != "") && ($.trim(datas[i]) != undefined)){
                    data = datas[i].split('/');
                    ano[i] = data[0];
                    mes[i] = data[1];
                    dia[i] = data[2];
                }
            }
            var anoUni = ano.sort().unique();
            var qtdDias = dia.length;
            var qtdAnos = anoUni.length;
            /* percorre o array de anos únicos */
            for(i = 0; i < anoUni.length; i++){
                var addVirgula = '';
                cont = 0;
                /* pega o indice das datas relativas ao ano 'pesquisado'(ano único: anoUni) */
                for(j = 0; j < ano.length; j++){
                    if(anoUni[i] == ano[j]){
                       posicaoAno[cont] = j;
                       cont++;
                    }
                }
                /* percorre o array de posicoes de um ano */
                for (k = 0; k < posicaoAno.length; k++ ) {
                    /* verifica se o próximo mes é igual ao atual( se é do mesmo mes ) */
                    if(mes[posicaoAno[k]] == mes[posicaoAno[k + 1]]){
                        /* verifica se o próximo dia é o ultimo do mes */
                        if ((mes[posicaoAno[k]] != mes[posicaoAno[k + 2]])) {
                            addVirgula = ' e ';
                        } else {
                            addVirgula = ', ';
                        }
                        /* escreve os dias de um determinado mes */
                        string += dia[posicaoAno[k]] + addVirgula;
                    } else {
                        /* se o próximo mes existe e se é diferente do atual então adiciona o 'e' */
                        if((mes[posicaoAno[k + 1]] != undefined) && (mes[posicaoAno[k + 1]] != mes[posicaoAno[k]])){
                            adicionaE = ' e ';
                        } else {
                            adicionaE = '';
                        }
                        /* escreve o dia do mes acompanhado do mes excrito por extenso */
                        string += dia[posicaoAno[k]] + ' de ' + getMesExtenso(mes[posicaoAno[k]]) + adicionaE;
                    }
                }
                /* reinicializa o array das posições do ano para usa-lo novamente para o ano seguinte */
                delete(posicaoAno);
                posicaoAno = new Array();
                /* contatena o 'e' entre os anos */
                if((i + 1) < qtdAnos){
                    juntaAnos = ' e ';
                } else {
                    juntaAnos = '';
                }
                string +=  ' de ' + anoUni[i] + juntaAnos;
            }
            /* verifica a quantidade de dias para escrever no plural ou no singular */
            if(qtdDias > 1){
                string = 'Dias ' + string;
            } else {
                string = 'Dia ' + string;
            }
        } else {
            string = 'Nenhuma data cadastrada';
        }
    }
    return string;
}
Array.prototype.unique = function () {
    var r = new Array();
    o:for(var i = 0, n = this.length; i < n; i++)
    {
        for(var x = 0, y = r.length; x < y; x++)
        {
            if(r[x]==this[i])
            {
                    continue o;
            }
        }
        r[r.length] = this[i];
    }
    return r;
}