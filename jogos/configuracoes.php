<?php
    include "class/path.php";

    class configuracoes{

        /* objeto de conexão ao banco de dados */
        private $xmlconfig;
        private $dirBase;
        private $urlSite;
        private $emailPadrao;
        private $emailDirecao;

        /* construtor da classe */
        function __construct(){
            $this->xmlconfig = simplexml_load_file(RAIZ_SITE . "xml/configuracoes.xml");
            /* diretório base do site  */
            $this->dirBase = trim($this->xmlconfig->configuracoes->caminho->base);
            /* URL do site */
            $this->urlSite = trim($this->xmlconfig->configuracoes->url->site);
            /* email padrao do  site */
            $this->emailPadrao = trim($this->xmlconfig->configuracoes->email->padrao);
            /* email direção do  site */
            $this->emailDirecao = trim($this->xmlconfig->configuracoes->email->direcao);
        }

        /* destrutor da classe */
            function __destruct(){
        }

        /* geters e seters */
        public function getDirBase(){
            return $this->dirBase;
        }
        public function getUrlSite(){
            return $this->urlSite;
        }
        public function getEmailPadrao(){
            return $this->emailPadrao;
        }
        public function getEmailDirecao(){
            return $this->emailDirecao;
        }

        /* geters e seters */
        public function dirBase(){
            return $this->getDirBase();
        }
        public function urlSite(){
            return $this->getUrlSite();
        }
        public function emailPadrao(){
            return $this->getEmailPadrao();
        }
        public function emailDirecao(){
            return $this->getEmailDirecao();
        }
    }
?>