function closeLayer(){
document.getElementById('flash').style.display='none';
document.getElementById('layer').style.display='none';
}

function DoPrinting() {
    if (!window.print) {
        alert("Netscape, Internet Explorer 4.0 ou superior!")
        return
    }
    window.print();
}

function validaEmail(mail){
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if(er.test(mail)){
        return true;
    } else {
       return false;
    }
}
function valida() {
    if ($("#matricula").val() == ""){
        alert('Informe seu número de matrícula.');
        $("#matricula").focus();
        return false;
    } else if ($("#equipe").val() == "0"){
        alert('Seleciona a que equipe você faz parte.');
        $("#equipe").focus();
        return false;    
    } else if ($("#modalidade").val() == "0"){
        alert('Selecione a modalidade que voê vai participar.');
        $("#modalidade").focus();
        return false;
    } else if ($("#nome").val() == ""){
        alert('Informe seu nome');
        $("#nome").focus();
        return false;   
    } else if (!validaEmail($("#email").val())){
        alert('E-mail inválido');
        $("#email").focus();
        return false;
    } 
    
    return true;
    
}

function validacoordenador() {
    if ($("#matricula").val() == ""){
        alert('Informe seu número de matrícula.');
        $("#matricula").focus();
        return false;
    } else if ($("#equipe").val() == "0"){
        alert('Seleciona a que equipe você faz parte.');
        $("#equipe").focus();
        return false;    
    } else if ($("#nome").val() == ""){
        alert('Informe seu nome');
        $("#nome").focus();
        return false;   
    } else if (!validaEmail($("#email").val())){
        alert('E-mail inválido');
        $("#email").focus();
        return false;
    } 
    
    return true;
    
}

function validacapitao() {
    if ($("#nome").val() == ""){
        alert('Informe seu nome.');
        $("#nome").focus();
        return false;
    } else if ($("#matricula").val() == ""){
        alert('Informe seu número de matrícula');
        $("#matricula").focus();
        return false;  
    } else if ($("#modalidade").val() == "0"){
        alert('Seleciona o time que você é capitão.');
        $("#modalidade").focus();
        return false;    
    } else if ($("#equipe").val() == "0"){
        alert('Seleciona a que equipe você faz parte.');
        $("#equipe").focus();
        return false;  
    } 
    
    return true;
    
}

function validatime() {
    if ($("#matricula").val() == ""){
        alert('Informe seu número de matrícula.');
        $("#matricula").focus();
        return false;
    } else if ($("#senha").val() == ""){
        alert('Informe sua senha');
        $("#senha").focus();
        return false; 
    } else if ($("#equipe").val() == "0"){
        alert('Seleciona a que equipe você faz parte.');
        $("#equipe").focus();
        return false;    
    } else if ($("#fantasia").val() == ""){
        alert('Informe o novo nome do seu time');
        $("#fantasia").focus();
        return false;   
    } 
    
    return true;
    
}

function validaMuro() {
    if ($("#matricula").val() == ""){
        alert('Informe seu número de matrícula.');
        $("#matricula").focus();
        return false;
    } else if ($("#mensagem").val() == ""){
        alert('Informe seu nome');
        $("#mensagem").focus();
        return false;   
    } else if ($("#nome").val() == ""){
        alert('Informe seu nome');
        $("#nome").focus();
        return false;   
    } 
    
    return true;
    
}


function validaContato(){
     if ($("#nome").val() == ""){
        alert('Informe o nome');
        $("#nome").focus();
        return false;
    } else if ($("#email").val() == ""){
        alert('Informe o email');
        $("#email").focus();
        return false;        
    } else if (!validaEmail($("#email").val())){
        alert('E-mail inválido');
        $("#email").focus();
        return false;
    } else if ($("#mensagem").val() == ""){
        alert('Informe a mensagem');
        $("#mensagem").focus();
        return false;        
    } 
    return true;
}

function validaClassificado(){
     if ($("#matricula").val() == ""){
        alert('Informe a sua matrícula');
        $("#matricula").focus();
        return false;
    } else if ($("#name").val() == ""){
        alert('Informe seu nome');
        $("#name").focus();
        return false;        
    } else if (!validaEmail($("#email").val())){
        alert('E-mail inválido');
        $("#email").focus();
        return false;
    } else if ($("#curso").val() == ""){
        alert('Informe o curso');
        $("#curso").focus();
        return false;        
    } 
    return true;
}

$(document).ready(function(){
    $("#capitainfone").mask('(99) 9999-9999');
    $("#telefone").mask('(99) 9999-9999');
});
