<div id="content">
    <h2>Calendário</h2>
    <div class="colleft">
			<img src="public/imgs/banner_home.jpg" border="0">
    </div>    
    
    <div class="colright">
        <ul>
            <li>Maio
                <ul>
                	<li><strong>21</strong></li>
                	<li>14h30: 1º Congresso Técnico: Sede Alberto Bins</li>
                	<li><strong>31</strong></li>
                	<li>Prazo limite para solicitação das camisetas.</li>
                </ul>
            </li>
            <li>Junho
                <ul>
                	<li><strong>23</strong></li>
                	<li>Limite de inscrição das equipes pelo site dos Jogos.</li>
                	<li><strong>24</strong></li>
                	<li>Limite para a entrega de autorizações</li>
                	<li><strong>25</strong></li>
                	<li>14h30: 2º Congresso Técnico: Sede Alberto Bins – Sorteio dos confrontos.</li>
                </ul>
            </li>
            <li>Julho
                <ul>
                	<li><strong>02</strong></li>
                	<li>7h30: Concentração / 8h: Cerimônia de Abertura.</li>
                </ul>
            </li>
        </ul>
    </div>
</div>