<div id="content">
    <h2>Arquivos para Download</h2>
    <br><br>
    
    <p>
	<!--
    <a href="public/pdf/regulamento-cheerleaders.pdf" target="_blank" style="color:#333">Regulamento - Modalidade Cheerleaders</a>
	<br>
    -->
    <a href="public/pdf/ResultadosJogos.xlsx" target="_blank" style="color:#333">Resultados Jogos do Unificado</a>
	<br>
	<a href="public/pdf/CONFRONTOS-2016.xlsx" target="_blank" style="color:#333">Confrontos 2016 Jogos do Unificado</a>
	<br>
    <a href="public/pdf/AUTORIZACAO_MAIOR_DE_IDADE_2016.pdf" target="_blank" style="color:#333">Autorização para Jogos - Maior de idade</a>
    <br>
    <a href="public/pdf/AUTORIZACAO_MENOR_DE_IDADE_2016.pdf" target="_blank" style="color:#333">Autorização para Jogos - Menor de idade</a>
    <br>
    <!--
    <a href="public/pdf/oficio_doacoes_jogos.jpg" target="_blank" style="color:#333">Ofício para Doação</a>
    <br>
    <a href="public/pdf/CARTA_COORDENADORES2015.pdf" target="_blank" style="color:#333">Carta aos Coordenadores</a>
    <br>
    <!--
    <a href="public/pdf/Criterios_de_Repescagem2014.doc" target="_blank" style="color:#333">Critérios de Repescagem</a>
    <a href="public/pdf/encomendas_das_camisetas_2013.xls" target="_blank" style="color:#333">Encomendas das camisetas 2013</a>
    <br>
    <a href="public/pdf/Criterios_de_Repescagem2013.doc" target="_blank" style="color:#333">Critérios para Repescagem</a>
    -->
    <br>
    </p>

    <br><br>
	<!--
	<span style="color:#5D2F78">
		<strong>Atenção, alunos:</strong><br />
		Toda a equipe que estiver acompanhada de 1(um) professor coordenador na Cerimônia de Abertura, no momento da contagem, receberá <br />pontuação equivalente a 10 alunos (20 pontos).<br />
		Obs: será contabilizado somente 1(um) professor.
	</span>
	-->
	<br><br><br><br><br>
	
    <h2>Regulamento</h2>
        <br>
  <p>
  		<br>
        <!--
        Aviso: O regulamento das cheerleaders e dos games serão explicados no 1º Congresso Técnico.
        -->
        <br><br><br>
        <strong>REGULAMENTO JOGOS DO UNIFICADO 2016</strong>
        <br><br><br><br>
        <strong>DISPOSIÇÕES GERAIS</strong>
        <br><br>
		Os jogos serão disputados nas seguintes modalidades, com as seguintes pontuações:
        <br />
        <strong>a)</strong>
        <table width="900" border="0" cellspacing="4">
          <tr>
            <td bgcolor="#CCCCCC">DESPORTOS COLETIVOS</td>
            <td bgcolor="#CCCCCC">1º</td>
            <td bgcolor="#CCCCCC">2º</td>
            <td bgcolor="#CCCCCC">3º</td>
            <td bgcolor="#CCCCCC">4</td>
          </tr>
          <tr>
            <td>Futebol 7 Masculino</td>
            <td>600</td>
            <td>400</td>
            <td>300</td>
            <td>200</td>
          </tr>
          <tr>
            <td>Futebol 7 Feminino</td>
            <td>600</td>
            <td>400</td>
            <td>300</td>
            <td>200</td>
          </tr>
          <tr>
            <td>Futebol de Campo Masculino</td>
            <td>600</td>
            <td>400</td>
            <td>300</td>
            <td>200</td>
          </tr>
          <tr>
            <td>Handebol Feminino</td>
            <td>600</td>
            <td>400</td>
            <td>300</td>
            <td>200</td>
          </tr>
          <tr>
            <td>Vôlei Misto</td>
            <td>600</td>
            <td>400</td>
            <td>300</td>
            <td>200</td>
          </tr>
        </table>
        <br>
        <p><strong>b)</strong> </p>
        <table width="900" border="0" cellspacing="4">
          <tr>
            <td bgcolor="#CCCCCC">ESPORTE INDIVIDUAL</td>
            <td bgcolor="#CCCCCC">1º</td>
            <td bgcolor="#CCCCCC">2º</td>
            <td bgcolor="#CCCCCC">3º</td>
            <td bgcolor="#CCCCCC">4º</td>
          </tr>
          <tr>
            <td>Game 1</td>
            <td>200</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Game 2</td>
            <td>200</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Game 3</td>
            <td>200</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Atletismo 100 metros Feminino</td>
            <td>200</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Atletismo 100 metros Masculino</td>
            <td>200</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Atletismo 200 metros Feminino</td>
            <td>200</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Atletismo 200 metros Masculino</td>
            <td>200</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Atletismo 400 metros Feminino</td>
            <td>200</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Atletismo 400 metros Masculino</td>
            <td>200</td>
            <td>150</td>
            <td>100</td>
            <td>50</td>
          </tr>
		</table>
        <br />
        <br>
        <p><strong>c)</strong> </p>
        <table width="900" border="0" cellspacing="4">
          <tr>
            <td bgcolor="#CCCCCC">TORCIDA</td>
            <td bgcolor="#CCCCCC">Ouro</td>
            <td bgcolor="#CCCCCC">Prata</td>
            <td bgcolor="#CCCCCC">Bronze</td>
          </tr>
          <tr>
            <td>*Torcida</td>
            <td>400</td>
            <td>300</td>
            <td>250</td>
          </tr>
          <tr>
            <td>*Cheerleaders</td>
            <td>200</td>
            <td>100</td>
            <td>50</td>
          </tr>
		</table>
        <br />
        (*) Poderá haver mais de uma equipe com a mesma classificação.<br /><br />
        <strong>D) CERIMÔNIA DE ABERTURA DOS JOGOS:</strong>
        <br />
        Concentração: Sede Campestre do SESC - Av. Protásio Alves, 6220 - Alto Petrópolis - Porto Alegre, às 7h30min.<br />
        • Abertura: 8h<br />
        • Dois (2) pontos por atleta uniformizado até o limite de 150 pontos por turma.<br />
        • A contagem é válida até às 8h.<br />
        <br><br><br><br>
        <strong>DISPOSIÇÕES TRANSITÓRIAS:</strong>
        <br><br>
        1. A participação nos jogos é exclusiva aos alunos do Unificado Z, dos Colégios Unificado, Colégios Leonardo da Vinci, do Unificado Med e Escolas Convidadas. Os alunos dos Colégios participarão somente nas equipes dos seus colégios, mesmo que estejam matriculados no Unificado Z.<br>
        <br>
        2. Os nomes das equipes deverão ser previamente aprovados pela Comissão Organizadora, sendo vedado o nome de bebidas alcoólicas, partidos políticos, cigarros e nomes que incitem a violência, o preconceito e ofendam a moral e os bons costumes.<br>
        <br>
        3. Serão aceitos como prova, para a identificação do atleta, a Carteira de Identidade e /ou a Carteira do Unificado Z, Colégios Unificado, Colégios Leonardo da Vinci e do Unificado Med, atualizadas, exigidas no início de cada partida. O aluno não identificado não poderá participar, salvo o testemunho de um professor do curso ou do colégio.<br>
        <br>
        4. O aluno que não entregar a autorização de maior ou menor de idade na secretaria de sua sede até o dia 24/06, não poderá participar das competições.<br>
        <br>
        5. 	a) Cada turma terá um ou mais professores coordenadores escolhido pela Comissão Técnica de Desportos.<br>
		b) Cada turma terá também alunos representantes da equipe, que serão escolhidos em aula. <br>
        <br>
        6. Cada aluno poderá participar apenas de uma modalidade esportiva, exceto Cheerleaders. O aluno inscrito em uma modalidade que não jogar na 1ª fase, poderá ser inscrito e participar em outra modalidade, a partir da 2ª fase. Os casos omissos serão julgados pela Comissão Técnica de Desportos.<br>
        <br>
        7. Será dado W. O. à turma que não apresentar a equipe fardada para o jogo, no horário marcado, com o seguinte número mínimo de atletas nos desportos coletivos, conforme critério oficial:<br>
        • Futebol 7 Masculino - 7 <br>
        • Futebol 7 Feminino - 7<br>
        • Futebol de Campo Masculino - 11<br>
        • Handebol Feminino - 7<br>
        • Vôlei Misto - 6<br>
        <br>
        8. As inscrições das turmas para os Jogos do Unificado 2016 estão abertas de 08 de Junho de 2016 até a meia-noite do dia 23 de Junho de 2016 e devem ser feitas unicamente através do site www.unificado.com.br/jogos. A inscrição das equipes para participar nas diferentes modalidades esportivas só poderá ser feita exclusivamente através do site. <br>
        <br>
        9. Será obrigatória a inscrição prévia em todas as modalidades de que a equipe participar, podendo incluir mais atletas até o início dos jogos em modalidades em que a equipe já esteja inscrita.<br>
        <br>
        10. A verificação da inscrição e da identidade dos atletas durante os jogos é direito e responsabilidade de cada equipe. A ocorrência de irregularidade na identificação de atletas, se comprovada, acarretará na eliminação sumária da equipe na modalidade, além de influenciar negativamente na contagem de pontos no quesito "torcida" (disciplina e comportamento).<br>
        <br>
        11. Os recursos e protestos, de qualquer modalidade, deverão ser encaminhados à Comissão Técnica de Desportos. Para tal, deverão ser respeitados os seguintes procedimentos:<br>
        • Apontamento em súmula, pelo coordenador ou representante da equipe, do procedimento irregular constatado;<br />
        • Encaminhamento, por escrito e detalhado, da irregularidade constatada no jogo em até 30 minutos após o evento;<br />
		• As decisões serão definitivas e irrevogáveis, sendo tomadas por maioria simples dos membros da Comissão Técnica de Desportos. Os erros de arbitragem são erros de fato, portanto, contra eles não caberão recursos.<br />
        <br>
        12. O aluno expulso, em qualquer modalidade esportiva, estará eliminado definitivamente, podendo participar apenas da torcida. É importante lembrar que má conduta influencia negativamente na contagem de pontos de sua turma, no quesito "torcida" (disciplina e comportamento).<br>
        <br>
        13. A agressão física a qualquer dirigente da competição ou componente da arbitragem, por parte de atleta ou torcedor, implicará na eliminação sumária da TURMA a que pertencer o infrator de TODAS modalidades.<br>
        <br>
        14. Nos jogos, o número de inscrições prévias é ilimitado. Na súmula só poderá constar o número máximo de atletas por modalidade, mesmo que o número de inscritos seja maior:<br><br>
        • 14 atletas no Futebol 7 Masculino e Feminino <br>
        • 22 atletas no Futebol de Campo Masculino <br>
        • 14 atletas no Handebol Feminino<br>
        • 12 atletas no Vôlei Misto<br>
        O número de substituições é livre.<br>
        <br>
        15. As equipes deverão comparecer com as camisetas oficiais de cada TURMA.<br>
        <br>
        16. O atleta deverá calçar somente tênis. Para todos os desportos coletivos, fica proibido o uso de sapatos e botas, sendo permitido apenas o uso de tênis apropriado de cada esporte. <br>
        <br>
        17. Será declarada vencedora dos jogos, a turma que somar o maior número de pontos.<br>
        <br>
        18. Em caso de empates no total de pontos, serão obedecidos, para desempate os seguintes critérios:<br><br>
        1º. O maior número de primeiros lugares nos esportes coletivos<br>
        2º. O maior número de segundos lugares nos esportes coletivos<br>
        <br>
        19. Torcidas<br>
		O julgamento das torcidas será feito pela Comissão Organizadora Técnica de desportos. Está vetada a contratação e a participação de torcidas organizadas e de elementos fora do público do Pré-vestibular e Colégios participantes.<br>
        <br>
        20. Doações<br>
		Pela entrega de alimento (arroz e/ou feijão) será atribuído 01 ponto para cada kg de arroz, 02 pontos para cada Kg de feijão e 02 pontos para kg de ração animal entregue. Até às 20h do dia 1/07 estaremos recebendo as doações, após este horário, os pontos não serão mais contabilizados. O material arrecadado será doado para instituições de caridade. O limite máximo será de 800 pontos para as doações. A doação de ração animal será limitada a um quarto do valor total, isto é, 200 pontos.<br>
        <br>

        <br><br><br><br>
        <strong>MODALIDADES:</strong>
        <br><br>
        <strong>FUTEBOL SETE FEMININO E MASCULINO</strong>
        <br>
        • A competição terá par tidas de 15 (quinze) minutos corridos, sem intervalo, descontando-se as interrupções por lesões, e os 2 pedidos de tempo.<br>
        • Cada equipe poderá utilizar os atletas inscritos, 07 (sete) no jogo e os demais no banco de reservas, até o limite de 14 (quatorze) atletas.<br>
        • O tiro de meta e reposições de bola em jogo somente poderão ser feitas com as mãos.<br>
        • Falta será cobrada em 2 toques.<br>
        • A cobrança de lateral ou escanteio será feita com os pés.<br>
        • O jogador não poderá atrasar a bola para o goleiro e o mesmo pegar com a mão.<br>
        • Penalidades máximas: será cobrado em movimento.<br>
        • Será permitido o uso de chuteira society.<br>
        <br>
        <strong>CRITÉRIOS DE DESEMPATE</strong>
        <br>
		Decisão por pênaltis: uma série de três pênaltis alternados. Só poderão participar da decisão os atletas que estiveram em quadra no final do jogo. Se houver empate após a cobrança das 3 penalidades, a decisão será feita por uma penalidade para cada equipe alternadamente até que haja um vencedor. Cada Atleta, só poderá cobrar novamente, após, todos seus companheiros já terem cobrado.<br>
        <br><br>
        <strong>FUTEBOL DE CAMPO</strong> 
        <br>
        • A competição terá partidas de 20 (vinte) minutos corridos, sem intervalo, descontando-se as interrupções por lesões, e os 2 pedidos de tempo.<br>
        • Não há impedimento.<br>
		<strong>O uso de chuteira com trava será permitido somente na modalidade Futebol de campo, sendo vedado o uso de trava metálica.</strong><br>
        • As demais regras, do futebol de campo, serão usadas conforme regulamento da FIFA.<br>
        <br>
        <strong>CRITÉRIOS DE DESEMPATE</strong>
        <br>
        Decisão por pênaltis: poderão participar da decisão os atletas que estiveram em quadra no final do jogo. A cobrança pelo mesmo jogador, só poderá ser repetida, se houver empate após a cobrança das 3 penalidades. A decisão será feita por uma penalidade para cada equipe alternadamente até que haja um vencedor. Cada Atleta, só poderá cobrar novamente, após seus companheiros já terem cobrado.<br>
        <br><br>
        <strong>HANDEBOL FEMININO </strong>
        <br>
        • A competição terá partidas de 15 minutos corridos sem intervalo, descontados apenas os pedidos de tempo e paralisações por lesões. <br>
        • As equipes serão compostas por sete (7) atletas (uma no gol e seis na linha). <br>
        • Cada equipe terá direito a 1 pedido de tempo de 30 segundos. Será usada a regra da Federação Internacional de Handebol. <br>
        <br>
        <strong>CRITÉRIOS DE DESEMPATE</strong><br>
        Havendo empate, serão cobrados tiros livres de 7 metros, 3 (três) para cada equipe, executadas alternadamente. Permanecendo o empate as cobranças serão alternadas por equipe, até que uma equipe obtenha vantagem sobre a outra. Todas as jogadoras poderão cobrar os tiros de 7 metros, e só haverá repetição de aluna após todas as jogadoras que estão em quadra terem feito a cobrança.<br>
        <br><br>
        <strong>VÔLEI INDOOR MISTO</strong>
        <br>
        • A competição terá partidas de 01 set de 15 pontos, com jus, até no máximo 18 pontos.<br>
        • Cada equipe terá direito a 1 (um) pedido de tempo de 30 segundos.<br>
        • A altura da rede será determinada pela comissão organizadora.<br>
        • Manejo de bola será conforme o da FIVB, assim como nas demais regras do voleibol.<br>
        • As equipes serão compostas por 6 atletas, sendo, obrigatoriamente, 3 atletas femininas e 3 atletas masculinos.<br>
        <br>
        <br><br>
        <strong>GAMES</strong>
        <br>
        <strong>I – Das disposições gerais:</strong><br>
        1 – A modalidade Games é individual. Cada equipe deverá designar apenas um jogador para disputar cada um dos três jogos em disputa na modalidade (Just Dance 2016, Mario Kart 8 ou FIFA 16).<br>
        2 – Cada equipe deverá entregar a ficha do seu jogador para cada uma dos três jogos na data limite estabelecida pela organização geral dos Jogos do Unificado 2016.<br>
        3 – Os jogadores devem estar atentos e presentes ao local das partidas de acordo com o sorteio das chaves. O não comparecimento em até 5 minutos configura a vitória automática da equipe adversária na rodada em questão.<br>
        4 – Os jogadores são responsáveis por comunicar o resultado final de suas disputas, ao término das mesmas, à comissão responsável pela modalidade games.<br>
        5 – Caso haja um número ímpar de participantes, uma equipe será sorteada para começar diretamente na segunda fase.<br>
        <strong>II – Do jogo Just Dance 2016:</strong><br>
        1 – As disputas serão feitas com músicas pré-definidas a cada chave sorteada.<br>
        2 – O jogador com maior pontuação é o vencedor de cada disputa.<br>
        <strong>III – Do jogo Mario Kart 8:</strong><br>
        1 – Os jogadores se enfrentarão em uma das copas do jogo (4 corridas), pré-definidas para cada chave sorteada, na categoria 200cc.<br>
        2 – Os jogadores poderão escolher o personagem e o veículo de sua preferência.<br>
        3 – As disputas serão feitas com o controle Wii Remote, ficando a critério do jogador o uso do modo “direcional” ou do modo “direção”.<br>
        4 – Não é permitido pausar a corrida em andamento sob nenhuma hipótese. Caso isso ocorra, o jogador que pausou terá um ponto descontado de sua pontuação final na copa.<br>
        5 – Será o vencedor da disputa aquele jogador que obtiver a melhor pontuação na copa. Em caso de empate, o vencedor será decidido em uma disputa simples de “par-ou-ímpar”.<br>
        <strong>IV – Do jogo FIFA 16:</strong><br>
        1 – As disputas serão realizadas entre times pré-definidos anteriormente ao sorteio das chaves. Os times não poderão ser trocados sob hipótese alguma.<br>
        2 – Os jogos terão duração de 10 minutos. Caso haja empate, a disputa será resolvida nos pênaltis.<br>
        3 – Os jogadores poderão fazer alterações táticas previamente à partida, mas o tempo para fazer essas alterações é limitado a 2 minutos.<br>
		<br><br>
        <strong>REGULAMENTO DA MODALIDADE CHEERLEADERS</strong>
        <br>
        - A apresentação deverá ter duração mínima de 1 minuto e máxima de 2 minutos, considerando-se as devidas edições.<br>
        - O ritmo é de livre escolha da equipe.<br>
        - O grupo de Cheerleaders deverá ser composto de no mínimo 3 pessoas e no máximo 20. É permitida a participação de ambos os sexos.<br>
        - Figurino: é obrigatório o uso da camiseta oficial da equipe para, no mínimo, 50% dos participantes do grupo.<br>
        - É autorizado o uso de acessórios e/ou elementos para compor a coreografia. (Elementos: Pompom, bastão, bandeira etc.) É VETADO qualquer tipo de material que possa sujar o ambiente, dada a necessidade de mantermos o chão limpo para a próxima equipe se apresentar. <br>
        - O mascote da equipe, se houver, introduzirá a apresentação e será avaliado.<br>
        - O figurino será avaliado (criatividade, acabamento, mascote, maquiagem).<br>
        - A edição da música, se houver, será avaliada.<br>
        - A coreografia, incluindo-se a sincronia dos participantes e sua evolução, será avaliada.<br>
        - Vaias às equipes adversárias durante as apresentações acarretarão perda de pontos na apresentação.<br>
        - É vetada qualquer apologia ou referência ao consumo de bebidas alcoólicas, à violência ou qualquer outra conduta incompatível com o ambiente educacional.<br>
        - A música deverá ser enviada à organização em formato MP3 e editada de acordo com o tempo de apresentação. Data limite para envio: 28/06 para matheus.pinto@unificado.com.br<br>
		<br><br>
        <strong>PRÊMIO FAIR PLAY </strong>
        <br>
        A turma que tiver o melhor desempenho nos jogos, no quesito "bom espírito esportivo", ganhará o Prêmio Fair Play. Serão avaliados:<br>
        • respeito e cortesia com os adversários e equipe de arbitragem<br>
        • postura adequada da torcida durante os jogos<br>
        • menor número de cartões vermelhos<br>
        • respeito ao regulamento da competição<br>
        <br><br>
		<strong>TROFÉU SOLIDARIEDADE</strong>
        <br>
        A turma que entregar a maior quantidade de feijão, arroz e/ou ração animal ganhará o Troféu Solidariedade. <br>
       	<br><br>
        <strong>A COMISSÃO TÉCNICA DE DESPORTOS é soberana para resolver qualquer dúvida nos itens colocados acima.</strong>
    -->
  <br><br><br><br><br><br><br><br><br><br><br><br>
    </div>
</div>