<div id="content">
    <h2>Equipes</h2>
    <div class="colleft">
    <br><br>
	<!--    
    <img src="public/imgs/resultados_por_equipe.jpg">
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    -->

            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 18px arial, sans-serif;">
			7M
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = '7M'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Carlos, Zé R e Vanderlei</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
            <!--<span style="font-size:10px;"><strong>Capitão:</strong> <?=$rs->fields['cap_fut7_masc']?></span><br>-->
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
            <!--<span style="font-size:10px;"><strong>Capitão:</strong> <?=$rs->fields['cap_hand_fem']?></span><br>-->
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
            <!--<span style="font-size:10px;"><strong>Capitão:</strong> <?=$rs->fields['cap_fut_fem']?></span><br>-->
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <!--<span style="font-size:10px;"><strong>Capitão:</strong> <?=$rs->fields['cap_volei_fem']?></span><br>-->
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
            <!--<span style="font-size:10px;"><strong>Capitão:</strong> <?=$rs->fields['cap_volei_masc']?></span><br>-->
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #DA1F22; color:#DA1F22; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '7M' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 18px arial, sans-serif;">
			8M
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = '8M'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Fábio V e Patrícia Z</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #2275B0; color:#2275B0; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8M' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 18px arial, sans-serif;">
			8T/7N
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = '8T/7N'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Heston, Zé T, Rodrigo S</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F0E900; color:#F0E900; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '8T/7N' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 18px arial, sans-serif;">
			PRÉ ZONA NORTE
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'PRÉ ZONA NORTE'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Rodrigo B, Mauro e Rodrigo T</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #6F1D20; color:#6F1D20; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ ZONA NORTE' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 18px arial, sans-serif;">
			PRÉ CANOAS
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'PRÉ CANOAS'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Elói,Rafael Bassi e Breno</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A07EAE; color:#A07EAE; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ CANOAS' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 18px arial, sans-serif;">
			PRÉ NILO
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'PRÉ NILO'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Casemiro , Tiago T e Cristiano A</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #D91D80; color:#D91D80; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'PRÉ NILO' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->

            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 18px arial, sans-serif;">
			COLÉGIO ALBERTO BINS
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'COLÉGIO ALBERTO BINS'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção, Sérgio e Paola</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #A0C93E; color:#A0C93E; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ALBERTO BINS' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 18px arial, sans-serif;">
			COLÉGIO ZONA NORTE
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'COLÉGIO ZONA NORTE'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Pedro M, Roberta e Michael</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0000B2; color:#0000B2; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO ZONA NORTE' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->

            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 18px arial, sans-serif;">
			COLÉGIO CANOAS
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'COLÉGIO CANOAS'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção, Adroaldo, Didi e Bruno</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #45216A; color:#45216A; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO CANOAS' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->

            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 18px arial, sans-serif;">
			COLÉGIO NILO
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'COLÉGIO NILO'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção, Bruno F e Karine</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #222; color:#222; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO NILO' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 18px arial, sans-serif;">
			UNIFICADO MED
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'UNIFICADO MED'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção, Vera, Felipe K , Henrique e Carina</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #21843E; color:#21843E; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO MED' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 18px arial, sans-serif;">
			LEO ALFA
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'LEO ALFA'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #777; color:#777; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO ALFA' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 18px arial, sans-serif;">
			LEO BETA
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'LEO BETA'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #CCC; color:#CCC; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'LEO BETA' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->

            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 18px arial, sans-serif;">
			UNIFICADO GRAVATAÍ - VIAMÃO
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'UNIFICADO GRAVATAÍ - VIAMÃO'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção , Fernando M e Guilherme R.</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #8FD0E8; color:#8FD0E8; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO GRAVATAÍ - VIAMÃO' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!-- ------------------- -->

            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 18px arial, sans-serif;">
			UNIFICADO VALE DOS SINOS
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'UNIFICADO VALE DOS SINOS'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção, Michel, Guilherme, Henrique e Aldrim</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E97E2E; color:#E97E2E; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'UNIFICADO VALE DOS SINOS' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!--
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 18px arial, sans-serif;">
			COLÉGIO DOM BOSCO
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'COLÉGIO DOM BOSCO'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #F1AF81; color:#F1AF81; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO DOM BOSCO' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 18px arial, sans-serif;">
			COLÉGIO MONTEIRO LOBATO
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'COLÉGIO MONTEIRO LOBATO'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #65340C; color:#65340C; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO MONTEIRO LOBATO' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            -->
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 18px arial, sans-serif;">
			COLÉGIO PROVÍNCIA DE SÃO PEDRO
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #E779A6; color:#E779A6; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO PROVÍNCIA DE SÃO PEDRO' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
            <!--
            
            <h3 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 18px arial, sans-serif;">
			COLÉGIO FARROUPILHA
			<?php
			$sql = "SELECT * FROM jogos2012_times WHERE nome = 'COLÉGIO FARROUPILHA'";
			$rs = $con->executar($sql);
			?>
            - <?=$rs->fields['fantasia']?>

            </h3>
            <span style="font-size:10px;"><strong>Professores Coordenadores:</strong> Direção</span><br>
            <span style="font-size:10px;"><strong>Aluno Coordenador:</strong> <?=$rs->fields['alunocor']?> - <?=$rs->fields['alunocor_emil']?></span><br>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">FUTEBOL 7 MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'FUTEBOL 7 MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">FUTEBOL 7 FEMININO</h4>
			<?php
			$sql = "SELECT * FROM jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'FUTEBOL 7 FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br /><br />
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">HANDEBOL FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'HANDEBOL FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">VÔLEI INDOOR MISTO</h4>
            <?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'VÔLEI INDOOR MISTO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">FUTEBOL DE CAMPO MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'FUTEBOL DE CAMPO MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">GAMES</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'GAMES'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">CHEERLEADERS</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'CHEERLEADERS'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'ATLETISMO 100 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">ATLETISMO 100 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'ATLETISMO 100 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
            <br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'ATLETISMO 200 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">ATLETISMO 200 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'ATLETISMO 200 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS FEMININO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'ATLETISMO 400 METROS FEMININO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
            <h4 style="display:block; padding:0 0 0 10px; border-bottom:1px solid #0EAFA0; color:#0EAFA0; font:bold 14px arial, sans-serif;">ATLETISMO 400 METROS MASCULINO</h4>
			<?php
			$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 'COLÉGIO FARROUPILHA' AND modalidade = 'ATLETISMO 400 METROS MASCULINO'  ORDER BY id DESC";
			$rsIns = $con->executar($sql);
			while (!$rsIns->EOF) {
			?>
			<?=$rsIns->fields['nome']?> - <?=$rsIns->fields['matricula']?><br />
            <?php
			$rsIns->MoveNext(); 
			}        
			?>
        	<br><br>
        	<br><br><br><br><br><br>
             -->
            
    </div>
    <div class="colright">
   &nbsp;
    </div>    
</div>