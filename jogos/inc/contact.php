<div id="content">
    <h2>Fale Conosco</h2>
    <div class="colleft">
        <p><strong>Qualquer dúvida que você tiver entre em contato com a gente.</strong></p>
        <p>Utilize o formulário ao lado para enviar suas dúvidas e sugestões.</p>
        <img src="public/imgs/img-aux.png" alt="" />
    </div>
    <div class="colright">
        <form id="editteam" name="editteam" action="enviaContato.php" method="POST" onsubmit="return validaContato()">
        <fieldset>
            <legend>Editar um time</legend>
            <div>
                <label for="editname">Nome Completo:</label>
                <input id="nome" class="text" type="text" name="nome" value="" />
                <label for="editmail">E-mail:</label>
                <input id="email" class="text" type="text" name="email" value="" />
                <label for="editfone">Telefone:</label>
                <input id="telefone" class="text" type="text" name="telefone" value="" />
                <label for="editsede">Sede:</label>
                <input id="sede" class="text" type="text" name="sede" value="" />
                <label for="edittime">Nome do seu time:</label>
                <input id="time" class="text" type="text" name="time" value="" />                
                <label for="editmessage">Mensagem:</label>
                <textarea id="mensagem" class="textarea" name="mensagem"></textarea>
                <input id="editsubmit" class="submit" type="submit" value="ENVIAR" name="ENVIAR" />
            </div>
        </fieldset>
    </form>
    </div>
</div>