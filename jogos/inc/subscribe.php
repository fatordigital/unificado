<?php
    include_once "class/path.php";
    include_once "configuracoes.php";
    $conf = new configuracoes();
    /* inclui o arquivo de funções */
    include_once RAIZ_SITE . "class/cms.funcoes.php";
    /* inclui o arquivo de conxao */
    include_once RAIZ_SITE . "class/cms.conexao.php";
    /* conecta ao banco de dados */
	$turma = $_SESSION['turma'];
	$Siduser = $_SESSION['iduser'];
	
if ($_SESSION['user'] == ''){
		session_destroy();
		echo '<script type="text/javascript">alert("Você precisa estar logado"); document.location = "'.$conf->urlSite().'entrar"</script>';
		exit;	
	}

?>
<div id="content">
    <h2>inscrição</h2>
    <div class="colleft">
			<img src="public/imgs/banner_home.jpg" border="0">
    </div>   
    <div class="colright">
    <?php
		if ($_SESSION['tipo'] == '1'){
			$sql = "SELECT * FROM jogos2012_times WHERE nome = '$turma'";
        	$rs = $con->executar($sql);
			$alunocor = $rs->fields['alunocor'];
			$idtime = $rs->fields['id'];
			$fantasia = $rs->fields['fantasia'];
			$alunocor_rg = $rs->fields['alunorcor_rg'];
			$alunocor_mat = $rs->fields['alunocor_mat'];
			$alunocor_fone = $rs->fields['alunocor_fone'];
			$alunocor_cel = $rs->fields['alunocor_cel'];
			$alunocor_emil = $rs->fields['alunocor_emil'];
			
			if ($alunocor == null){
				?>
                    <form id="editteam" name="editteam" action="saveCoordenador.php" method="POST" onsubmit="return valida()">
                    <fieldset>
                    <div>
                    <strong>Olá Aluno Coordenador da turma <?php echo $turma ?>!</strong><br>
                    Este é o seu primeiro acesso, e deve informa os seguintes dados<br><br>
                    <label for="editname">nome completo:</label>
                    <input id="nome" class="text" type="text" name="nome" value="" style="text-transform:uppercase ;" onblur="this.value=this.value.toUpperCase()" />
                    <label for="editrg">RG:</label>
                    <input id="rg" class="text" type="text" name="rg" value="" />
					<label for="editmatricula">nº de matrícula:</label>
                    <input id="matricula" class="text" type="text" name="matricula" value="" />
                    <label for="editfone">Telefone:</label>
                    <input id="fone" class="text" type="text" name="fone" value="" />
                    <label for="editcel">Celular:</label>
                    <input id="cel" class="text" type="text" name="cel" value="" />
                    <label for="editmail">e-mail:</label>
                    <input id="email" class="text" type="text" name="email" value="" />
                    <input id="idtime" class="text" type="hidden" name="idtime" value="<?php echo $idtime ?>" />
                    <input id="editsubmit" class="submit" type="submit" value="ENVIAR" name="ENVIAR" />
                    </div>
                    </fieldset>
                    </form>
<?php				
			} else {
				
				?>
				Olá Coordenador <strong><?php echo $alunocor ?></strong> da turma <?php echo $turma ?><br><br>
                Seus dados:<br>
                Nome: <?php echo $alunocor ?><br>
                RG: <?php echo $alunocor_rg ?><br>
                Matricula: <?php echo $alunocor_mat ?><br>
                Fone: <?php echo $alunocor_fone ?><br>
                Celular: <?php echo $alunocor_cel ?><br>
                E-mail: <?php echo $alunocor_emil ?><br>
                <br><br><br><br>

		        <form id="editteam" name="editteam" action="saveFantasia.php" method="POST" onsubmit="">
		        <fieldset>
                <label for="editnomefantasia"><strong>Cadastrar / Alterar Nome do time:</strong></label>
                <input id="nomefantasia" class="text" type="text" name="nomefantasia" value="<?php echo $fantasia ?>" />
                <input id="idtime" class="text" type="hidden" name="idtime" value="<?php echo $idtime ?>" />
                <input id="editsubmit" class="submit" type="submit" value="ENVIAR" name="ENVIAR" />
                </fieldset>
                </form>
                <br><br><br><br>
                <form id="editparticipante" name="editparticipante" action="saveParticipante.php" method="POST" onsubmit="">
                <fieldset>
                <strong>Ficha de Inscrição de aluno participante</strong><br>
                Cadastre aqui seus colegas, um por vez, na devida modalidade.<br><br>
                <label for="editname">nome completo:</label>
                <input id="nome" class="text" type="text" name="nome" value="" style="text-transform:uppercase ;" onblur="this.value=this.value.toUpperCase()"/>
                <label for="editrg">e-mail:</label>
                <input id="rg" class="text" type="text" name="rg" value="" />
                <label for="editmatricula">nº de matrícula:</label>
                <input id="matricula" class="text" type="text" name="matricula" value="" />
                <input id="equipe" class="text" type="hidden" name="equipe" value="<?php echo $turma ?>" />
                <label for="editmodalidade">modalidade:</label>
                <select id="modalidade" name="modalidade">
                <option value="0" selected="selected">selecionar a modalidade...</option>
                <option value="FUTEBOL 7 MASCULINO">FUTEBOL 7 MASCULINO</option>
                <option value="FUTEBOL 7 FEMININO">FUTEBOL 7 FEMININO</option>
                <option value="HANDEBOL FEMININO">HANDEBOL FEMININO</option>
                <option value="VÔLEI INDOOR MISTO">VÔLEI INDOOR MISTO</option>
                <option value="FUTEBOL DE CAMPO MASCULINO">FUTEBOL DE CAMPO MASCULINO</option>
                <option value="GAMES">GAMES</option>
                <option value="CHEERLEADERS">CHEERLEADERS</option>
                <option value="ATLETISMO 100 METROS FEMININO">ATLETISMO 100 METROS FEMININO</option> 
                <option value="ATLETISMO 100 METROS MASCULINO">ATLETISMO 100 METROS MASCULINO</option>
                <option value="ATLETISMO 200 METROS FEMININO">ATLETISMO 200 METROS FEMININO</option>
                <option value="ATLETISMO 200 METROS MASCULINO">ATLETISMO 200 METROS MASCULINO</option>
                <option value="ATLETISMO 400 METROS FEMININO">ATLETISMO 400 METROS FEMININO</option>
                <option value="ATLETISMO 400 METROS MASCULINO">ATLETISMO 400 METROS MASCULINO</option>
                </select>
                <input id="editsubmit" class="submit" type="submit" value="ENVIAR" name="ENVIAR" />
                </fieldset>
                </form>
                <br><br><br>
                <strong>Alunos cadastrados:</strong><br><br>
                <?php
				$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '$turma'";
				$rs = $con->executar($sql);
				while (!$rs->EOF) {
				?>
				Nome: <strong><?php echo $rs->fields['nome'] ?></strong> - <?php echo $rs->fields['modalidade'] ?> (<a href="deleteUser.php?id=<?php echo $rs->fields['id'] ?>">deletar</a>)<br>
                <?php
				$rs->MoveNext();
				}
				?>
                <!--
                <?php
				$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = 1";
				$rs = $con->executar($sql);
				while (!$rs->EOF) {
				?>
                <strong><?php echo $rs->fields['modalidade'] ?></strong><br><br>
				<?php
					$sql = "SELECT * FROM  jogos2012_inscricoes WHERE modalidade = '".$rs->fields['modalidade'] . "' ORDER BY id ASC";
					$rsm = $con->executar($sql);
					while (!$rsm->EOF) { 
					?>
					Nome: <?php echo $rsm->fields['nome'] ?><br>
					<?php
					$rsm->MoveNext(); 
					}
				$rs->MoveNext(); 
				}
				
				?>
                -->
                
                

    <?php
//CADASTRO DOS CAPITOES
			}
		}	else if ($_SESSION['tipo'] == '2'){
			$sql = "SELECT * FROM jogos2012_times WHERE nome = '$turma'";
        	$rs = $con->executar($sql);
			$idtime = $rs->fields['id'];
			
			
				?>
				Olá Capitão  do <strong>
				<?php 
					if ($_SESSION['iduser'] >= '19' AND $_SESSION['iduser'] <= '35') {
							$nomecapitao = $rs->fields['cap_fut7_masc'];
							$captipo = 'cap_fut7_masc';
							$campomodalidade = 'Futebol 7 Masculino';
							echo 'Futebol 7 Masculino';
						} else if ($_SESSION['iduser'] >= '36' AND $_SESSION['iduser'] <= '52') {
							$nomecapitao = $rs->fields['cap_fut7_fem'];
							$captipo = 'cap_fut7_fem';
							$campomodalidade = 'Futebol 7 Feminino';
							echo 'Futebol 7 Feminino';
						} else if ($_SESSION['iduser'] >= '53' AND $_SESSION['iduser'] <= '69') {
							$nomecapitao = $rs->fields['cap_volei_masc'];
							$captipo = 'cap_volei_masc';
							$campomodalidade = 'Vôlei Masculino';
							echo 'Vôlei Masculino';
						} else if ($_SESSION['iduser'] >= '70' AND $_SESSION['iduser'] <= '86') {
							$nomecapitao = $rs->fields['cap_han_fem'];
							$captipo = 'cap_han_fem';
							$campomodalidade = 'Handebol Feminino';
							echo 'Handebol Feminino';
						} else if ($_SESSION['iduser'] >= '87' AND $_SESSION['iduser'] <= '103') {
							$nomecapitao = $rs->fields['cap_fut11_masc'];
							$captipo = 'cap_fut11_masc';
							$campomodalidade = 'Futebol 11 Masculino';
							echo 'Futebol 11 Masculino';
						}
				
				?>
                </strong> da turma <?php echo $turma ?><br><br>
                <br><br><br><br>

		        <form id="editteam" name="editteam" action="saveCapitao.php" method="POST" onsubmit="">
		        <fieldset>
                <label for="editnomefantasia"><strong>Cadastrar / Alterar seu nome:</strong></label>
                <input id="nomecapitao" class="text" type="text" name="nomecapitao" value="<?php echo $nomecapitao ?>" />
                <input id="capitao" class="text" type="hidden" name="capitao" value="<?php echo $captipo ?>" />
                <input id="idtime" class="text" type="hidden" name="idtime" value="<?php echo $idtime ?>" />
                <input id="editsubmit" class="submit" type="submit" value="ENVIAR" name="ENVIAR" />
                </fieldset>
                </form>
                <br><br><br><br>
                <form id="editparticipante" name="editparticipante" action="saveParticipante.php" method="POST" onsubmit="">
                <fieldset>
                <strong>Ficha de Inscrição de aluno participante</strong><br>
                Cadastre aqui seus colegas, um por vez.<br><br>
                <label for="editname">nome completo:</label>
                <input id="nome" class="text" type="text" name="nome" value="" style="text-transform:uppercase ;" onblur="this.value=this.value.toUpperCase()" />
                <label for="editrg">e-mail:</label>
                <input id="rg" class="text" type="text" name="rg" value="" />
                <label for="editmatricula">nº de matrícula:</label>
                <input id="matricula" class="text" type="text" name="matricula" value="" />
                <input id="equipe" class="text" type="hidden" name="equipe" value="<?php echo $turma ?>" />
                <input id="modalidade" class="text" type="hidden" name="modalidade" value="<?php echo $campomodalidade ?>" />
                <input id="editsubmit" class="submit" type="submit" value="ENVIAR" name="ENVIAR" />
                </fieldset>
                </form>
                <br><br><br>
                <strong>Alunos cadastrados no <?php echo $campomodalidade ?>:</strong><br><br>
                <?php
				$sql = "SELECT * FROM  jogos2012_inscricoes WHERE turma = '$turma' AND modalidade = '$campomodalidade'";
				$rs = $con->executar($sql);
				while (!$rs->EOF) {
				?>
				Nome: <strong><?php echo $rs->fields['nome'] ?></strong> - <?php echo $rs->fields['modalidade'] ?> (<a href="deleteUser.php?id=<?php echo $rs->fields['id'] ?>">deletar</a>)<br>
                <?php
				$rs->MoveNext();
				}
				?>
                
				<?php
		} else if ($_SESSION['tipo'] == '3') {
			
			
				?>
                <strong>Termômetro da solidariedade:</strong>
				<?php
				
				
			
				$sql2 = mysql_query("SELECT * FROM jogos2012_times");
				$conta = mysql_num_rows($sql2);
				while ($va = mysql_fetch_array($sql2)) {
?>

		        <form id="editteam" name="editteam" action="saveTermometro.php" method="POST" onsubmit="">
		        <fieldset>
                <label for="editnomefantasia"><strong><?php echo $va['nome']; ?> - <?php echo $va['fantasia']; ?></strong></label>
                <input id="kilos" class="text" type="text" name="kilos" value="<?php echo $va['kilos']; ?>" />
                <input id="idtime" class="text" type="hidden" name="idtime" value="<?php echo $va['id']; ?>" />
                <input id="editsubmit" class="submit" type="submit" value="ATUALIZAR" name="ATUALIZAR" />
                </fieldset>
                </form>
                <br>
                
                <?php
				}
				?>

    			<br><br><br><br>
		        <form id="editteam" name="editteam" action="saveParticipante.php" method="POST" onsubmit="">
        <fieldset>
            <div>
                <strong>Ficha de Inscrição de aluno participante</strong><br>
                Aqui você pode cadastrar alunos em qualquer time e qualquer modalidade!   :)<br><br>
                <label for="editname">nome completo:</label>
                <input id="nome" class="text" type="text" name="nome" value="" style="text-transform:uppercase ;" onblur="this.value=this.value.toUpperCase()"/>
                <label for="editmail">e-mail:</label>
                <input id="rg" class="text" type="text" name="rg" value="" />
                <label for="editmatricula">nº de matrícula:</label>
                <input id="matricula" class="text" type="text" name="matricula" value="" />
                <label for="editturma">turma:</label>
                <select id="equipe" name="equipe">
                    <option value="0" selected="selected">selecionar o time...</option>
                    <option value="7M">7M</option>
                    <option value="8M">8M</option>
                    <option value="8T/7N">8T/7N</option>
                    <option value="PRÉ ZONA NORTE">PRÉ ZONA NORTE</option>
                    <option value="PRÉ CANOAS">PRÉ CANOAS</option>
                    <option value="PRÉ NILO">PRÉ NILO</option>
                    <option value="COLÉGIO ALBERTO BINS">COLÉGIO ALBERTO BINS</option>
                    <option value="COLÉGIO ZONA NORTE">COLÉGIO ZONA NORTE</option>
                    <option value="COLÉGIO CANOAS">COLÉGIO CANOAS</option>
                    <option value="COLÉGIO NILO">COLÉGIO NILO</option>
                    <option value="UNIFICADO MED">UNIFICADO MED</option>
                    <option value="LEO ALFA">LEO ALFA</option>
                    <option value="LEO BETA">LEO BETA</option>
                    <option value="UNIFICADO GRAVATAÍ - VIAMÃO">UNIFICADO GRAVATAÍ - VIAMÃO</option>
                    <option value="UNIFICADO VALE DOS SINOS">UNIFICADO VALE DOS SINOS</option>
                    <option value="COLÉGIO PROVÍNCIA DE SÃO PEDRO">COLÉGIO PROVÍNCIA DE SÃO PEDRO</option>
				</select>
                <label for="editmodalidade">modalidade:</label>
                <select id="modalidade" name="modalidade">
                    <option value="0" selected="selected">selecionar a modalidade...</option>
                    <option value="FUTEBOL 7 MASCULINO">FUTEBOL 7 MASCULINO</option>
                    <option value="FUTEBOL 7 FEMININO">FUTEBOL 7 FEMININO</option>
                    <option value="HANDEBOL FEMININO">HANDEBOL FEMININO</option>
                    <option value="VÔLEI INDOOR MISTO">VÔLEI INDOOR MISTO</option>
                    <option value="FUTEBOL DE CAMPO MASCULINO">FUTEBOL DE CAMPO MASCULINO</option>
                    <option value="GAMES">GAMES</option>
                    <option value="CHEERLEADERS">CHEERLEADERS</option>
                    <option value="ATLETISMO 100 METROS FEMININO">ATLETISMO 100 METROS FEMININO</option> 
                    <option value="ATLETISMO 100 METROS MASCULINO">ATLETISMO 100 METROS MASCULINO</option>
                    <option value="ATLETISMO 200 METROS FEMININO">ATLETISMO 200 METROS FEMININO</option>
                    <option value="ATLETISMO 200 METROS MASCULINO">ATLETISMO 200 METROS MASCULINO</option>
                    <option value="ATLETISMO 400 METROS FEMININO">ATLETISMO 400 METROS FEMININO</option>
                    <option value="ATLETISMO 400 METROS MASCULINO">ATLETISMO 400 METROS MASCULINO</option>
				</select>
                <input id="editsubmit" class="submit" type="submit" value="ENVIAR" name="ENVIAR" />
            </div>
        </fieldset>
    </form>
            	<br><br><br><br>
                <strong>Alunos cadastrados:</strong><br><br>
                <?php
				$sql3 = mysql_query("SELECT * FROM jogos2012_inscricoes order by turma desc");
				$conta = mysql_num_rows($sql3);
				while ($vb = mysql_fetch_array($sql3)) {
				?>
				Nome: <strong><?php echo $vb['nome'] ?></strong> - <?php echo $vb['modalidade'] ?> - <?php echo $vb['turma'] ?> (<a href="deleteUser.php?id=<?php echo $vb['id'] ?>">deletar</a>)<br>
                <?php
				}
				?>
    <?php
		}
	?>
    </div>
</div>