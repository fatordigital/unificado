<div id="content">
    <h2>Classificados do Timão</h2>  
    <?php if ($_SESSION['login_class'] == '' || !isset ($_SESSION['login_class'])) {?>
    <img src="public/imgs/login.jpg" alt="" />
    <form id="editteam" name="editteam" action="login_classificados.php" method="POST">
        <fieldset>
            <legend>Ver / Adicionar item nos Classificados</legend>
            <p>Entre aqui você que ainda está sem time e buscar colegas para formar um time.</p>
            <div>
                <label for="sede">Curso:</label>
                <select id="sede" name="sede_class">
                    <option value="" selected="selected">Selecionar sua sede...</option>
                    <option value="Unificado Alberto Bins">Unificado Alberto Bins</option>
                    <option value="Unificado Bento Gonçalves">Unificado Bento Gonçalves</option>
                    <option value="Unificado Strip Center">Unificado Strip Center</option>
                    <option value="Unificado Nilo Peçanha">Unificado Nilo Peçanha</option>
                    <option value="Unificado Novo Hamburgo">Unificado Novo Hamburgo</option>
                    <option value="Unificado Canoas - Shopping Canoas">Unificado Canoas - Shopping Canoas</option>
                    <option value="Unificado Med">Unificado Med</option>
                </select>
                <p>* demais unidades não estão participando do concurso.</p>
                <label for="newregister">Matrícula</label>
                <input id="newregister" class="text" type="text" name="login_class" value="" />
                <label for="newpass">Senha:</label>
                <input id="newpass" class="text" type="password" name="senha" value="" />
                <input id="newsubmit" class="submit" type="submit" value="ENTRAR" name="entrar" />
            </div>
        </fieldset>
    </form>

    <? } else {     
        $matricula = $_SESSION['login_class'];
		
		$sede = $_POST['sede'];
        
        $sql = "SELECT * FROM classificados ORDER BY id DESC";
        $rs = $con->executar($sql);
        
    ?>
        <form name="team" action="saveClassificados.php" method="POST" onsubmit="return validaClassificado();">
            <fieldset id="capitain">
                <legend></legend>
                <div class="color">
                    <p><strong>Criar seu anúncio</strong></p>

                    <div class="grid_1">
                    	<input id="sede" class="text" type="hidden" name="sede" value="<?=$sede?>" />
                        <label for="matricula">Matrícula</label>
                        <input id="matricula" class="text" type="text" name="matricula" value="<?=$matricula?>" />
                    </div>
                    <div class="grid_1">
                        <label for="name">Nome:</label>
                        <input id="name" class="text" type="text" name="name" value="" />
                    </div>
                    <div class="grid_1">
                        <label for="email">E-mail:</label>
                        <input id="email" class="text" type="text" name="email" value="" />
                    </div>
                    <div class="grid_1">
                        <label for="curso">Curso que irá prestar vestibular:</label>
                        <label for="curso">Curso:</label>
                        <select id="curso" name="curso" width="80px">
                            <?php include 'cursos.php' ?>
                        </select>
                        <br><br>
                        <input id="newsubmit" class="submit" type="submit" value="ENTRAR" name="entrar" />         
                    </div>
                    <p>
                    Caso o cursos que você for prestar no vestibular,
                    <br>não estiver contemplado na lista entre em contato pelo email:
                    <br><strong>contato@timaounificadoufrgs.com.br</strong>,
                    <br>que prontamente estaremos adicionando.
                    </p>
                </div>
                <br><br>
                <img src="public/imgs/img-aux-3.png" alt="" /> 
            </fieldset>

            <fieldset id="membersteam">
				<?php 
				while (!$rs->EOF) {
				?>
            
                <div class="color teammember">
                <div class="grid_2">
				Eu <strong><?=$rs->fields['nome']?></strong> estou procurando um time.<br>
                Meu email é: <strong><?=$rs->fields['email']?></strong>.<br>
                Vou fazer vestibular para: <strong><?=$rs->fields['curso']?></strong>.<br>
                Sou da sede: <strong><?=$rs->fields['sede']?></strong>.
				<?php 
				$a = $rs->fields['curso'];
				$b = $rs->fields['sede'];
				$tuti = 'Estou procurando time para participar do %23timaounificadoufrgs!  Vou fazer vestibular para '.$a.'. Via ClassificadosTimão.';
				?>
                <br>
                <span style="font-size:11px; color:#FFF;">
                Compartilhar: <a href="http://twitter.com/home?status=<?php echo $tuti ?>" target="_blank" style="color:#FFF">Twitter</a>
                &nbsp;&nbsp;&nbsp;
                <a href="http://www.facebook.com/sharer.php?u=http://www.unificado.com.br/timaoufrgs" target="_blank" style="color:#FFF">Facebook</a>
                </div>
                </div>
                <?php 
                $rs->MoveNext(); 
				}
				?>
            </fieldset>
	        </form>
    <? } ?>
</div>